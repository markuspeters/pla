#!/bin/bash

# Process a Power TAC state log file, removing entries that will not 
# be processed by PLA
#
# Usage:
#
#	./processlogs.sh <statefile> <format> <processedfile>
#
# where <statefile> is the name of the original state file and
# <processedfile> the name of the file where output is redirected to
#
# If format is present it must contain one of the following values
#   2012 - Power TAC 2012 Nuremberg demo 
#   2013 - Power TAC 2013 finals (default)
#

EXPECTED_ARGS=1

if [ $# -lt $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` <statefile> [<format>]"
  exit $E_BADARGS
fi

FORMAT="2013"
if [ $# -ge 2 ]
then
  FORMAT=$2 
fi

# Force awk to use period as decimal separator
export LC_ALL=en_US

# Change field separator to double colon, change "readResolve" pseudo
# constructor to "new"
cat $1 | tr -d '\r' | sed -e 's/:/::/' | sed -e 's/:::/::/' | sed -e 's/::-rr::/::new::/' | awk -v FMT="$FORMAT" '
BEGIN	{ 
	  timeslot=0;
          # Artifical incrementing ID for various conversion purposes
          id=1;
          FS="::"; 
	  OFS="::";

          # Set to 1 after the SimStart message has been processed
          simstarted=0;
	}
	{


	  if ( $0 ~ /org.powertac.common.msg.SimStart/ ) {
            simstarted=1;
          }
 
	  if ( $0 ~ /org.powertac.common.msg.TimeslotUpdate/ ) {

            if ( FMT == "2012" && timeslot > 0 ) {
              # Generate artificial cumulative cash position records like this:
              #360::151612::org.powertac.common.CashPosition::2427::new::1762::0.0::360
              #360::151616::org.powertac.common.CashPosition::2429::new::9::0.0::360

              for (cp in CASHPOSBALANCE) {
                printf("%d::%d::org.powertac.common.CashPosition::%d::new::%d::%.8f::%d\n",timeslot,$1,id++,CASHPOSOWNER[cp],CASHPOSBALANCE[cp],timeslot);
              }
            }

	    timeslot=$6-1;
	  }

          if ( $0 ~ /org.powertac.common.WeatherForecastPrediction/ || 
               $0 ~ /org.powertac.du.DefaultBroker::.*setLocal/ ||
               $0 ~ /org.powertac.common.RandomSeed/ ||
               $0 ~ /(genco.Genco|genco.Buyer|common.Broker|du.DefaultBroker)::.*addMarketPosition/ ||
               $0 ~ /org.powertac.common.Competition::.*(addCustomer|addBroker)/ ||
               $0 ~ /org.powertac.common.Timeslot::.*disable/ ||
               $0 ~ /org.powertac.common.Tariff::.*(new|-rr|setState|addHourlyCharge)/ ||
               $0 ~ /org.powertac.common.TariffSpecification::.*addRate/ ||
               $0 ~ /org.powertac.common.TimeService::.*setCurrentTime/ ||
               $0 ~ /org.powertac.common.Rate::.*(set|add)HourlyCharge/ ||
               $0 ~ /org.powertac.common.Rate.ProbeCharge/ ||
               $0 ~ /org.powertac.common.HourlyCharge::.*setRateId/ ||
               $0 ~ /org.powertac.common.msg.OrderStatus::.*timeslotDisabled/ ||
               $0 ~ /org.powertac.common.msg.BrokerAccept/ ||
               $0 ~ /org.powertac.common.msg.SimPause/ ||
               $0 ~ /org.powertac.common.msg.SimResume/ ||
               $0 ~ /org.powertac.common.msg.SimEnd/ ||
               $0 ~ /org.powertac.common.msg.VariableRateUpdate/ ||
               $0 ~ /org.powertac.common.msg.TariffRevoke/ ||
               $0 ~ /org.powertac.common.msg.TariffStatus/ ){
            # Ignore these voluminous state messages
          } else if ( FMT == "2012" && $0 ~ /org.powertac.common.CashPosition::.*new/ ) {
            # For 2012 logs, we create artificial cumulative cash positions
            # Example of cash position creation in unprocessed log:
            # 172327:org.powertac.common.CashPosition::1855::new::1::0.0
            CASHPOSOWNER[$3] = $5
            CASHPOSBALANCE[$3] = 0
          } else if ( FMT == "2012" && $0 ~ /org.powertac.common.CashPosition::.*deposit/ ) {
            # Example of cash position deposit in unprocessed log:
            # 172327:org.powertac.common.CashPosition::1855::deposit::-0.0
            CASHPOSBALANCE[$3] = CASHPOSBALANCE[$3] + $5
          } else if ( $0 ~ /org.powertac.common.msg.TimeslotUpdate/ ) {
            if ( simstarted == 1 ) {
              # Only process TimeslotUpdate messages after the simulation has started.
              # The 2012 logs contained one timeslot update before the simulation began
              # which confuses the import process.
              print timeslot,$0;
            }
          } else {
	    print timeslot,$0;
          }
	}
END	{
          # Output final cash balance records 
          for (cp in CASHPOSBALANCE) {
            printf("%d::%d::org.powertac.common.CashPosition::%d::new::%d::%.8f::%d\n",timeslot,$1,id++,CASHPOSOWNER[cp],CASHPOSBALANCE[cp],timeslot);
          }
        }
'

