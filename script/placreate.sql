SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `pla` DEFAULT CHARACTER SET latin1 ;
USE `pla` ;

-- -----------------------------------------------------
-- Table `pla`.`pla_competition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_competition` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_competition` (
  `cmp_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cmp_name` VARCHAR(256) NULL DEFAULT NULL COMMENT 'Descriptive name of the Competition.' ,
  PRIMARY KEY (`cmp_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COMMENT = 'Represents a single Power TAC Competition. Competitions form the top of the class hierarchy in PLA. Objects of all other types are either directly or indirectly related to a Competition and must be interpreted within the surrounding Competition context.';


-- -----------------------------------------------------
-- Table `pla`.`pla_competition_properties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_competition_properties` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_competition_properties` (
  `cpr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Comment' ,
  `cpr_name` VARCHAR(256) NULL DEFAULT NULL ,
  `cpr_value` VARCHAR(256) NULL DEFAULT NULL ,
  `cpr_competition` INT(11) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`cpr_id`) ,
  INDEX `fk_cpr_cmp_idx` (`cpr_competition` ASC) ,
  UNIQUE INDEX `uk_cpr_id_name` (`cpr_name` ASC, `cpr_id` ASC) ,
  CONSTRAINT `fk_cpr_cmp`
    FOREIGN KEY (`cpr_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COMMENT = 'Competition Properties are key/value pairs holding miscellaneous properties of the parent competition, such as the \'MinimumTimeslotCount\' or \'BootstrapTimeslotCount\'.\n\nPLA automatically stores a \'Comment\' property for each Competition upon import which holds the descriptive name given to the import.';


-- -----------------------------------------------------
-- Table `pla`.`pla_import`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_import` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_import` (
  `imp_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `imp_ts` INT UNSIGNED NULL ,
  `imp_time` INT NOT NULL ,
  `imp_class` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_instance_id` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `imp_method` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg1` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg2` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg3` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg4` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg5` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg6` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg7` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg8` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arg9` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_arga` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_argb` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_argc` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_argd` VARCHAR(256) NULL DEFAULT NULL ,
  `imp_target` INT(11) UNSIGNED NULL DEFAULT NULL ,
  INDEX `fk_imp_cmp_idx` (`imp_target` ASC) ,
  PRIMARY KEY (`imp_id`) ,
  INDEX `idx_imp_class` (`imp_class` ASC) ,
  INDEX `idx_imp_target` (`imp_target` ASC) ,
  INDEX `idx_imp_instance_id` (`imp_instance_id` ASC) ,
  INDEX `idx_tmp_ts` (`imp_ts` ASC) ,
  INDEX `index8` (`imp_class` ASC, `imp_method` ASC) ,
  CONSTRAINT `fk_imp_cmp`
    FOREIGN KEY (`imp_target` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `pla`.`pla_timeslot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_timeslot` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_timeslot` (
  `tsl_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tsl_competition` INT(11) UNSIGNED NOT NULL ,
  `tsl_serial` INT NOT NULL ,
  `tsl_time` DATETIME NOT NULL ,
  `tsl_timestring` VARCHAR(256) NOT NULL COMMENT 'Within each competition, the timeslot serial needs to be unique.' ,
  UNIQUE INDEX `uk_tsl_serial_competition` (`tsl_competition` ASC, `tsl_serial` ASC) ,
  INDEX `fk_tsl_cmp_idx` (`tsl_competition` ASC) ,
  PRIMARY KEY (`tsl_id`) ,
  INDEX `idx_tsl_time` (`tsl_time` ASC) ,
  INDEX `idx_tsl_timestring` (`tsl_timestring` ASC) ,
  INDEX `idx_tsl_serial` (`tsl_serial` ASC) ,
  CONSTRAINT `fk_tsl_cmp`
    FOREIGN KEY (`tsl_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_customer` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_customer` (
  `cst_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cst_competition` INT(11) UNSIGNED NOT NULL ,
  `cst_instance` INT(11) UNSIGNED NOT NULL ,
  `cst_population` INT NOT NULL COMMENT 'Number of persons represented by the Customer instance.' ,
  `cst_name` VARCHAR(256) NOT NULL COMMENT 'Descriptive name of the Customer instance.' ,
  `cst_powertype` VARCHAR(256) NULL DEFAULT NULL COMMENT 'Descriptive ' ,
  `cst_multi_contracting` TINYINT(1) NULL DEFAULT FALSE COMMENT 'Indicates whether or not this customer engages in multiple contracts at the same time.' ,
  `cst_can_negotiate` TINYINT(1) NULL DEFAULT FALSE COMMENT 'Indicates whether or not this Customer negotiates individual contracts and chooses from Tariffs (TRUE), or whether he only chooses Tariffs but does not engage in negotiations (FALSE).' ,
  PRIMARY KEY (`cst_id`) ,
  INDEX `fk_cst_competition_idx` (`cst_competition` ASC) ,
  INDEX `idx_cst_instance` (`cst_instance` ASC) ,
  UNIQUE INDEX `uk_cst_instance_competition` (`cst_competition` ASC, `cst_instance` ASC) ,
  CONSTRAINT `fk_cst_competition`
    FOREIGN KEY (`cst_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents a group of small-scale consumers or producers of electricity with a shared consumption, negotiation, and tariff selection behavior. Customers are a Broker\'s counterparty in the retail market where they either choose standardized Tariffs anonymously or (for larger Customers) enter into in bilateral negotiations with the Broker over individualized contracts.';


-- -----------------------------------------------------
-- Table `pla`.`pla_broker`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_broker` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_broker` (
  `bkr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bkr_instance` INT(11) UNSIGNED NOT NULL ,
  `bkr_competition` INT(11) UNSIGNED NOT NULL ,
  `bkr_name` VARCHAR(256) NOT NULL COMMENT 'Login name of the Broker.' ,
  PRIMARY KEY (`bkr_id`) ,
  INDEX `fk_bkr_cmp_idx` (`bkr_competition` ASC) ,
  UNIQUE INDEX `uk_bkr_instance_competition` (`bkr_instance` ASC, `bkr_competition` ASC) ,
  INDEX `idx_bkr_competition` (`bkr_competition` ASC) ,
  CONSTRAINT `fk_bkr_cmp`
    FOREIGN KEY (`bkr_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'A Broker represents a retailer or aggregator, they serve a local community of Customers and trade in the Wholesale Market to offset their Customers\' net consumption or production. Brokers are the participants in the Power TAC competition, and are usually implemented by the competing third parties, the exception being the default broker, which represents the incumbent monopoly and is provided by the platform.';


-- -----------------------------------------------------
-- Table `pla`.`pla_tariff`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_tariff` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_tariff` (
  `trf_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `trf_instance` INT(11) UNSIGNED NOT NULL ,
  `trf_broker` INT(11) UNSIGNED NOT NULL ,
  `trf_competition` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`trf_id`) ,
  INDEX `fk_trf_bkr_idx` (`trf_broker` ASC) ,
  INDEX `fk_trf_cmp_idx` (`trf_competition` ASC) ,
  UNIQUE INDEX `uk_trf_instance_competition` (`trf_instance` ASC, `trf_competition` ASC) ,
  INDEX `idx_trf_competition` (`trf_competition` ASC) ,
  INDEX `idx_trf_instance` (`trf_instance` ASC) ,
  CONSTRAINT `fk_trf_bkr`
    FOREIGN KEY (`trf_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_trf_cmp`
    FOREIGN KEY (`trf_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_tariff_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_tariff_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_tariff_transaction` (
  `tfc_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `tfc_instance` INT(11) UNSIGNED NOT NULL ,
  `tfc_customer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `tfc_tariff` INT(11) UNSIGNED NOT NULL ,
  `tfc_timeslot` INT(11) UNSIGNED NOT NULL ,
  `tfc_customer_count` INT NULL DEFAULT NULL ,
  `tfc_kwh` DECIMAL(15,5) NOT NULL ,
  `tfc_charge` DECIMAL(15,5) NOT NULL ,
  `tfc_type` VARCHAR(256) NOT NULL ,
  PRIMARY KEY (`tfc_id`) ,
  INDEX `fk_tfc_cst_idx` (`tfc_customer` ASC) ,
  INDEX `fk_tfc_trf_idx` (`tfc_tariff` ASC) ,
  INDEX `fk_tfc_tsl_idx` (`tfc_timeslot` ASC) ,
  INDEX `idx_tfc` (`tfc_instance` ASC) ,
  INDEX `idx_tfc_type` (`tfc_type` ASC) ,
  CONSTRAINT `fk_tfc_cst`
    FOREIGN KEY (`tfc_customer` )
    REFERENCES `pla`.`pla_customer` (`cst_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tfc_trf`
    FOREIGN KEY (`tfc_tariff` )
    REFERENCES `pla`.`pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tfc_tsl`
    FOREIGN KEY (`tfc_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_weather_forecast`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_weather_forecast` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_weather_forecast` (
  `wfc_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wfc_instance` INT(11) UNSIGNED NOT NULL ,
  `wfc_competition` INT(11) UNSIGNED NOT NULL ,
  `wfc_timeslot` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`wfc_id`) ,
  INDEX `fk_wfc_cmp_idx` (`wfc_competition` ASC) ,
  INDEX `fk_wfc_tsl_idx` (`wfc_timeslot` ASC) ,
  INDEX `idx_wfc_instance` (`wfc_instance` ASC) ,
  UNIQUE INDEX `uk_wfc_instance_competition` (`wfc_instance` ASC, `wfc_competition` ASC) ,
  CONSTRAINT `fk_wfc_cmp`
    FOREIGN KEY (`wfc_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wfc_tsl`
    FOREIGN KEY (`wfc_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_weather_forecast_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_weather_forecast_data` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_weather_forecast_data` (
  `wfd_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wfd_forecast` INT(11) UNSIGNED NOT NULL ,
  `wfd_instance` INT(11) UNSIGNED NOT NULL ,
  `wfd_offset` INT NOT NULL ,
  `wfd_temperature` DECIMAL(15,5) NOT NULL ,
  `wfd_wind_speed` DECIMAL(15,5) NOT NULL ,
  `wfd_wind_direction` DECIMAL(15,5) NOT NULL ,
  `wfd_cloud_cover` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`wfd_id`) ,
  INDEX `idx_wfd_instance` (`wfd_instance` ASC) ,
  INDEX `fk_wfd_wfc_idx` (`wfd_forecast` ASC) ,
  CONSTRAINT `fk_wfd_wfc`
    FOREIGN KEY (`wfd_forecast` )
    REFERENCES `pla`.`pla_weather_forecast` (`wfc_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_orderbook`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_orderbook` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_orderbook` (
  `obk_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `obk_instance` INT(11) UNSIGNED NOT NULL ,
  `obk_competition` INT(11) UNSIGNED NOT NULL ,
  `obk_timeslot` INT(11) UNSIGNED NOT NULL ,
  `obk_clearing_price` DECIMAL(15,5) NOT NULL ,
  `obk_date_executed` DATETIME NULL ,
  PRIMARY KEY (`obk_id`) ,
  INDEX `fk_obk_cmp_idx` (`obk_competition` ASC) ,
  INDEX `fk_obk_tsl_idx` (`obk_timeslot` ASC) ,
  INDEX `idx_obk_instance` (`obk_instance` ASC) ,
  UNIQUE INDEX `uk_obk_instance_competition` (`obk_instance` ASC, `obk_competition` ASC) ,
  CONSTRAINT `fk_obk_cmp`
    FOREIGN KEY (`obk_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_obk_tsl`
    FOREIGN KEY (`obk_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_orderbook_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_orderbook_order` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_orderbook_order` (
  `obo_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `obo_instance` INT(11) UNSIGNED NOT NULL ,
  `obo_competition` INT(11) UNSIGNED NOT NULL ,
  `obo_orderbook` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `obo_mwh` DECIMAL(15,5) NOT NULL ,
  `obo_limit_price` DECIMAL(15,5) NOT NULL ,
  `obo_type` ENUM('ASK','BID') NULL DEFAULT NULL ,
  PRIMARY KEY (`obo_id`) ,
  INDEX `fk_obo_orderbook_idx` (`obo_orderbook` ASC) ,
  INDEX `idx_obo_instance` (`obo_instance` ASC) ,
  INDEX `fk_obo_competition_idx` (`obo_competition` ASC) ,
  CONSTRAINT `fk_obo_orderbook`
    FOREIGN KEY (`obo_orderbook` )
    REFERENCES `pla`.`pla_orderbook` (`obk_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_obo_competition`
    FOREIGN KEY (`obo_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_buyer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_buyer` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_buyer` (
  `buy_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `buy_instance` INT(11) UNSIGNED NOT NULL ,
  `buy_competition` INT(11) UNSIGNED NOT NULL ,
  `buy_name` VARCHAR(256) NOT NULL ,
  `buy_price_beta` DECIMAL(15,5) NULL DEFAULT NULL ,
  `buy_mwh` DECIMAL(15,5) NULL DEFAULT NULL ,
  PRIMARY KEY (`buy_id`) ,
  INDEX `fk_buy_cmp_idx` (`buy_competition` ASC) ,
  UNIQUE INDEX `uk_buy_instance_competition` (`buy_instance` ASC, `buy_competition` ASC) ,
  INDEX `idx_buy_instance` (`buy_instance` ASC) ,
  CONSTRAINT `fk_buy_cmp`
    FOREIGN KEY (`buy_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_genco`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_genco` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_genco` (
  `gnc_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `gnc_instance` INT(11) UNSIGNED NOT NULL ,
  `gnc_competition` INT(11) UNSIGNED NOT NULL ,
  `gnc_name` VARCHAR(256) NOT NULL ,
  `gnc_nominal_capacity` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'NEEDS CLARIFICATION' ,
  `gnc_variability` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'NEEDS CLARIFICATION' ,
  `gnc_reliability` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'Proportion of time the Genco is available to produce electricity. Ranges from 0.0 to 1.0 where higher values denote larger shares of time available for production.' ,
  `gnc_cost` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'NEEDS CLARIFICATION' ,
  `gnc_carbon_emission_rate` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'NEEDS CLARIFICATION' ,
  `gnc_commitment_leadtime` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'NEEDS CLARIFICATION' ,
  PRIMARY KEY (`gnc_id`) ,
  INDEX `fk_gnc_cmp_idx` (`gnc_competition` ASC) ,
  UNIQUE INDEX `uk_gnc_instance_competition` (`gnc_instance` ASC, `gnc_competition` ASC) ,
  INDEX `idx_gnc_instance` (`gnc_instance` ASC) ,
  CONSTRAINT `fk_gnc_cmp`
    FOREIGN KEY (`gnc_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents a large-scale producer of electricity and an actor in the wholesale market.';


-- -----------------------------------------------------
-- Table `pla`.`pla_market_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_market_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_market_transaction` (
  `mtr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `mtr_instance` INT(11) UNSIGNED NOT NULL ,
  `mtr_competition` INT(11) UNSIGNED NOT NULL ,
  `mtr_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mtr_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mtr_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mtr_delivery_timeslot` INT(11) UNSIGNED NOT NULL COMMENT 'Reference to the timeslot in which the transaction occurred.' ,
  `mtr_order_timeslot` INT(11) UNSIGNED NOT NULL COMMENT 'Reference to the timeslot in which the underlying power delivery occurrs\n.' ,
  `mtr_mwh` DECIMAL(15,5) NOT NULL ,
  `mtr_price` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`mtr_id`) ,
  INDEX `fk_mtr_cmp_idx` (`mtr_competition` ASC) ,
  INDEX `fk_mtr_bkr_idx` (`mtr_broker` ASC) ,
  INDEX `fk_mtr_tsl_idx` (`mtr_delivery_timeslot` ASC) ,
  INDEX `idx_mtr_instance` (`mtr_instance` ASC) ,
  UNIQUE INDEX `uk_mtr_instance_competition` (`mtr_instance` ASC, `mtr_competition` ASC) ,
  INDEX `fk_mtr_buy_idx` (`mtr_buyer` ASC) ,
  INDEX `fk_mtr_gnc_idx` (`mtr_genco` ASC) ,
  INDEX `fk_mtr_order_tsl_idx` (`mtr_order_timeslot` ASC) ,
  CONSTRAINT `fk_mtr_cmp`
    FOREIGN KEY (`mtr_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_bkr`
    FOREIGN KEY (`mtr_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_delivery_tsl`
    FOREIGN KEY (`mtr_delivery_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_buy`
    FOREIGN KEY (`mtr_buyer` )
    REFERENCES `pla`.`pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_gnc`
    FOREIGN KEY (`mtr_genco` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mtr_order_tsl`
    FOREIGN KEY (`mtr_order_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_cash_position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_cash_position` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_cash_position` (
  `cpo_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `cpo_instance` INT(11) UNSIGNED NOT NULL ,
  `cpo_competition` INT(11) UNSIGNED NOT NULL ,
  `cpo_timeslot` INT(11) UNSIGNED NOT NULL ,
  `cpo_broker` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Exactly one in { cpo_broker, cpo_genco, cpo_buyer } is non-null and references the entity whose cash position is described' ,
  `cpo_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `cpo_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `cpo_balance` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`cpo_id`) ,
  INDEX `fk_cpo_bkr_idx` (`cpo_broker` ASC) ,
  INDEX `fk_cpo_cmp_idx` (`cpo_competition` ASC) ,
  INDEX `idx_cpo_instance` (`cpo_instance` ASC) ,
  INDEX `fk_cpo_gnc_idx` (`cpo_genco` ASC) ,
  INDEX `fk_cpo_buy_idx` (`cpo_buyer` ASC) ,
  INDEX `fk_cpo_tsl_idx` (`cpo_timeslot` ASC) ,
  CONSTRAINT `fk_cpo_bkr`
    FOREIGN KEY (`cpo_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_cmp`
    FOREIGN KEY (`cpo_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_gnc`
    FOREIGN KEY (`cpo_genco` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_buy`
    FOREIGN KEY (`cpo_buyer` )
    REFERENCES `pla`.`pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cpo_tsl`
    FOREIGN KEY (`cpo_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents the cash account of a Broker, Genco, or Buyer with the Power TAC accounting service. A Cash Position has an initial balance, credits to or debits from the Cash Position are represented as Cash Position Deposits.';


-- -----------------------------------------------------
-- Table `pla`.`pla_market_position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_market_position` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_market_position` (
  `mpo_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `mpo_instance` INT(11) UNSIGNED NOT NULL ,
  `mpo_competition` INT(11) UNSIGNED NOT NULL ,
  `mpo_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mpo_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mpo_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `mpo_timeslot` INT(11) UNSIGNED NOT NULL ,
  `mpo_initial` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`mpo_id`) ,
  INDEX `fk_mpo_bkr_idx` (`mpo_broker` ASC) ,
  INDEX `fk_mpo_cmp_idx` (`mpo_competition` ASC) ,
  INDEX `idx_mpo_instance` (`mpo_instance` ASC) ,
  INDEX `fk_mpo_tsl_idx` (`mpo_timeslot` ASC) ,
  UNIQUE INDEX `uk_mpo_instance_competition` (`mpo_instance` ASC, `mpo_competition` ASC) ,
  INDEX `fk_mpo_gnc_idx` (`mpo_genco` ASC) ,
  INDEX `fk_mpo_buy_idx` (`mpo_buyer` ASC) ,
  CONSTRAINT `fk_mpo_bkr`
    FOREIGN KEY (`mpo_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_cmp`
    FOREIGN KEY (`mpo_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_tsl`
    FOREIGN KEY (`mpo_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_gnc`
    FOREIGN KEY (`mpo_genco` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpo_buy`
    FOREIGN KEY (`mpo_buyer` )
    REFERENCES `pla`.`pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_market_position_update`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_market_position_update` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_market_position_update` (
  `mpu_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `mpu_market_position` INT(11) UNSIGNED NOT NULL ,
  `mpu_timeslot` INT(11) UNSIGNED NOT NULL ,
  `mpu_mwh` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`mpu_id`) ,
  INDEX `fk_mpd_mpo_idx` (`mpu_market_position` ASC) ,
  INDEX `fk_mpu_tsl_idx` (`mpu_timeslot` ASC) ,
  CONSTRAINT `fk_mpd_mpo`
    FOREIGN KEY (`mpu_market_position` )
    REFERENCES `pla`.`pla_market_position` (`mpo_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_mpu_tsl`
    FOREIGN KEY (`mpu_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_rate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_rate` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_rate` (
  `rte_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rte_instance` INT(11) UNSIGNED NOT NULL ,
  `rte_competition` INT(11) UNSIGNED NOT NULL ,
  `rte_tariff` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `rte_weekly_begin` INT NULL DEFAULT NULL COMMENT 'Weekly applicability' ,
  `rte_weekly_end` INT NULL DEFAULT NULL ,
  `rte_daily_begin` INT NULL DEFAULT NULL ,
  `rte_daily_end` INT NULL DEFAULT NULL ,
  `rte_tier_threshold` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_fixed` TINYINT(1) NULL DEFAULT TRUE ,
  `rte_min_value` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_max_value` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_notice_interval` INT NULL DEFAULT NULL ,
  `rte_expected_mean` DECIMAL(15,5) NULL DEFAULT NULL ,
  `rte_max_curtailment` DECIMAL(15,5) NULL DEFAULT NULL ,
  PRIMARY KEY (`rte_id`) COMMENT 'fk_rte_trf index description' ,
  INDEX `fk_rte_trf_idx` (`rte_tariff` ASC) ,
  INDEX `idx_rte_instance` (`rte_instance` ASC) ,
  INDEX `fk_rte_cmp_idx` (`rte_competition` ASC) ,
  CONSTRAINT `fk_rte_trf`
    FOREIGN KEY (`rte_tariff` )
    REFERENCES `pla`.`pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rte_cmp`
    FOREIGN KEY (`rte_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_weather_report`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_weather_report` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_weather_report` (
  `wre_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `wre_instance` INT(11) UNSIGNED NOT NULL ,
  `wre_competition` INT(11) UNSIGNED NOT NULL ,
  `wre_timeslot` INT(11) UNSIGNED NOT NULL ,
  `wre_temperature` DECIMAL(15,5) NOT NULL ,
  `wre_wind_speed` DECIMAL(15,5) NOT NULL ,
  `wre_wind_direction` DECIMAL(15,5) NOT NULL ,
  `wre_cloud_cover` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`wre_id`) ,
  INDEX `fk_wre_cmp_idx` (`wre_competition` ASC) ,
  INDEX `fk_wre_tsl_idx` (`wre_timeslot` ASC) ,
  INDEX `idx_wre_instance` (`wre_instance` ASC) ,
  CONSTRAINT `fk_wre_cmp`
    FOREIGN KEY (`wre_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_wre_tsl`
    FOREIGN KEY (`wre_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_order` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_order` (
  `ord_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ord_instance` INT(11) UNSIGNED NOT NULL ,
  `ord_competition` INT(11) UNSIGNED NOT NULL ,
  `ord_broker` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `ord_genco` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `ord_buyer` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `ord_timeslot` INT(11) UNSIGNED NOT NULL COMMENT 'The timeslot that the originator of the order is  buying/selling electricity for.' ,
  `ord_timeslot_placed` INT(11) UNSIGNED NOT NULL COMMENT 'The timeslot that the order originated in.' ,
  `ord_mwh` DECIMAL(15,5) NOT NULL ,
  `ord_limit` DECIMAL(15,5) NULL ,
  PRIMARY KEY (`ord_id`) ,
  INDEX `fk_ord_cmp_idx` (`ord_competition` ASC) ,
  INDEX `fk_ord_bkr_idx` (`ord_broker` ASC) ,
  INDEX `fk_ord_tsl_order_idx` (`ord_timeslot` ASC) ,
  INDEX `idx_ord_instance` (`ord_instance` ASC) ,
  UNIQUE INDEX `uk_ord_instance_competition` (`ord_instance` ASC, `ord_competition` ASC) ,
  INDEX `fk_ord_gnc_idx` (`ord_genco` ASC) ,
  INDEX `fk_ord_buy_idx` (`ord_buyer` ASC) ,
  INDEX `fk_ord_tsl_placed_idx` (`ord_timeslot_placed` ASC) ,
  CONSTRAINT `fk_ord_cmp`
    FOREIGN KEY (`ord_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_bkr`
    FOREIGN KEY (`ord_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_tsl_order`
    FOREIGN KEY (`ord_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_gnc`
    FOREIGN KEY (`ord_genco` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_buy`
    FOREIGN KEY (`ord_buyer` )
    REFERENCES `pla`.`pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ord_tsl_placed`
    FOREIGN KEY (`ord_timeslot_placed` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_cleared_trade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_cleared_trade` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_cleared_trade` (
  `clt_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `clt_instance` INT(11) UNSIGNED NOT NULL ,
  `clt_competition` INT(11) UNSIGNED NOT NULL ,
  `clt_timeslot` INT(11) UNSIGNED NOT NULL COMMENT 'Timeslot to which the trade refers. That is, the time at which money and energy will actually be exchanged.' ,
  `clt_execution_mwh` DECIMAL(15,5) NOT NULL COMMENT 'Magnitude of the total energy (in MWh) traded as a result of the clearing process described by this object. Either zero or positive.' ,
  `clt_execution_price` DECIMAL(15,5) NOT NULL COMMENT 'Price, in money units per MWh, for energy traded as a result of the clearing process described by this object. Either zero or positive.' ,
  `clt_date_executed` DATETIME NOT NULL COMMENT 'Time' ,
  PRIMARY KEY (`clt_id`) ,
  INDEX `fk_clt_cmp_idx` (`clt_competition` ASC) ,
  INDEX `fk_clt_tsl_idx` (`clt_timeslot` ASC) ,
  INDEX `idx_clt_instance` (`clt_instance` ASC) ,
  CONSTRAINT `fk_clt_cmp`
    FOREIGN KEY (`clt_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_clt_tsl`
    FOREIGN KEY (`clt_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents publicly announced information about a single wholesale market clearing. A Cleared Trade is generated at least for each Timeslot in which a non-zero quantity was traded.';


-- -----------------------------------------------------
-- Table `pla`.`pla_balancing_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_balancing_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_balancing_transaction` (
  `btr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `btr_instance` INT(11) UNSIGNED NOT NULL ,
  `btr_competition` INT(11) UNSIGNED NOT NULL COMMENT 'While the parent competition is implicit in btr_broker and btr_timeslot, it is included here for query convenience.' ,
  `btr_broker` INT(11) UNSIGNED NOT NULL ,
  `btr_timeslot` INT(11) UNSIGNED NOT NULL ,
  `btr_kwh` DECIMAL(15,5) NOT NULL COMMENT 'Magnitude of the balancing action taken by the DU w.r.t. the referenced timeslot and broker. Positive values indicate flows to the broker (it was underbalanced) while negative values indicate flows from the broker.' ,
  `btr_charge` DECIMAL(15,5) NOT NULL COMMENT 'Charge for the balancing action taken by the DU w.r.t. the referenced timeslot and broker. Positive values indicate credits (the broker received money for its imbalance) while negative values indicate debits.' ,
  PRIMARY KEY (`btr_id`) ,
  INDEX `fk_btr_cmp_idx` (`btr_competition` ASC) ,
  INDEX `fk_btr_bkr_idx` (`btr_broker` ASC) ,
  INDEX `idx_btr_instance` (`btr_instance` ASC) ,
  INDEX `idx_btr_time` (`btr_timeslot` ASC) ,
  UNIQUE INDEX `uk_btr_instance_competition` (`btr_instance` ASC, `btr_competition` ASC) ,
  INDEX `fk_btr_tsl_idx` (`btr_timeslot` ASC) ,
  CONSTRAINT `fk_btr_cmp`
    FOREIGN KEY (`btr_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_btr_bkr`
    FOREIGN KEY (`btr_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_btr_tsl`
    FOREIGN KEY (`btr_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'A BalancingTransaction represents the action taken by the Distribution Utility in a particular Timeslot, and with respect to a particular Broker, to balance that Broker\'s total supply and demand. Its attributes are the magnitude of the balancing (in kWh) and the associated balancing charges to the broker (negative for costs, positive for gains).';


-- -----------------------------------------------------
-- Table `pla`.`pla_distribution_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_distribution_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_distribution_transaction` (
  `dtr_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `dtr_instance` INT(11) UNSIGNED NOT NULL ,
  `dtr_competition` INT(11) UNSIGNED NOT NULL ,
  `dtr_broker` INT(11) UNSIGNED NOT NULL ,
  `dtr_timeslot` INT(11) UNSIGNED NOT NULL ,
  `dtr_kwh` DECIMAL(15,5) NOT NULL COMMENT 'NEEDS CLARIFICATION' ,
  `dtr_charge` DECIMAL(15,5) NOT NULL COMMENT 'NEEDS CLARIFICATION' ,
  PRIMARY KEY (`dtr_id`) ,
  INDEX `fk_dtr_cmp_idx` (`dtr_competition` ASC) ,
  INDEX `fk_dtr_bkr_idx` (`dtr_broker` ASC) ,
  INDEX `idx_dtr_instance` (`dtr_instance` ASC) ,
  INDEX `idx_dtr_time` (`dtr_timeslot` ASC) ,
  UNIQUE INDEX `uk_dtr_instance_competition` (`dtr_instance` ASC, `dtr_competition` ASC) ,
  INDEX `fk_dtr_tsl_idx` (`dtr_timeslot` ASC) ,
  CONSTRAINT `fk_dtr_cmp`
    FOREIGN KEY (`dtr_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_dtr_bkr`
    FOREIGN KEY (`dtr_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_dtr_tsl`
    FOREIGN KEY (`dtr_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents a charge by the Distribution Utility to a given Broker for transport of energy over its facilities during a given Timeslot.';


-- -----------------------------------------------------
-- Table `pla`.`pla_genco_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_genco_state` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_genco_state` (
  `gcs_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `gcs_genco` INT(11) UNSIGNED NOT NULL ,
  `gcs_timeslot` INT(11) UNSIGNED NOT NULL ,
  `gcs_current_capacity` DECIMAL(15,5) NULL DEFAULT NULL COMMENT 'NEEDS CLARIFICATION' ,
  `gcs_in_operation` TINYINT(1) NULL DEFAULT NULL COMMENT 'Indicates whether the referenced Genco is available for producing electricity in the referenced Timeslot.' ,
  PRIMARY KEY (`gcs_id`) ,
  INDEX `fk_gcs_gnc_idx` (`gcs_genco` ASC) ,
  INDEX `fk_gcs_tsl_idx` (`gcs_timeslot` ASC) ,
  CONSTRAINT `fk_gcs_gnc`
    FOREIGN KEY (`gcs_genco` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_gcs_tsl`
    FOREIGN KEY (`gcs_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents the state of the time-varying properties of a Genco for a given Timeslot.';


-- -----------------------------------------------------
-- Table `pla`.`pla_buyer_state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_buyer_state` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_buyer_state` (
  `bys_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bys_buyer` INT(11) UNSIGNED NOT NULL ,
  `bys_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bys_current_capacity` DECIMAL(15,5) NULL DEFAULT NULL ,
  `bys_in_operation` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`bys_id`) ,
  INDEX `fk_bys_gnc_idx` (`bys_buyer` ASC) ,
  INDEX `fk_bys_tsl_idx` (`bys_timeslot` ASC) ,
  CONSTRAINT `fk_bys_gnc`
    FOREIGN KEY (`bys_buyer` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bys_tsl`
    FOREIGN KEY (`bys_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents the state of the time-varying properties of a Buyer for a given Timeslot.';


-- -----------------------------------------------------
-- Table `pla`.`pla_bank_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_bank_transaction` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_bank_transaction` (
  `bnt_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bnt_instance` INT(11) UNSIGNED NOT NULL ,
  `bnt_competition` INT(11) UNSIGNED NOT NULL ,
  `bnt_broker` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Exactly one in { bnt_broker, bnt_genco, bnt_buyer } is non-null and references the entity whose cash account is affected by the transaction.' ,
  `bnt_genco` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Exactly one in { bnt_broker, bnt_genco, bnt_buyer } is non-null and references the entity whose cash account is affected by the transaction.' ,
  `bnt_buyer` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'Exactly one in { bnt_broker, bnt_genco, bnt_buyer } is non-null and references the entity whose cash account is affected by the transaction.' ,
  `bnt_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bnt_amount` DECIMAL(15,5) NOT NULL COMMENT 'Amount of money credited to (positive sign) or debited from (negative sign) the broker\'s cash account.' ,
  PRIMARY KEY (`bnt_id`) ,
  INDEX `fk_bnt_cmp_idx` (`bnt_competition` ASC) ,
  INDEX `fk_bnt_bkr_idx` (`bnt_broker` ASC) ,
  INDEX `fk_bnt_tsl_idx` (`bnt_timeslot` ASC) ,
  INDEX `idx_bnt_instance` (`bnt_instance` ASC) ,
  UNIQUE INDEX `uk_bnt_instance_competition` (`bnt_instance` ASC, `bnt_competition` ASC) ,
  INDEX `fk_bnt_gnc_idx` (`bnt_genco` ASC) ,
  INDEX `fk_bnt_buy_idx` (`bnt_buyer` ASC) ,
  CONSTRAINT `fk_bnt_cmp`
    FOREIGN KEY (`bnt_competition` )
    REFERENCES `pla`.`pla_competition` (`cmp_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_bkr`
    FOREIGN KEY (`bnt_broker` )
    REFERENCES `pla`.`pla_broker` (`bkr_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_tsl`
    FOREIGN KEY (`bnt_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_gnc`
    FOREIGN KEY (`bnt_genco` )
    REFERENCES `pla`.`pla_genco` (`gnc_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bnt_buy`
    FOREIGN KEY (`bnt_buyer` )
    REFERENCES `pla`.`pla_buyer` (`buy_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Represents payments between Brokers, Gencos, Buyers and the outside world. Bank Transactions are generated by Power TAC\'s accounting service, usually as the offsetting transaction to some (logical) flow of electricity from or to the respective entity.';


-- -----------------------------------------------------
-- Table `pla`.`pla_hourly_charge`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_hourly_charge` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_hourly_charge` (
  `hcg_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `hcg_instance` INT(11) UNSIGNED NOT NULL ,
  `hcg_rate` INT(11) UNSIGNED NULL ,
  `hcg_timeslot` INT(11) UNSIGNED NOT NULL ,
  `hcg_value` DECIMAL(15,5) NULL ,
  PRIMARY KEY (`hcg_id`) ,
  INDEX `fk_hcg_rate_idx` (`hcg_rate` ASC) ,
  INDEX `fk_hcg_timeslot_idx` (`hcg_timeslot` ASC) ,
  CONSTRAINT `fk_hcg_rate`
    FOREIGN KEY (`hcg_rate` )
    REFERENCES `pla`.`pla_rate` (`rte_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_hcg_timeslot`
    FOREIGN KEY (`hcg_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_balancing_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_balancing_order` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_balancing_order` (
  `bor_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bor_instance` INT(11) UNSIGNED NOT NULL ,
  `bor_tariff` INT(11) UNSIGNED NOT NULL ,
  `bor_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bor_exercise_ratio` DECIMAL(15,5) NOT NULL ,
  `bor_price` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`bor_id`) ,
  INDEX `fk_bor_tariff_idx` (`bor_tariff` ASC) ,
  INDEX `fk_bor_timeslot_idx` (`bor_timeslot` ASC) ,
  CONSTRAINT `fk_bor_tariff`
    FOREIGN KEY (`bor_tariff` )
    REFERENCES `pla`.`pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bor_timeslot`
    FOREIGN KEY (`bor_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pla`.`pla_balancing_control_event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pla`.`pla_balancing_control_event` ;

CREATE  TABLE IF NOT EXISTS `pla`.`pla_balancing_control_event` (
  `bce_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `bce_instance` INT(11) UNSIGNED NOT NULL ,
  `bce_tariff` INT(11) UNSIGNED NOT NULL ,
  `bce_timeslot` INT(11) UNSIGNED NOT NULL ,
  `bce_kwh` DECIMAL(15,5) NOT NULL ,
  `bce_payment` DECIMAL(15,5) NOT NULL ,
  PRIMARY KEY (`bce_id`) ,
  INDEX `fk_bce_timeslot_idx` (`bce_timeslot` ASC) ,
  INDEX `fk_bce_tariff_idx` (`bce_tariff` ASC) ,
  CONSTRAINT `fk_bce_timeslot`
    FOREIGN KEY (`bce_timeslot` )
    REFERENCES `pla`.`pla_timeslot` (`tsl_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bce_tariff`
    FOREIGN KEY (`bce_tariff` )
    REFERENCES `pla`.`pla_tariff` (`trf_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `pla` ;

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_import_new`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_import_new` (`imp_id` INT, `imp_ts` INT, `imp_time` INT, `imp_class` INT, `imp_instance_id` INT, `imp_method` INT, `imp_arg1` INT, `imp_arg2` INT, `imp_arg3` INT, `imp_arg4` INT, `imp_arg5` INT, `imp_arg6` INT, `imp_arg7` INT, `imp_arg8` INT, `imp_arg9` INT, `imp_arga` INT, `imp_argb` INT, `imp_argc` INT, `imp_argd` INT, `imp_target` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_balancing_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_balancing_transaction` (`btr_id` INT, `btr_instance` INT, `btr_competition` INT, `btr_broker` INT, `btr_timeslot` INT, `btr_kwh` INT, `btr_charge` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_bank_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_bank_transaction` (`bnt_id` INT, `bnt_instance` INT, `bnt_competition` INT, `bnt_broker` INT, `bnt_genco` INT, `bnt_buyer` INT, `bnt_timeslot` INT, `bnt_amount` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_broker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_broker` (`bkr_id` INT, `bkr_instance` INT, `bkr_competition` INT, `bkr_name` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_buyer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_buyer` (`buy_id` INT, `buy_instance` INT, `buy_competition` INT, `buy_name` INT, `buy_price_beta` INT, `buy_mwh` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_buyer_state`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_buyer_state` (`bys_id` INT, `bys_buyer` INT, `bys_timeslot` INT, `bys_current_capacity` INT, `bys_in_operation` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_cash_position`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_cash_position` (`cpo_id` INT, `cpo_instance` INT, `cpo_timeslot` INT, `cpo_competition` INT, `cpo_broker` INT, `cpo_genco` INT, `cpo_buyer` INT, `cpo_balance` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_cleared_trade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_cleared_trade` (`clt_id` INT, `clt_instance` INT, `clt_competition` INT, `clt_timeslot` INT, `clt_execution_mwh` INT, `clt_execution_price` INT, `clt_date_executed` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_competition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_competition` (`cmp_id` INT, `cmp_name` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_competition_properties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_competition_properties` (`cpr_id` INT, `cpr_name` INT, `cpr_value` INT, `cpr_competition` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_customer` (`cst_id` INT, `cst_competition` INT, `cst_instance` INT, `cst_population` INT, `cst_name` INT, `cst_powertype` INT, `cst_multi_contracting` INT, `cst_can_negotiate` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_distribution_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_distribution_transaction` (`dtr_id` INT, `dtr_instance` INT, `dtr_competition` INT, `dtr_broker` INT, `dtr_timeslot` INT, `dtr_kwh` INT, `dtr_charge` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_genco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_genco` (`gnc_id` INT, `gnc_instance` INT, `gnc_competition` INT, `gnc_name` INT, `gnc_nominal_capacity` INT, `gnc_variability` INT, `gnc_reliability` INT, `gnc_cost` INT, `gnc_carbon_emission_rate` INT, `gnc_commitment_leadtime` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_genco_state`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_genco_state` (`gcs_id` INT, `gcs_genco` INT, `gcs_timeslot` INT, `gcs_current_capacity` INT, `gcs_in_operation` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_import`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_import` (`imp_id` INT, `imp_ts` INT, `imp_time` INT, `imp_class` INT, `imp_instance_id` INT, `imp_method` INT, `imp_arg1` INT, `imp_arg2` INT, `imp_arg3` INT, `imp_arg4` INT, `imp_arg5` INT, `imp_arg6` INT, `imp_arg7` INT, `imp_arg8` INT, `imp_arg9` INT, `imp_arga` INT, `imp_argb` INT, `imp_argc` INT, `imp_target` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_market_position`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_market_position` (`mpo_id` INT, `mpo_instance` INT, `mpo_competition` INT, `mpo_broker` INT, `mpo_genco` INT, `mpo_buyer` INT, `mpo_timeslot` INT, `mpo_initial` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_market_position_update`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_market_position_update` (`mpu_id` INT, `mpu_market_position` INT, `mpu_timeslot` INT, `mpu_mwh` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_market_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_market_transaction` (`mtr_id` INT, `mtr_instance` INT, `mtr_competition` INT, `mtr_broker` INT, `mtr_genco` INT, `mtr_buyer` INT, `mtr_order_timeslot` INT, `mtr_delivery_timeslot` INT, `mtr_mwh` INT, `mtr_price` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_order` (`ord_id` INT, `ord_instance` INT, `ord_competition` INT, `ord_broker` INT, `ord_genco` INT, `ord_buyer` INT, `ord_timeslot` INT, `ord_timeslot_placed` INT, `ord_mwh` INT, `ord_limit` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_orderbook`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_orderbook` (`obk_id` INT, `obk_instance` INT, `obk_competition` INT, `obk_timeslot` INT, `obk_clearing_price` INT, `obk_date_executed` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_orderbook_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_orderbook_order` (`obo_id` INT, `obo_instance` INT, `obo_competition` INT, `obo_orderbook` INT, `obo_mwh` INT, `obo_limit_price` INT, `obo_type` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_rate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_rate` (`rte_id` INT, `rte_instance` INT, `rte_competition` INT, `rte_tariff` INT, `rte_weekly_begin` INT, `rte_weekly_end` INT, `rte_daily_begin` INT, `rte_daily_end` INT, `rte_tier_threshold` INT, `rte_fixed` INT, `rte_min_value` INT, `rte_max_value` INT, `rte_notice_interval` INT, `rte_expected_mean` INT, `rte_max_curtailment` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_tariff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_tariff` (`trf_id` INT, `trf_instance` INT, `trf_broker` INT, `trf_competition` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_tariff_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_tariff_transaction` (`tfc_id` INT, `tfc_instance` INT, `tfc_customer` INT, `tfc_tariff` INT, `tfc_timeslot` INT, `tfc_customer_count` INT, `tfc_kwh` INT, `tfc_charge` INT, `tfc_type` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_timeslot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_timeslot` (`tsl_id` INT, `tsl_instance` INT, `tsl_competition` INT, `tsl_serial` INT, `tsl_time` INT, `tsl_timestring` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_weather_forecast`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_weather_forecast` (`wfc_id` INT, `wfc_instance` INT, `wfc_competition` INT, `wfc_timeslot` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_weather_forecast_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_weather_forecast_data` (`wfd_id` INT, `wfd_forecast` INT, `wfd_instance` INT, `wfd_offset` INT, `wfd_temperature` INT, `wfd_wind_speed` INT, `wfd_wind_direction` INT, `wfd_cloud_cover` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_weather_report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_weather_report` (`wre_id` INT, `wre_instance` INT, `wre_competition` INT, `wre_timeslot` INT, `wre_temperature` INT, `wre_wind_speed` INT, `wre_wind_direction` INT, `wre_cloud_cover` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_hourly_charge`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_hourly_charge` (`hcg_id` INT, `hcg_instance` INT, `hcg_rate` INT, `hcg_timeslot` INT, `hcg_value` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_balancing_order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_balancing_order` (`bor_id` INT, `bor_instance` INT, `bor_tariff` INT, `bor_timeslot` INT, `bor_exercise_ratio` INT, `bor_price` INT);

-- -----------------------------------------------------
-- Placeholder table for view `pla`.`vpla_balancing_control_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pla`.`vpla_balancing_control_event` (`bce_id` INT, `bce_instance` INT, `bce_tariff` INT, `bce_timeslot` INT, `bce_kwh` INT, `bce_payment` INT);

-- -----------------------------------------------------
-- procedure import
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import`;

DELIMITER $$
USE `pla`$$
CREATE PROCEDURE `import`(comment VARCHAR(256), mark_processed BOOLEAN, format VARCHAR(16))
import_proc_label:BEGIN
    # --- Store new competition ---
    DECLARE cmpname VARCHAR(256);
    DECLARE cmpid INT;

	SET FOREIGN_KEY_CHECKS = 0;

	IF format = '2012' THEN
		CALL do_log(concat_ws(',','Importing using format 2012',cmpid,cmpname));
	ELSEIF format = '2013' THEN
		CALL do_log(concat_ws(',','Importing using format 2013',cmpid,cmpname));
	ELSE
		CALL do_log(concat_ws(',','Unknown format -',format,'- aborting import.',cmpid,cmpname));
		LEAVE import_proc_label;
	END IF;

    SELECT imp_arg1
        INTO cmpname
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.Competition' 
        AND imp_method='new';
  
    IF cmpname IS NULL THEN
        # There was no competition because it was already processed:
        # use the highest available competition id
        SELECT max(cmp_id)
            INTO cmpid
            FROM pla_competition;
    ELSE
        INSERT INTO pla_competition (cmp_name) VALUES (cmpname);
        SET cmpid = LAST_INSERT_ID();
    END IF;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Competition', 'new');
    END IF;

    CALL do_log(concat_ws(',','Importing competition',cmpid,cmpname));

    -- ----------------------------------------------------
    -- Store competition properties
    -- ----------------------------------------------------
    CALL do_log(concat_ws(',','Storing competition properties for competition',cmpid,cmpname));

    INSERT INTO pla_competition_properties
        (cpr_name,cpr_value,cpr_competition)
    (SELECT substr(imp_method,5), imp_arg1, cmpid
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.Competition' 
        AND imp_method LIKE 'with%');
  
    # Mark as processed
    IF mark_processed THEN
        UPDATE vpla_import_new
        SET imp_target = cmpid
        WHERE imp_class='org.powertac.common.Competition' 
        AND imp_method LIKE 'with%';
    END IF;

    INSERT INTO pla_competition_properties
        (cpr_name,cpr_value,cpr_competition)
    (SELECT 'SimStart', imp_arg1, cmpid
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.msg.SimStart');

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.SimStart', 'new');
    END IF;

    IF comment IS NOT NULL THEN
        INSERT INTO pla_competition_properties
            (cpr_name,cpr_value,cpr_competition)
        VALUES
            ('Comment',comment,cmpid);
    END IF;

    -- ----------------------------------------------------
    -- Store customers
    -- ----------------------------------------------------

    CALL import_customers(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store brokers
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing brokers for competition',cmpid,cmpname));

    INSERT INTO pla_broker
        (bkr_instance,bkr_competition,bkr_name)
    (SELECT imp_instance_id,cmpid,imp_arg1
        FROM vpla_import_new
        WHERE (   imp_class='org.powertac.common.Broker' OR 
                  imp_class='org.powertac.du.DefaultBroker')
        AND imp_method='new');

    IF mark_processed THEN
        UPDATE vpla_import_new
        SET imp_target = cmpid
        WHERE ( imp_class='org.powertac.common.Broker' OR 
                imp_class='org.powertac.du.DefaultBroker') AND 
                imp_method = 'new';
    END IF;

    -- ----------------------------------------------------
    -- Store GenCos
    -- ----------------------------------------------------

    CALL import_gencos(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store Buyers
    -- ----------------------------------------------------

    CALL import_buyers(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store timeslots
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing original timeslots for competition',cmpid,cmpname));

    INSERT INTO pla_timeslot
        (tsl_competition,tsl_serial,tsl_time,tsl_timestring)
    (SELECT DISTINCT cmpid,convert_key(imp_arg2)-1,convert_timestamp(imp_arg1),imp_arg1
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.msg.TimeslotUpdate'
        AND imp_method='new');

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));
	CALL do_log(concat_ws(',','Storing past-game timeslots for competition',cmpid,cmpname));

	INSERT INTO pla_timeslot
        (tsl_competition,tsl_serial,tsl_time,tsl_timestring)
	(SELECT 
		cmpid, new_serial, new_time, DATE_FORMAT(new_time, '%Y-%m-%dT%H:%i:%S.000Z') new_timestring
	FROM
	(SELECT convert_key(imp_arg3) new_serial, 
			maxt.tsl_time + INTERVAL (convert_key(imp_arg3)-maxt.tsl_serial) HOUR new_time,
			maxt.* 
	FROM vpla_import_new,
		 (SELECT *
		 FROM pla_timeslot
		 WHERE tsl_serial = 
			(SELECT MAX(tsl_serial) 
			 FROM pla_timeslot
			 WHERE tsl_competition=cmpid)
			AND tsl_competition=cmpid) maxt
	WHERE imp_class='org.powertac.common.msg.TimeslotUpdate'
		AND imp_method='new'
		AND convert_key(imp_arg3) > 
			(SELECT MAX(tsl_serial) FROM pla_timeslot WHERE tsl_competition=cmpid)) newserials);

	CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.TimeslotUpdate', 'new');
    END IF;

	IF format = '2012' THEN
		CALL do_log(concat_ws(',','Creating temporary timeslot instance mapping for competition',cmpid,cmpname));
		-- The 2012 format requires the timeslot instance IDs in some places, where
		-- later formats work exclusively with timeslot serials. Create a temporary
		-- in-memory mapping table
		CREATE TABLE IF NOT EXISTS tmp_tsl_instance_map
			(tslmap_serial INT(11) UNSIGNED,
			 tslmap_instance INT(11) UNSIGNED,
			 UNIQUE INDEX uk_tslmap_serial (tslmap_serial ASC),
			 UNIQUE INDEX uk_tslmap_instance (tslmap_instance ASC))
			engine = memory;

		TRUNCATE TABLE tmp_tsl_instance_map;

		INSERT INTO tmp_tsl_instance_map
			(tslmap_serial,tslmap_instance)
		(SELECT convert_key(imp_arg1), imp_instance_id
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.Timeslot'
			AND imp_method='new');

		IF mark_processed THEN
			CALL mark_as_processed(cmpid, 'org.powertac.common.Timeslot', 'new');
		END IF;

		CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));
	END IF;

    -- ----------------------------------------------------
    -- Store Tariffs
    -- ----------------------------------------------------

    CALL import_tariffs(cmpid, mark_processed, format);

    -- ----------------------------------------------------
    -- Store weather forecasts
    -- ----------------------------------------------------

    CALL import_weather(cmpid, mark_processed);

    -- ----------------------------------------------------
    -- Store orderbooks and their entries
    -- ----------------------------------------------------

    CALL import_orderbooks(cmpid, mark_processed, format);

    -- ----------------------------------------------------
    -- Store market transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing market transactions for competition',cmpid,cmpname));

	IF format = '2012' THEN
		INSERT INTO pla_market_transaction
			(mtr_instance,mtr_competition,mtr_delivery_timeslot,mtr_order_timeslot,mtr_mwh,mtr_price,mtr_broker,mtr_genco,mtr_buyer)
		SELECT imp_instance_id,cmpid,delivery_timeslot.tsl_id, order_timeslot.tsl_id,imp_mwh,imp_price, bkr_id,gnc_id,buy_id
		FROM
			(SELECT imp_instance_id,
					imp_arg2 imp_tsl_order_timestring,
					convert_number(imp_arg4) imp_mwh,
					convert_number(imp_arg5) imp_price,
					convert_key(imp_arg3) imp_tsl_delivery_instance,
					convert_key(imp_arg1) imp_entity_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.MarketTransaction' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON (tslmap_instance = imp_tsl_delivery_instance)
		LEFT JOIN pla_timeslot delivery_timeslot
			ON (delivery_timeslot.tsl_serial = tslmap_serial AND delivery_timeslot.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot order_timeslot
			ON (order_timeslot.tsl_timestring = imp_tsl_order_timestring AND order_timeslot.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker bkr
			ON (bkr.bkr_instance = imp_entity_instance AND bkr.bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco gnc
			ON (gnc.gnc_instance = imp_entity_instance AND gnc.gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer buy
			ON (buy.buy_instance = imp_entity_instance AND buy.buy_competition = cmpid);
	ELSE
		INSERT INTO pla_market_transaction
			(mtr_instance,mtr_competition,mtr_delivery_timeslot,mtr_order_timeslot,mtr_mwh,mtr_price,mtr_broker,mtr_genco,mtr_buyer)
		SELECT imp_instance_id,cmpid,delivery_timeslot.tsl_id, order_timeslot.tsl_id,imp_mwh,imp_price, bkr_id,gnc_id,buy_id
		FROM
			(SELECT imp_instance_id,
					convert_key(imp_arg2) imp_tsl_order_serial,
					convert_number(imp_arg4) imp_mwh,
					convert_number(imp_arg5) imp_price,
					convert_key(imp_arg3) imp_tsl_delivery_serial,
					convert_key(imp_arg1) imp_entity_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.MarketTransaction' AND imp_method='new') imp
		LEFT JOIN pla_timeslot delivery_timeslot
			ON (delivery_timeslot.tsl_serial = imp_tsl_delivery_serial AND delivery_timeslot.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot order_timeslot
			ON (order_timeslot.tsl_serial = imp_tsl_order_serial AND order_timeslot.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker bkr
			ON (bkr.bkr_instance = imp_entity_instance AND bkr.bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco gnc
			ON (gnc.gnc_instance = imp_entity_instance AND gnc.gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer buy
			ON (buy.buy_instance = imp_entity_instance AND buy.buy_competition = cmpid);
	END IF;
    
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.MarketTransaction', 'new');
    END IF;


    -- ----------------------------------------------------
    -- Store cash positions and their changes
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing cash positions for competition',cmpid,cmpname));

    INSERT INTO pla_cash_position
        (cpo_instance,cpo_competition,cpo_broker,cpo_genco,cpo_buyer,cpo_balance,cpo_timeslot)
    (SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,convert_number(imp_arg2),tsl_id
     FROM vpla_import_new
	 LEFT JOIN pla_timeslot
	   ON (tsl_serial=convert_key(imp_arg3) AND tsl_competition=cmpid)
     LEFT JOIN pla_broker
       ON (bkr_instance=convert_key(imp_arg1) AND bkr_competition=cmpid)
     LEFT JOIN pla_genco
       ON (gnc_instance=convert_key(imp_arg1) AND gnc_competition=cmpid)
     LEFT JOIN pla_buyer
       ON (buy_instance=convert_key(imp_arg1) AND buy_competition=cmpid)
     WHERE imp_class='org.powertac.common.CashPosition' 
     AND imp_method='new');    
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CashPosition', 'new');
    END IF;

  -- ----------------------------------------------------
  -- Store market positions and their changes. 
  -- ----------------------------------------------------

    CALL import_market_positions(cmpid, mark_processed, format);

  -- ----------------------------------------------------
  -- Store Orders
  -- ----------------------------------------------------  

    CALL do_log(concat_ws(',','Storing orders for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT INTO pla_order
			(ord_instance,ord_competition,ord_broker,ord_genco,ord_buyer,ord_timeslot,ord_timeslot_placed,ord_mwh,ord_limit)     
		SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,timeslot_order.tsl_id,timeslot_placed.tsl_id,imp_mwh,imp_limit
		FROM
		(SELECT imp_instance_id,
				imp_ts,
				convert_number(imp_arg3) imp_mwh,
				convert_number(imp_arg4) imp_limit,
				convert_key(imp_arg2) imp_tsl_instance,
				convert_key(imp_arg1) imp_entity_instance
		FROM vpla_import_new
		WHERE imp_class='org.powertac.common.Order' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON imp_tsl_instance = tslmap_instance
		LEFT JOIN pla_timeslot timeslot_order
			ON (timeslot_order.tsl_serial = tslmap_serial AND timeslot_order.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot timeslot_placed
			ON (timeslot_placed.tsl_serial = imp_ts AND timeslot_placed.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker
			ON (bkr_instance = imp_entity_instance AND bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco
			ON (gnc_instance = imp_entity_instance AND gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer
			ON (buy_instance = imp_entity_instance AND buy_competition = cmpid)
		-- Guard against erroneous orders for timeslots outside the actual game time
		WHERE timeslot_order.tsl_id IS NOT NULL;
	ELSE
		INSERT INTO pla_order
			(ord_instance,ord_competition,ord_broker,ord_genco,ord_buyer,ord_timeslot,ord_timeslot_placed,ord_mwh,ord_limit)     
		SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,timeslot_order.tsl_id,timeslot_placed.tsl_id,imp_mwh,imp_limit
		FROM
		(SELECT imp_instance_id,
				imp_ts,
				convert_number(imp_arg3) imp_mwh,
				convert_number(imp_arg4) imp_limit,
				convert_key(imp_arg2) imp_tsl_serial,
				convert_key(imp_arg1) imp_entity_instance
		FROM vpla_import_new
		WHERE imp_class='org.powertac.common.Order' AND imp_method='new') imp
		LEFT JOIN pla_timeslot timeslot_order
			ON (timeslot_order.tsl_serial = imp_tsl_serial AND timeslot_order.tsl_competition = cmpid)
		LEFT JOIN pla_timeslot timeslot_placed
			ON (timeslot_placed.tsl_serial = imp_ts AND timeslot_placed.tsl_competition = cmpid)
		LEFT OUTER JOIN pla_broker
			ON (bkr_instance = imp_entity_instance AND bkr_competition = cmpid)
		LEFT OUTER JOIN pla_genco
			ON (gnc_instance = imp_entity_instance AND gnc_competition = cmpid)
		LEFT OUTER JOIN pla_buyer
			ON (buy_instance = imp_entity_instance AND buy_competition = cmpid)
		-- Guard against erroneous orders for timeslots outside the actual game time
		WHERE timeslot_order.tsl_id IS NOT NULL; 
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Order', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store Cleared Trades
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing cleared trades for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT INTO pla_cleared_trade
			(clt_instance,clt_competition,clt_timeslot,clt_execution_mwh,clt_execution_price,clt_date_executed)
		SELECT imp_instance_id,cmpid,tsl_id,imp_execution_mwh,imp_execution_price,imp_date_executed
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg2) imp_execution_mwh,
					convert_number(imp_arg3) imp_execution_price,
					convert_timestamp(imp_arg4) imp_date_executed,
					convert_key(imp_arg1) imp_tsl_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.ClearedTrade' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON imp_tsl_instance = tslmap_instance
		LEFT JOIN pla_timeslot
			ON (tsl_serial=tslmap_serial AND tsl_competition=cmpid);
	ELSE
		INSERT INTO pla_cleared_trade
			(clt_instance,clt_competition,clt_timeslot,clt_execution_mwh,clt_execution_price,clt_date_executed)
		SELECT imp_instance_id,cmpid,tsl_id,imp_execution_mwh,imp_execution_price,imp_date_executed
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg2) imp_execution_mwh,
					convert_number(imp_arg3) imp_execution_price,
					convert_timestamp(imp_arg4) imp_date_executed,
					convert_key(imp_arg1) imp_tsl_serial
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.ClearedTrade' AND imp_method='new') imp
		LEFT JOIN pla_timeslot
			ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.ClearedTrade', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store balancing transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing balancing transactions for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT INTO pla_balancing_transaction
			(btr_instance,btr_competition,btr_broker,btr_timeslot,btr_kwh,btr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_btr_kwh,imp_btr_charge
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg3) imp_btr_kwh,
					convert_number(imp_arg4) imp_btr_charge,
					imp_arg2 imp_tsl_timestring,
					convert_key(imp_arg1) imp_bkr_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.BalancingTransaction' AND imp_method='new') imp
		LEFT JOIN pla_timeslot 
			ON (tsl_timestring=imp_tsl_timestring AND tsl_competition=cmpid)
		LEFT JOIN pla_broker 
			ON (bkr_instance=imp_bkr_instance AND bkr_competition=cmpid);
	ELSE
		INSERT INTO pla_balancing_transaction
			(btr_instance,btr_competition,btr_broker,btr_timeslot,btr_kwh,btr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_btr_kwh,imp_btr_charge
		FROM
			(SELECT imp_instance_id,
					convert_number(imp_arg3) imp_btr_kwh,
					convert_number(imp_arg4) imp_btr_charge,
					convert_key(imp_arg2) imp_tsl_serial,
					convert_key(imp_arg1) imp_bkr_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.BalancingTransaction' AND imp_method='new') imp
		LEFT JOIN pla_timeslot 
			ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid)
		LEFT JOIN pla_broker 
			ON (bkr_instance=imp_bkr_instance AND bkr_competition=cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.BalancingTransaction', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store distribution transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing distribution transactions for competition',cmpid,cmpname));

	IF FORMAT = "2012" THEN
		INSERT INTO pla_distribution_transaction
			(dtr_instance,dtr_competition,dtr_broker,dtr_timeslot,dtr_kwh,dtr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_dtr_kwh,imp_dtr_charge
		FROM
			(SELECT 
				imp_instance_id,
				convert_number(imp_arg3) imp_dtr_kwh,
				convert_number(imp_arg4) imp_dtr_charge,
				imp_arg2 imp_tsl_timestring,
				convert_key(imp_arg1) imp_bkr_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.DistributionTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON tsl_timestring=imp_tsl_timestring AND tsl_competition=cmpid
			LEFT JOIN pla_broker 
				ON bkr_instance=imp_bkr_instance AND bkr_competition=cmpid;
	ELSE
		INSERT INTO pla_distribution_transaction
			(dtr_instance,dtr_competition,dtr_broker,dtr_timeslot,dtr_kwh,dtr_charge)
		SELECT imp_instance_id,cmpid,bkr_id,tsl_id,imp_dtr_kwh,imp_dtr_charge
		FROM
			(SELECT 
				imp_instance_id,
				convert_number(imp_arg3) imp_dtr_kwh,
				convert_number(imp_arg4) imp_dtr_charge,
				convert_key(imp_arg2) imp_tsl_serial,
				convert_key(imp_arg1) imp_bkr_instance
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.DistributionTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON tsl_serial=imp_tsl_serial AND tsl_competition=cmpid
			LEFT JOIN pla_broker 
				ON bkr_instance=imp_bkr_instance AND bkr_competition=cmpid;
	END IF;
	
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.DistributionTransaction', 'new');
    END IF;

    -- ----------------------------------------------------
    -- Store bank transactions
    -- ----------------------------------------------------

    CALL do_log(concat_ws(',','Storing bank transactions for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT INTO pla_bank_transaction
			(bnt_instance,bnt_competition,bnt_broker,bnt_genco,bnt_buyer,bnt_timeslot,bnt_amount)
		   SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,tsl_id,imp_bnt_amount
			FROM
				(SELECT 
					imp_instance_id,
					convert_number(imp_arg2) imp_bnt_amount,
					imp_arg3 imp_tsl_timestring,
					convert_key(imp_arg1) imp_entity_instance
				FROM vpla_import_new
				WHERE imp_class='org.powertac.common.BankTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON (tsl_timestring=imp_tsl_timestring AND tsl_competition=cmpid)
			LEFT JOIN pla_broker 
				ON (bkr_instance=imp_entity_instance AND bkr_competition=cmpid)
			LEFT JOIN pla_genco
				ON (gnc_instance=imp_entity_instance AND gnc_competition=cmpid)
			LEFT JOIN pla_buyer 
				ON (buy_instance=imp_entity_instance AND buy_competition=cmpid);
	ELSE
		INSERT INTO pla_bank_transaction
			(bnt_instance,bnt_competition,bnt_broker,bnt_genco,bnt_buyer,bnt_timeslot,bnt_amount)
		   SELECT imp_instance_id,cmpid,bkr_id,gnc_id,buy_id,tsl_id,imp_bnt_amount
			FROM
				(SELECT 
					imp_instance_id,
					convert_number(imp_arg2) imp_bnt_amount,
					convert_key(imp_arg3) imp_tsl_serial,
					convert_key(imp_arg1) imp_entity_instance
				FROM vpla_import_new
				WHERE imp_class='org.powertac.common.BankTransaction' AND imp_method='new') imp
			LEFT JOIN pla_timeslot 
				ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid)
			LEFT JOIN pla_broker 
				ON (bkr_instance=imp_entity_instance AND bkr_competition=cmpid)
			LEFT JOIN pla_genco
				ON (gnc_instance=imp_entity_instance AND gnc_competition=cmpid)
			LEFT JOIN pla_buyer 
				ON (buy_instance=imp_entity_instance AND buy_competition=cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.BankTransaction', 'new');
    END IF;

  -- ----------------------------------------------------
  -- Store balancing orders and control events
  -- ----------------------------------------------------


    CALL do_log(concat_ws(',','Storing balancing orders for competition',cmpid,cmpname));

    INSERT INTO pla_balancing_order
        (bor_instance,bor_tariff,bor_timeslot,bor_exercise_ratio,bor_price)
       SELECT imp_instance_id,trf_id,tsl_id,bor_exercise_ratio,bor_price
        FROM
            (SELECT 
                imp_instance_id,
                imp_ts,
                convert_number(imp_arg1) bor_exercise_ratio,
                convert_number(imp_arg2) bor_price,
                convert_key(imp_arg3) imp_tariff_instance
            FROM vpla_import_new
            WHERE imp_class='org.powertac.common.msg.BalancingOrder' AND imp_method='new') imp
        LEFT JOIN pla_tariff
            ON (trf_instance=imp_tariff_instance AND trf_competition=cmpid)
        LEFT JOIN pla_timeslot 
            ON (tsl_serial=imp_ts AND tsl_competition=cmpid);
    
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.BalancingOrder', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing balancing control events for competition',cmpid,cmpname));

	IF format = "2012" THEN
		INSERT INTO pla_balancing_control_event
			(bce_instance,bce_tariff,bce_timeslot,bce_kwh,bce_payment)
		   SELECT imp_instance_id,trf_id,tsl_id,bce_kwh,bce_payment
			FROM
				(SELECT 
					imp_instance_id,
					convert_key(imp_arg1) imp_tariff_instance,
					convert_number(imp_arg2) bce_kwh,
					convert_number(imp_arg3) bce_payment,
					convert_key(imp_arg4) imp_tsl_serial
				FROM vpla_import_new
				WHERE imp_class='org.powertac.common.msg.BalancingControlEvent' AND imp_method='new') imp
			LEFT JOIN pla_tariff
				ON (trf_instance=imp_tariff_instance AND trf_competition=cmpid)
			LEFT JOIN pla_timeslot 
				ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid);
	ELSE
		INSERT INTO pla_balancing_control_event
			(bce_instance,bce_tariff,bce_timeslot,bce_kwh,bce_payment)
		   SELECT imp_instance_id,trf_id,tsl_id,bce_kwh,bce_payment
			FROM
				(SELECT 
					imp_instance_id,
					convert_key(imp_arg1) imp_tariff_instance,
					convert_number(imp_arg2) bce_kwh,
					convert_number(imp_arg3) bce_payment,
					convert_key(imp_arg4) imp_tsl_serial
				FROM vpla_import_new
				WHERE imp_class='org.powertac.common.msg.BalancingControlEvent' AND imp_method='new') imp
			LEFT JOIN pla_tariff
				ON (trf_instance=imp_tariff_instance AND trf_competition=cmpid)
			LEFT JOIN pla_timeslot 
				ON (tsl_serial=imp_tsl_serial AND tsl_competition=cmpid);
    END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.BalancingControlEvent', 'new');
    END IF;
  -- ----------------------------------------------------
  -- Mark simulation end
  -- ----------------------------------------------------

    INSERT INTO pla_competition_properties
        (cpr_name,cpr_value,cpr_competition)
    (SELECT 'SimEnd',imp_ts, cmpid
    FROM vpla_import_new
    WHERE imp_class='org.powertac.common.msg.SimEnd');
    
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.SimEnd', 'new');
    END IF;

	IF format = '2012' THEN
		DROP TABLE IF EXISTS tmp_tsl_instance_map;
	END IF;

	SET FOREIGN_KEY_CHECKS = 1;
    CALL do_log(concat_ws(',','Done'));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure setup_log
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`setup_log`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`setup_log` ()
BEGIN
    CREATE TABLE IF NOT EXISTS pla.pla_log 
        (time DATETIME,
         msg varchar(512)) 
    engine = memory;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure do_log
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`do_log`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`do_log` (in logMsg VARCHAR(512))
BEGIN
Declare continue handler for 1146 -- Table not found
  BEGIN
     call setup_log();
     insert into pla.pla_log values(CURRENT_TIMESTAMP(),logMsg);
  END;
 
  insert into pla.pla_log values(CURRENT_TIMESTAMP(),logMsg);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function convert_timestamp
-- -----------------------------------------------------

USE `pla`;
DROP function IF EXISTS `pla`.`convert_timestamp`;

DELIMITER $$
USE `pla`$$


CREATE FUNCTION `pla`.`convert_timestamp` (timestamp_string VARCHAR(256)) RETURNS DATETIME
BEGIN
    DECLARE conv_string VARCHAR(256);

    IF timestamp_string = "null" THEN
        RETURN NULL;
    ELSE
        SET conv_string = replace(substr(timestamp_string FROM 1 FOR length(timestamp_string) - 5), 'T',' ');
        return(CAST(conv_string AS DATETIME));
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure split_string
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`split_string`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`split_string` (input TEXT, tmp_table VARCHAR(255))
BEGIN
    DECLARE cur_position INT DEFAULT 1 ;
    DECLARE remainder TEXT;
    DECLARE cur_string VARCHAR(1000);
    DECLARE delimiter_length TINYINT UNSIGNED;
    DECLARE delimiter CHAR(1);

    SET @query_drop = CONCAT('DROP TEMPORARY TABLE IF EXISTS ', tmp_table);
    PREPARE smt_drop FROM @query_drop;
    EXECUTE smt_drop;

    SET @query_create = CONCAT('CREATE TEMPORARY TABLE ', tmp_table, '(id VARCHAR(11) NOT NULL) ENGINE=MyISAM;');
    PREPARE stm_create FROM @query_create;
    EXECUTE stm_create;

    SET @query_insert = CONCAT('INSERT INTO ', tmp_table, ' VALUES (?);');
    PREPARE stm_insert FROM @query_insert;

    SET delimiter = ',';
    SET remainder = input;
    SET delimiter_length = CHAR_LENGTH(delimiter);

    WHILE CHAR_LENGTH(remainder) > 0 AND cur_position > 0 DO
        SET cur_position = INSTR(remainder, `delimiter`);
        IF cur_position = 0 THEN
            SET cur_string = remainder;
        ELSE
            SET cur_string = LEFT(remainder, cur_position - 1);
        END IF;
        IF TRIM(cur_string) != '' THEN
            SET @id = cur_string;
            EXECUTE stm_insert USING @id;
        END IF;
        SET remainder = SUBSTRING(remainder, cur_position + delimiter_length);
    END WHILE;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_weather
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_weather`;

DELIMITER $$
USE `pla`$$
CREATE PROCEDURE `pla`.`import_weather` (cmpid INT, mark_processed BOOLEAN)
BEGIN
  # Weather forecasts have a weird format where the child references
  # (from weather_forecast to weather_forecast_data) are stored as a 
  # string list of instance_ids with the weather_forecast. This needs 
  # to be untangled procedurally.
  DECLARE instance_id INT;
  DECLARE timeslot_id INT;
  DECLARE children VARCHAR(512);
  DECLARE current_forecast INT;
  DECLARE no_more_rows BOOLEAN DEFAULT FALSE;

  # This cursor goes over all weather_forecast (with some untangled ids)
--  DECLARE wfc_cur CURSOR FOR
--    SELECT imp_instance_id,
--           tsl_id,
--           imp_arg2
--    FROM vpla_import_new,pla_timeslot
--    WHERE imp_class='org.powertac.common.WeatherForecast' 
--    AND imp_method='new'
--    AND tsl_instance=convert_key(imp_arg1) AND tsl_competition=cmpid;

--  DECLARE CONTINUE HANDLER FOR NOT FOUND
--    SET no_more_rows = TRUE;

--  CALL do_log(concat_ws(',','Opening weather forecast cursor'));
--  OPEN wfc_cur;

--  the_loop: LOOP
--    FETCH  wfc_cur
--    INTO   instance_id, timeslot_id, children;

--    IF no_more_rows THEN
--        CLOSE wfc_cur;
--        LEAVE the_loop;
--    END IF;

--    CALL do_log(concat_ws(',','Importing weather forecast',instance_id,timeslot_id,children));

    # First create the parent object
--    INSERT INTO pla_weather_forecast
--      (wfc_instance,wfc_competition,wfc_timeslot)
--      VALUES (instance_id,cmpid,timeslot_id);

--    SET current_forecast = LAST_INSERT_ID();
--    DROP TEMPORARY TABLE IF EXISTS tmp_children;
--    CALL split_string(children, 'tmp_children');
    
--    CALL do_log(concat_ws(',','Adding children to forecast',children,current_forecast));

    # Then look for all the children belonging to this
    # parent and insert them with the right reference
--    INSERT INTO pla_weather_forecast_data
--    (wfd_instance,wfd_forecast,wfd_offset,wfd_temperature,wfd_wind_speed,wfd_wind_direction,wfd_cloud_cover)
--       (SELECT imp_instance_id,
--               current_forecast,
--               convert_key(imp_arg1),
--               convert_number(imp_arg2),
--               convert_number(imp_arg3),
--               convert_number(imp_arg4),
--               convert_number(imp_arg5)
--        FROM vpla_import_new
--        WHERE imp_class='org.powertac.common.WeatherForecastPrediction' 
--        AND imp_method='new'
--        AND imp_instance_id in (SELECT id FROM tmp_children));

--  END LOOP the_loop;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid,'org.powertac.common.WeatherForecast','new');
        CALL mark_as_processed(cmpid,'org.powertac.common.WeatherForecastPrediction','new');
    END IF;

    CALL do_log(concat_ws(',','Importing weather reports for competition',cmpid));

    INSERT INTO pla_weather_report
        (wre_instance,wre_competition,wre_timeslot,wre_temperature,wre_wind_speed,wre_wind_direction,wre_cloud_cover)
    SELECT imp_instance_id, cmpid, tsl_id, imp_temperature, imp_wind_speed, imp_wind_direction, imp_cloud_cover
    FROM
       (SELECT imp_instance_id,
               convert_number(imp_arg2) imp_temperature,
               convert_number(imp_arg3) imp_wind_speed,
               convert_number(imp_arg4) imp_wind_direction,
               convert_number(imp_arg5) imp_cloud_cover,
               convert_key(imp_arg1) imp_tsl_serial
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.WeatherReport' 
        AND imp_method='new') imp
    INNER JOIN pla_timeslot
        ON tsl_serial=imp_tsl_serial AND tsl_competition=cmpid;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid,'org.powertac.common.WeatherReport','new');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure mark_as_processed
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`mark_as_processed`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`mark_as_processed` (cmpid INT, class VARCHAR(256), method VARCHAR(256))
BEGIN

  CALL do_log(concat_ws(',','Marking as read',cmpid,class,method));
  UPDATE pla_import
    SET imp_target = cmpid
    WHERE imp_class = class AND imp_method = method AND imp_target IS NULL;
  CALL do_log(concat_ws(',','Import rows marked',ROW_COUNT()));
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_orderbooks
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_orderbooks`;

DELIMITER $$
USE `pla`$$
CREATE PROCEDURE `pla`.`import_orderbooks` (cmpid INT, mark_processed BOOLEAN, format VARCHAR(16))
BEGIN

    CALL do_log(concat_ws(',','Importing orderbooks for competition',cmpid));

	IF format = '2012' THEN
		INSERT INTO pla_orderbook
			(obk_instance,obk_competition,obk_timeslot,obk_clearing_price,obk_date_executed)
		SELECT imp_instance_id, cmpid, tsl_id, obk_clearing_price, obk_date_executed 
		FROM
			(SELECT imp_instance_id,
				convert_number(imp_arg2) obk_clearing_price, 
				convert_timestamp(imp_arg3) obk_date_executed,
				convert_key(imp_arg1) imp_tsl_instance 
			FROM vpla_import_new
			WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='new') imp,
		tmp_tsl_instance_map, pla_timeslot
		WHERE
			tslmap_instance = imp_tsl_instance AND
			tslmap_serial = tsl_serial AND 
			tsl_competition = cmpid;
	ELSE
		INSERT INTO pla_orderbook
			(obk_instance,obk_competition,obk_timeslot,obk_clearing_price,obk_date_executed)
		SELECT imp_instance_id, cmpid, tsl_id, obk_clearing_price, obk_date_executed FROM
		(SELECT imp_instance_id,
			convert_number(imp_arg2) obk_clearing_price, 
			convert_timestamp(imp_arg3) obk_date_executed,
			convert_key(imp_arg1) imp_tsl_serial 
		FROM vpla_import_new
		WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='new') imp
		LEFT JOIN pla_timeslot
			ON (tsl_serial = imp_tsl_serial AND tsl_competition = cmpid);
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Orderbook', 'new');
    END IF;

    # We first create these entries without reference to their orderbooks. These
    # dangling references are then fixed below.
    CALL do_log(concat_ws(',','Importing orderbook orders for competition',cmpid));

    INSERT INTO pla_orderbook_order
        (obo_instance,obo_competition,obo_mwh,obo_limit_price)
    (SELECT imp_instance_id,cmpid,
        convert_number(imp_arg1),
        convert_number(imp_arg2) 
    FROM vpla_import_new
    WHERE imp_class='org.powertac.common.OrderbookOrder' AND imp_method='new');

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.OrderbookOrder', 'new');
    END IF;

    CALL do_log(concat_ws(',','Updating orderbook bids for competition',cmpid));

    UPDATE pla_orderbook_order,
        (SELECT obo_id, obk_id FROM
            (SELECT imp_instance_id imp_obk_instance, convert_key(imp_arg1) imp_obo_instance
            FROM vpla_import_new 
            WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='addBid') imp
        LEFT JOIN pla_orderbook
            ON (imp_obk_instance = obk_instance AND obk_competition = cmpid)
        LEFT JOIN pla_orderbook_order
            ON (imp_obo_instance = obo_instance AND obo_competition = cmpid)) jointbl
    SET obo_orderbook = jointbl.obk_id, obo_type = 'BID'
    WHERE pla_orderbook_order.obo_id = jointbl.obo_id;
        
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Orderbook', 'addBid');
    END IF;

    CALL do_log(concat_ws(',','Updating orderbook asks for competition',cmpid));

    UPDATE pla_orderbook_order,
    (SELECT obo_id, obk_id FROM
        (SELECT imp_instance_id imp_obk_instance, convert_key(imp_arg1) imp_obo_instance
        FROM vpla_import_new 
        WHERE imp_class='org.powertac.common.Orderbook' AND imp_method='addAsk') imp
    LEFT JOIN pla_orderbook
        ON (imp_obk_instance = obk_instance AND obk_competition = cmpid)
    LEFT JOIN pla_orderbook_order
        ON (imp_obo_instance = obo_instance AND obo_competition = cmpid)) jointbl
    SET obo_orderbook = jointbl.obk_id, obo_type = 'ASK'
    WHERE pla_orderbook_order.obo_id = jointbl.obo_id;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Orderbook', 'addAsk');
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_gencos
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_gencos`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`import_gencos` (cmpid INT, mark_processed BOOLEAN)
BEGIN

    CALL do_log(concat_ws(',','Storing gencos for competition',cmpid));
    INSERT INTO pla_genco
        (gnc_instance,gnc_competition,gnc_name)
    (SELECT imp_instance_id,cmpid,imp_arg1
     FROM vpla_import_new
     WHERE (imp_class='org.powertac.genco.Genco' 
            AND imp_method='new'
            -- Call to three argument constructor triggers call to default constructor
            AND imp_arg2 IS NOT NULL));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing genco nominal capacity for competition',cmpid));
    UPDATE pla_genco,vpla_import_new
    SET gnc_nominal_capacity = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withNominalCapacity'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withNominalCapacity');
    END IF;

    CALL do_log(concat_ws(',','Storing genco variability for competition',cmpid));
    
    UPDATE pla_genco,vpla_import_new
    SET gnc_variability = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withVariability'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withVariability');
    END IF;

    CALL do_log(concat_ws(',','Storing genco reliability for competition',cmpid));
    
    UPDATE pla_genco,vpla_import_new
    SET gnc_reliability = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withReliability'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withReliability');
    END IF;

    CALL do_log(concat_ws(',','Storing genco cost for competition',cmpid));

    UPDATE pla_genco,vpla_import_new
    SET gnc_cost = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withCost'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withCost');
    END IF;

    CALL do_log(concat_ws(',','Storing genco carbon emission rate for competition',cmpid));

    UPDATE pla_genco,vpla_import_new
    SET gnc_carbon_emission_rate = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withCarbonEmissionRate'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withCarbonEmissionRate');
    END IF;

    CALL do_log(concat_ws(',','Storing genco commitment leadtime for competition',cmpid));

    UPDATE pla_genco,vpla_import_new
    SET gnc_commitment_leadtime = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Genco' 
      AND imp_method='withCommitmentLeadtime'
      AND gnc_instance = imp_instance_id
      AND gnc_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'withCommitmentLeadtime');
    END IF;

    # --- Storing genco state
    CALL do_log(concat_ws(',','Storing genco state (current capacity) for competition',cmpid));

    INSERT INTO pla_genco_state
    (gcs_genco,gcs_current_capacity,gcs_timeslot)
       (SELECT gnc_id,convert_number(imp_arg1),tsl_id
        FROM vpla_import_new,pla_genco,pla_timeslot
        WHERE (imp_class='org.powertac.genco.Genco' AND imp_method='setCurrentCapacity')
        AND gnc_instance=imp_instance_id and gnc_competition=cmpid
        AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'setCurrentCapacity');
    END IF;

    CALL do_log(concat_ws(',','Storing genco state (in operation) for competition',cmpid));

    INSERT INTO pla_genco_state
    (gcs_genco,gcs_in_operation,gcs_timeslot)
       (SELECT gnc_id,IF(imp_arg1 = 'true', TRUE, FALSE),tsl_id
        FROM vpla_import_new,pla_genco,pla_timeslot
        WHERE (imp_class='org.powertac.genco.Genco' AND imp_method='setInOperation')
        AND gnc_instance=imp_instance_id and gnc_competition=cmpid
        AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Genco', 'setInOperation');
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function convert_number
-- -----------------------------------------------------

USE `pla`;
DROP function IF EXISTS `pla`.`convert_number`;

DELIMITER $$
USE `pla`$$


CREATE FUNCTION `convert_number`(instring VARCHAR(256)) 
RETURNS DECIMAL(15,5)
BEGIN
  DECLARE pos INTEGER;
  DECLARE epos INTEGER;
  
  DECLARE s VARCHAR(256);

  DECLARE exp INTEGER;
  DECLARE base DECIMAL(15,5);

  SET s = replace(instring, 'null', '0.0');
  SET pos = INSTR(s, '.');
  SET epos = INSTR(s, 'E');

  IF(pos > 0 && epos <= 0) THEN
    SELECT CAST(substring(s, 1, pos + 5) AS DECIMAL(15,5)) INTO base;
    RETURN base;
  ELSEIF(pos > 0 && epos > 0) THEN
    SELECT substring(s, epos + 1, length(s)) INTO exp;

    IF exp > 9 THEN
        RETURN CAST('9999999999.99999' AS DECIMAL(15,5));
    ELSEIF exp < -9 THEN
        RETURN CAST('0000000000.00000' AS DECIMAL(15,5));
    ELSEIF epos > pos + 5 THEN
        RETURN concat(substring(s, 1, pos + 5),'E',exp);
    ELSE
        RETURN concat(substring(s, 1, epos - 1),'E',exp);
    END IF;    
  ELSE
    SELECT CAST(s AS DECIMAL(15,5)) INTO base;
  END IF;

  RETURN base;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_buyers
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_buyers`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`import_buyers` (cmpid INT, mark_processed BOOLEAN)
BEGIN

    INSERT INTO pla_buyer
        (buy_instance,buy_competition,buy_name)
    (SELECT imp_instance_id,cmpid,imp_arg1
    FROM vpla_import_new
    WHERE ( imp_class='org.powertac.genco.Buyer' 
            AND imp_method='new'
            -- Call to three argument constructor triggers call to default constructor
            AND imp_arg2 IS NOT NULL));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'new');
    END IF;


    CALL do_log(concat_ws(',','Storing buyer price beta for competition',cmpid));

    UPDATE pla_buyer,vpla_import_new
        SET buy_price_beta = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Buyer' 
      AND imp_method='setPriceBeta'
      AND buy_instance = imp_instance_id
      AND buy_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setPriceBeta');
    END IF;


    CALL do_log(concat_ws(',','Storing buyer Mwh for competition',cmpid));
    
    UPDATE pla_buyer,vpla_import_new
    SET buy_price_beta = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.genco.Buyer' 
      AND imp_method='setMwh'
      AND buy_instance = imp_instance_id
      AND buy_competition = cmpid;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setMwh');
    END IF;

    # --- Storing buyer state
    CALL do_log(concat_ws(',','Storing buyer state (current capacity) for competition',cmpid));
    
    INSERT INTO pla_buyer_state
        (bys_buyer,bys_current_capacity,bys_timeslot)
    (SELECT buy_id,convert_number(imp_arg1),tsl_id
     FROM vpla_import_new,pla_buyer,pla_timeslot
     WHERE (imp_class='org.powertac.genco.Buyer' AND imp_method='setCurrentCapacity')
       AND buy_instance=imp_instance_id AND buy_competition=cmpid
       AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setCurrentCapacity');
    END IF;

    CALL do_log(concat_ws(',','Storing buyer state (in operation) for competition',cmpid));

    INSERT INTO pla_buyer_state
        (bys_buyer,bys_in_operation,bys_timeslot)
    (SELECT buy_id,IF(imp_arg1 = 'true', TRUE, FALSE),tsl_id
    FROM vpla_import_new,pla_buyer,pla_timeslot
    WHERE (imp_class='org.powertac.genco.Buyer' AND imp_method='setInOperation')
    AND buy_instance=imp_instance_id AND buy_competition=cmpid
    AND tsl_serial=imp_ts AND tsl_competition=cmpid);

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.genco.Buyer', 'setInOperation');
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_tariffs
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_tariffs`;

DELIMITER $$
USE `pla`$$
CREATE PROCEDURE `pla`.`import_tariffs` (cmpid INT, mark_processed BOOLEAN, format VARCHAR(16))
BEGIN

    # --- Store tariffs and rates
    CALL do_log(concat_ws(',','Storing tariffs for competition',cmpid));

    INSERT INTO pla_tariff
        (trf_instance,trf_competition,trf_broker)
    SELECT imp_instance_id,cmpid,bkr_id FROM
        (SELECT imp_instance_id,
                convert_key(imp_arg1) imp_bkr_instance
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.TariffSpecification' AND imp_method='new'        
        GROUP BY imp_instance_id,imp_bkr_instance) imp
        LEFT JOIN pla_broker
            ON (bkr_instance=imp_bkr_instance AND bkr_competition=cmpid);

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.TariffSpecification', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing rates for competition',cmpid));

    # -- Rates Part 1: Rates created using the default constructor and "set/with" methods
    # --               These appear to be mainly used by the default broker
    INSERT INTO pla_rate
        (rte_instance,rte_competition)
    (SELECT imp_instance_id,cmpid
    FROM vpla_import_new
    WHERE imp_class='org.powertac.common.Rate' AND imp_method='new' and imp_arg1 IS NULL);

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        UPDATE pla_import
            SET imp_target = cmpid
            WHERE imp_class = 'org.powertac.common.Rate' AND imp_method = 'new' 
                    AND imp_arg1 IS NULL AND imp_target IS NULL;
    END IF;

    CALL do_log(concat_ws(',','Storing rate values for competition',cmpid));
    
    UPDATE pla_rate,vpla_import_new
    SET rte_min_value = convert_number(imp_arg1),
        rte_max_value = convert_number(imp_arg1),
        rte_expected_mean = convert_number(imp_arg1)
    WHERE imp_class='org.powertac.common.Rate' 
      AND imp_method='withValue'
      AND rte_instance = imp_instance_id
      AND rte_competition = cmpid;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Rate', 'withValue');
    END IF;

    CALL do_log(concat_ws(',','Storing rate parent tariffs for competition',cmpid));

    UPDATE pla_rate,pla_tariff,vpla_import_new
    SET rte_tariff = trf_id
    WHERE imp_class='org.powertac.common.Rate' 
      AND imp_method='setTariffId'
      AND rte_instance = imp_instance_id
      AND rte_competition = cmpid
      AND trf_instance = convert_key(imp_arg1)
      AND trf_competition = cmpid;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.Rate', 'setTariffId');
    END IF;

    # -- Rates Part 2: Rates created using the full constructor. Used by all other brokers.
    INSERT INTO pla_rate
        (rte_instance,rte_competition,rte_tariff,
         rte_weekly_begin,rte_weekly_end,rte_daily_begin,rte_daily_end,
         rte_tier_threshold,rte_fixed,rte_min_value,rte_max_value,
         rte_notice_interval,rte_expected_mean,rte_max_curtailment)
    SELECT imp_instance_id,cmpid,trf_id,rte_weekly_begin,rte_weekly_end,
         rte_daily_begin,rte_daily_end,rte_tier_threshold,rte_fixed,
         rte_min_value,rte_max_value,rte_notice_interval,rte_expected_mean,
         rte_max_curtailment
    FROM
        (SELECT imp_instance_id,
            convert_key(imp_arg1) imp_trf_instance,
            convert_number(imp_arg2) rte_weekly_begin,
            convert_number(imp_arg3) rte_weekly_end,
            convert_number(imp_arg4) rte_daily_begin,
            convert_number(imp_arg5) rte_daily_end,
            convert_number(imp_arg6) rte_tier_threshold,
            IF(STRCMP(imp_arg7, 'true'), 1, 0) rte_fixed,
            convert_number(imp_arg8) rte_min_value,
            convert_number(imp_arg9) rte_max_value,
            convert_number(imp_arga) rte_notice_interval,
            convert_number(imp_argb) rte_expected_mean,
            convert_number(imp_argc) rte_max_curtailment
        FROM vpla_import_new
        WHERE imp_class='org.powertac.common.Rate' AND imp_method='new' and imp_arg1 IS NOT NULL) imp
        LEFT JOIN pla_tariff
            ON (trf_instance = imp_trf_instance AND trf_competition = cmpid);

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        UPDATE pla_import
            SET imp_target = cmpid
            WHERE imp_class = 'org.powertac.common.Rate' AND imp_method = 'new' 
                    AND imp_arg1 IS NOT NULL AND imp_target IS NULL;
    END IF;

    # --- Store hourly charges
    CALL do_log(concat_ws(',','Storing hourly charges for competition',cmpid));
    
    # -- First insert all the hourly charges. This is very inconveniently modelled
    # -- in the state logs. On the one hand we have hourly charges with references
    # -- to their parnet rates; and on the other hand we have those without reference,
    # -- which are then followed by a VariableRateUpdate used to connect them.

    # -- So first for the complete hourly charges:
    INSERT INTO pla_hourly_charge
        (hcg_instance,hcg_rate,hcg_timeslot,hcg_value)
    SELECT imp_instance_id,rte_id,tsl_id,hcg_value
    FROM
        (SELECT imp_instance_id,
            convert_key(imp_arg1) imp_rte_instance,
            convert_number(imp_arg2) hcg_value,
            imp_arg3 imp_tsl_timestring
         FROM vpla_import_new
         WHERE imp_class='org.powertac.common.HourlyCharge' 
            AND imp_method='new'
            AND convert_number(imp_arg1) > -1) imp         
    LEFT JOIN pla_rate
        ON (rte_instance = imp_rte_instance AND rte_competition = cmpid)  
    -- Note the inner join on timestring; some brokers may specify
    -- validity times that are not at the hour boundaries. We ignore those
    -- here
    JOIN pla_timeslot
        ON (tsl_timestring = imp_tsl_timestring AND tsl_competition = cmpid);

    # -- And now for the incomplete hourly charges:
    INSERT INTO pla_hourly_charge
        (hcg_instance,hcg_rate,hcg_timeslot,hcg_value)
    SELECT hcg_instance,rte_id,tsl_id,hcg_value
    FROM
        (SELECT imp_instance_id hcg_instance,
            imp_arg1 imp_hcg_rate_id,
            convert_number(imp_arg2) hcg_value,
            imp_arg3 imp_tsl_timestring
         FROM vpla_import_new
         WHERE imp_class='org.powertac.common.HourlyCharge' 
            AND imp_method='new'
            AND convert_number(imp_arg1) = -1) imp_hcg
        -- Note the inner join on timestring; some brokers may specify
        -- validity times that are not at the hour boundaries. We ignore those
        -- here
        JOIN pla_timeslot
            ON (tsl_timestring = imp_hcg.imp_tsl_timestring AND tsl_competition = cmpid),
        (SELECT convert_key(imp_arg3) vru_hcg_instance,
            convert_key(imp_arg4) imp_rte_instance
         FROM vpla_import_new
         WHERE imp_class='org.powertac.common.msg.VariableRateUpdate' 
            AND imp_method='new') imp_vru   
        LEFT JOIN pla_rate
            ON (rte_instance = imp_rte_instance AND rte_competition = cmpid)  
    WHERE hcg_instance = vru_hcg_instance;

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.HourlyCharge', 'new');
        CALL mark_as_processed(cmpid, 'org.powertac.common.msg.VariableRateUpdate', 'new');   
    END IF;

    # --- Store tariff transactions
    CALL do_log(concat_ws(',','Storing tariff transactions for competition',cmpid));

	IF format = '2012' THEN
		-- The 2012 format works with joins on timestrings
		INSERT INTO pla_tariff_transaction
			(tfc_instance,tfc_customer,tfc_tariff,tfc_timeslot,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type)
		SELECT imp_instance_id,cst_id,trf_id,tsl_id,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type
		FROM
			(SELECT imp_instance_id,
				convert_key(imp_arg6) tfc_customer_count,
				convert_number(imp_arg7) tfc_kwh,
				convert_number(imp_arg8) tfc_charge,
				imp_arg3 tfc_type,
				convert_key(imp_arg4) imp_trf_instance,
				imp_arg2 imp_tsl_timestring,
				convert_key(imp_arg5) imp_cst_instance
			 FROM vpla_import_new
			 WHERE imp_class='org.powertac.common.TariffTransaction' 
				AND imp_method='new'
				AND imp_arg3 IN ('CONSUME','PRODUCE','SIGNUP','WITHDRAW','PERIODIC','PUBLISH')) imp
		LEFT JOIN pla_tariff
			ON (trf_instance = imp_trf_instance AND trf_competition = cmpid)
		LEFT JOIN pla_timeslot
		    ON (tsl_timestring = imp_tsl_timestring AND tsl_competition = cmpid)
		LEFT JOIN pla_customer
			ON (cst_instance = imp_cst_instance AND cst_competition = cmpid);
	ELSE
		INSERT INTO pla_tariff_transaction
			(tfc_instance,tfc_customer,tfc_tariff,tfc_timeslot,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type)
		SELECT imp_instance_id,cst_id,trf_id,tsl_id,tfc_customer_count,tfc_kwh,tfc_charge,tfc_type
		FROM
			(SELECT imp_instance_id,
				convert_key(imp_arg6) tfc_customer_count,
				convert_number(imp_arg7) tfc_kwh,
				convert_number(imp_arg8) tfc_charge,
				imp_arg3 tfc_type,
				convert_key(imp_arg4) imp_trf_instance,
				convert_number(imp_arg2) imp_tsl_serial,
				convert_key(imp_arg5) imp_cst_instance
			 FROM vpla_import_new
			 WHERE imp_class='org.powertac.common.TariffTransaction' 
				AND imp_method='new') imp
		LEFT JOIN pla_tariff
			ON (trf_instance = imp_trf_instance AND trf_competition = cmpid)
		LEFT JOIN pla_timeslot
			ON (tsl_serial = imp_tsl_serial AND tsl_competition = cmpid)
		LEFT JOIN pla_customer
			ON (cst_instance = imp_cst_instance AND cst_competition = cmpid);
		-- WHERE 
			-- see Github Issue 565
			-- trf_id IS NOT NULL;
	END IF;

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.TariffTransaction', 'new');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_customers
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_customers`;

DELIMITER $$
USE `pla`$$
CREATE PROCEDURE `pla`.`import_customers` (cmpid INT, mark_processed BOOLEAN)
BEGIN
    # --- Store customer info
    CALL do_log(concat_ws(',','Storing customers for competition',cmpid));

    INSERT INTO pla_customer
        (cst_competition,cst_instance,cst_name,cst_population)
    (SELECT cmpid,imp_instance_id,imp_arg1,CAST(imp_arg2 AS UNSIGNED)
     FROM vpla_import_new
     WHERE imp_class='org.powertac.common.CustomerInfo' 
     AND imp_method='new');
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'new');
    END IF;

    CALL do_log(concat_ws(',','Storing customer power types',cmpid));

    UPDATE pla_customer,vpla_import_new
    SET cst_powertype = imp_arg1
    WHERE imp_class='org.powertac.common.CustomerInfo' 
      AND imp_method='withPowerType'
      AND cst_instance = imp_instance_id
      AND cst_competition = cmpid;
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'withPowerType');
    END IF;

    CALL do_log(concat_ws(',','Storing customer multi contracting',cmpid));
    
    UPDATE pla_customer,vpla_import_new
    SET cst_multi_contracting = IF(imp_arg1='true', TRUE, FALSE)
    WHERE imp_class='org.powertac.common.CustomerInfo' 
      AND imp_method='withMultiContracting'
      AND cst_instance = imp_instance_id
      AND cst_competition = cmpid;
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'withMultiContracting');
    END IF;

    CALL do_log(concat_ws(',','Storing customer negotiation capability',cmpid));
    
    UPDATE pla_customer,vpla_import_new
    SET cst_multi_contracting = IF(imp_arg1='true', TRUE, FALSE)
    WHERE imp_class='org.powertac.common.CustomerInfo' 
      AND imp_method='withCanNegotiate'
      AND cst_instance = imp_instance_id
      AND cst_competition = cmpid;
  
    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.CustomerInfo', 'withCanNegotiate');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure import_market_positions
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`import_market_positions`;

DELIMITER $$
USE `pla`$$
CREATE PROCEDURE `pla`.`import_market_positions` (cmpid INT, mark_processed BOOLEAN, format VARCHAR(16))
BEGIN

    # Market positions in Power TAC relate to a broker. Since gencos and buyers
    # are also brokers in Power TAC parlance, we need to connect the market positions
    # with one out of three separate tables.
    CALL do_log(concat_ws(',','Storing market positions for competition',cmpid));

	IF format = "2012" THEN
		INSERT INTO pla_market_position
			(mpo_instance,mpo_competition,mpo_broker,mpo_genco,mpo_buyer,mpo_timeslot,mpo_initial)
		SELECT imp_instance_id, cmpid, bkr_id, gnc_id, buy_id, tsl_id, imp_mpo_initial
		FROM
			(SELECT 
				imp_instance_id,           
				convert_number(imp_arg3) imp_mpo_initial,
				convert_key(imp_arg1) imp_entity_instance,
				convert_key(imp_arg2) imp_tsl_instance
			 FROM vpla_import_new
			 WHERE imp_class='org.powertac.common.MarketPosition' AND imp_method='new') imp
		LEFT JOIN tmp_tsl_instance_map
			ON imp_tsl_instance = tslmap_instance
		LEFT JOIN pla_broker
			ON bkr_instance=imp_entity_instance AND bkr_competition=cmpid
		LEFT JOIN pla_genco
			ON gnc_instance=imp_entity_instance AND gnc_competition=cmpid
		LEFT JOIN pla_buyer
			ON buy_instance=imp_entity_instance AND buy_competition=cmpid
		LEFT JOIN pla_timeslot
			ON tsl_serial=tslmap_serial AND tsl_competition=cmpid;
	ELSE
		INSERT INTO pla_market_position
			(mpo_instance,mpo_competition,mpo_broker,mpo_genco,mpo_buyer,mpo_timeslot,mpo_initial)
		SELECT imp_instance_id, cmpid, bkr_id, gnc_id, buy_id, tsl_id, imp_mpo_initial
		FROM
			(SELECT 
				imp_instance_id,           
				convert_number(imp_arg3) imp_mpo_initial,
				convert_key(imp_arg1) imp_entity_instance,
				convert_key(imp_arg2) imp_tsl_serial
			 FROM vpla_import_new
			 WHERE imp_class='org.powertac.common.MarketPosition' AND imp_method='new') imp
		LEFT JOIN pla_broker
			ON bkr_instance=imp_entity_instance AND bkr_competition=cmpid
		LEFT JOIN pla_genco
			ON gnc_instance=imp_entity_instance AND gnc_competition=cmpid
		LEFT JOIN pla_buyer
			ON buy_instance=imp_entity_instance AND buy_competition=cmpid
		LEFT JOIN pla_timeslot
			ON tsl_serial=imp_tsl_serial AND tsl_competition=cmpid;
	END IF;
  
    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.MarketPosition', 'new');
    END IF;


    CALL do_log(concat_ws(',','Storing market position changes for competition',cmpid));

    INSERT INTO pla_market_position_update
        (mpu_market_position,mpu_mwh,mpu_timeslot)
    SELECT mpo_id, convert_number(imp_arg1) imp_mwh, tsl_id
        FROM vpla_import_new
    LEFT JOIN pla_timeslot
        ON (tsl_serial=imp_ts AND tsl_competition=cmpid)
    LEFT JOIN pla_market_position
        ON mpo_instance=imp_instance_id AND mpo_competition=cmpid
    WHERE imp_class='org.powertac.common.MarketPosition' AND imp_method='updateBalance';

    CALL do_log(concat_ws(',','Rows affected:',ROW_COUNT()));

    IF mark_processed THEN
        CALL mark_as_processed(cmpid, 'org.powertac.common.MarketPosition', 'updateBalance');
    END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function convert_key
-- -----------------------------------------------------

USE `pla`;
DROP function IF EXISTS `pla`.`convert_key`;

DELIMITER $$
USE `pla`$$
CREATE FUNCTION `convert_key`(instring VARCHAR(256)) 
RETURNS INT
BEGIN

  DECLARE pos INTEGER;
  DECLARE s VARCHAR(256);

  IF(instring = 'null') THEN
    RETURN NULL;
  ELSE
    RETURN CAST(TRIM(instring) AS UNSIGNED INTEGER);
  END IF;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function test_import
-- -----------------------------------------------------

USE `pla`;
DROP function IF EXISTS `pla`.`test_import`;

DELIMITER $$
USE `pla`$$


CREATE FUNCTION `pla`.`test_import` (cmpid INT)
RETURNS VARCHAR(256)
BEGIN

    DECLARE cnt,cntimp INTEGER;
    DECLARE minval,maxval,minvalimp,maxvalimp DECIMAL(15,5);

    DECLARE dres,dres2,dresimp,dres2imp DECIMAL(15,5);

    -- -----------------------------------------------------------------------------
    --  Test competition and its properties
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*) 
    INTO cnt
    FROM pla.pla_competition 
    WHERE cmp_id = cmpid;
        
    IF cnt != 1 THEN
        RETURN concat_ws(',','Failure in test','pla_competition');
    END IF;
      
    SELECT COUNT(*) 
    INTO cnt
    FROM pla.pla_competition_properties
    WHERE cpr_id = cmpid;

    IF cnt = 0 THEN
        RETURN concat_ws(',','Failure in test','pla_competition_properties');
    END IF;

    -- -----------------------------------------------------------------------------
    --  Test competition and its properties
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*), MIN(tsl_serial), MAX(tsl_serial) 
    INTO cnt, minval, maxval
    FROM pla_timeslot 
    WHERE tsl_competition = cmpid;

    SELECT COUNT(*), MIN(convert_key(imp_arg1)), MAX(convert_key(imp_arg1))
    INTO cntimp, minvalimp, maxvalimp
    FROM pla_import
    WHERE imp_class='org.powertac.common.Timeslot' AND imp_method='new';

    IF cnt != cntimp OR minval != minvalimp OR maxval != maxvalimp THEN
        RETURN concat_ws(',','Failure in test','pla_timeslot');
    END IF;

    -- -----------------------------------------------------------------------------
    --  Test balancing transactions
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*),SUM(btr_kwh), SUM(btr_charge) 
    INTO cnt,dres,dres2
    FROM pla_balancing_transaction 
    WHERE btr_competition=cmpid;

    SELECT COUNT(*),SUM(imp_arg3),SUM(imp_arg4) 
    INTO cntimp,dresimp,dres2imp
    FROM pla_import
    WHERE imp_class='org.powertac.common.BalancingTransaction' AND imp_method='new';

    IF cnt != cntimp OR aeq(dres,dresimp) OR aeq(dres2,dres2imp) THEN
        RETURN concat_ws(',','Failure in test','pla_balancing_transaction');
    END IF;

    -- -----------------------------------------------------------------------------
    --  Test market transactions
    -- -----------------------------------------------------------------------------

    SELECT COUNT(*),SUM(mtr_mwh), SUM(mtr_price) 
    INTO cnt,dres,dres2
    FROM pla_market_transaction 
    WHERE mtr_competition=cmpid;

    SELECT COUNT(*),SUM(imp_arg4),SUM(imp_arg5) 
    INTO cntimp,dresimp,dres2imp
    FROM pla_import
    WHERE imp_class='org.powertac.common.MarketTransaction' AND imp_method='new';

    IF cnt != cntimp OR aeq(dres,dresimp) OR aeq(dres2,dres2imp) THEN
        RETURN concat_ws(',','Failure in test','pla_market_transaction');
    END IF;

    RETURN "OK";

-- SELECT * FROM pla_customer;
-- SELECT * FROM pla_broker;
-- SELECT * FROM pla_genco;
-- SELECT * FROM pla_genco_state;
-- SELECT * FROM pla_tariff;
-- SELECT * FROM pla_rate;
-- SELECT * FROM pla_tariff_transaction;
-- SELECT * FROM pla_weather_forecast;
-- SELECT * FROM pla_weather_forecast_data;
-- SELECT * FROM pla_weather_report;
-- SELECT * FROM pla_order;
-- SELECT * FROM pla_orderbook;
-- SELECT * FROM pla_orderbook_order;

-- SELECT * FROM pla_cash_position;
-- SELECT * FROM pla_cash_position_deposit;
-- SELECT * FROM pla_bank_transaction;
-- SELECT * FROM pla_buyer;
-- SELECT * FROM pla_buyer_state;
-- SELECT * FROM pla_distribution_transaction;

-- SELECT * FROM pla_market_position;
-- SELECT * FROM pla_market_position_update;
-- SELECT * FROM pla_order;
-- SELECT * FROM pla_cleared_trade;


END$$

DELIMITER ;

-- -----------------------------------------------------
-- function bootstrap_length
-- -----------------------------------------------------

USE `pla`;
DROP function IF EXISTS `pla`.`bootstrap_length`;

DELIMITER $$
USE `pla`$$


CREATE FUNCTION `pla`.`bootstrap_length` (cmpid INT)
RETURNS INT
BEGIN
    DECLARE start INT;

    SELECT SUM(timeslots) offset 
    INTO start
    FROM
        (SELECT
            CAST(AVG(pla.convert_number(cpr_value)) AS UNSIGNED INTEGER) timeslots
        FROM pla_competition_properties
        WHERE
            cpr_competition = cmpid AND
            cpr_name IN ('BootstrapTimeslotCount', 'BootstrapDiscardedTimeslots')
        GROUP BY cpr_name) a;
    
    RETURN start;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure delete_competition
-- -----------------------------------------------------

USE `pla`;
DROP procedure IF EXISTS `pla`.`delete_competition`;

DELIMITER $$
USE `pla`$$


CREATE PROCEDURE `pla`.`delete_competition` (cmpid INT)
BEGIN

    DELETE FROM pla_balancing_transaction WHERE btr_competition = cmpid;
    DELETE FROM pla_bank_transaction WHERE bnt_competition = cmpid;
    DELETE FROM pla_cash_position WHERE cpo_competition = cmpid;
    DELETE FROM pla_cleared_trade WHERE clt_competition = cmpid;
    DELETE FROM pla_distribution_transaction WHERE dtr_competition = cmpid;
    DELETE FROM pla_market_position WHERE mpo_competition = cmpid;
    DELETE FROM pla_market_transaction WHERE mtr_competition = cmpid;
    DELETE FROM pla_order WHERE ord_competition = cmpid;
    DELETE FROM pla_orderbook WHERE obk_competition = cmpid;
    DELETE FROM pla_tariff WHERE trf_competition = cmpid;
    DELETE FROM pla_weather_forecast WHERE wfc_competition = cmpid;
    DELETE FROM pla_weather_report WHERE wre_competition = cmpid;
    DELETE FROM pla_customer WHERE cst_competition = cmpid;
    DELETE FROM pla_buyer WHERE buy_competition = cmpid;
    DELETE FROM pla_genco WHERE gnc_competition = cmpid;
    DELETE FROM pla_broker WHERE bkr_competition = cmpid;
    DELETE FROM pla_timeslot WHERE tsl_competition = cmpid;
    DELETE FROM pla_competition_properties WHERE cpr_competition = cmpid;
    DELETE FROM pla_competition WHERE cmp_id = cmpid;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- function aeq
-- -----------------------------------------------------

USE `pla`;
DROP function IF EXISTS `pla`.`aeq`;

DELIMITER $$
USE `pla`$$


CREATE FUNCTION `pla`.`aeq` (v1 DECIMAL, v2 DECIMAL)
RETURNS BOOLEAN
BEGIN

    DECLARE eps DECIMAL(5,5) DEFAULT 0.1;

    IF v1 > v2 + eps OR v1 < v2 - eps THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `pla`.`vpla_import_new`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_import_new` ;
DROP TABLE IF EXISTS `pla`.`vpla_import_new`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_import_new` AS
SELECT
    imp_id, imp_ts, imp_time, imp_class, imp_instance_id, imp_method, imp_arg1, imp_arg2, imp_arg3, imp_arg4, imp_arg5, imp_arg6, imp_arg7, imp_arg8, imp_arg9, imp_arga, imp_argb, imp_argc, imp_argd, imp_target
FROM 
    pla.pla_import 
WHERE 
    imp_target IS NULL;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_balancing_transaction`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_balancing_transaction` ;
DROP TABLE IF EXISTS `pla`.`vpla_balancing_transaction`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_balancing_transaction` AS
SELECT 
    btr_id, btr_instance, btr_competition, btr_broker, btr_timeslot, btr_kwh, btr_charge
FROM 
    pla_balancing_transaction;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_bank_transaction`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_bank_transaction` ;
DROP TABLE IF EXISTS `pla`.`vpla_bank_transaction`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_bank_transaction` AS
SELECT
    bnt_id, bnt_instance, bnt_competition, bnt_broker, bnt_genco, bnt_buyer, bnt_timeslot, bnt_amount
FROM 
    pla_bank_transaction;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_broker`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_broker` ;
DROP TABLE IF EXISTS `pla`.`vpla_broker`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_broker` AS
SELECT
    bkr_id, bkr_instance, bkr_competition, bkr_name
FROM 
    pla_broker;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_buyer`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_buyer` ;
DROP TABLE IF EXISTS `pla`.`vpla_buyer`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_buyer` AS
SELECT
    buy_id, buy_instance, buy_competition, buy_name, buy_price_beta, buy_mwh
FROM
    pla_buyer;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_buyer_state`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_buyer_state` ;
DROP TABLE IF EXISTS `pla`.`vpla_buyer_state`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_buyer_state` AS
SELECT
    bys_id, bys_buyer, bys_timeslot, bys_current_capacity, bys_in_operation
FROM
    pla_buyer_state;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_cash_position`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_cash_position` ;
DROP TABLE IF EXISTS `pla`.`vpla_cash_position`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_cash_position` AS
SELECT
   cpo_id, cpo_instance, cpo_timeslot, cpo_competition, cpo_broker, cpo_genco, cpo_buyer, cpo_balance
FROM
    pla_cash_position;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_cleared_trade`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_cleared_trade` ;
DROP TABLE IF EXISTS `pla`.`vpla_cleared_trade`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_cleared_trade` AS
SELECT
    clt_id, clt_instance, clt_competition, clt_timeslot, clt_execution_mwh, clt_execution_price, clt_date_executed
FROM
    pla_cleared_trade;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_competition`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_competition` ;
DROP TABLE IF EXISTS `pla`.`vpla_competition`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_competition` AS
SELECT
    cmp_id, cmp_name
FROM
    pla_competition;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_competition_properties`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_competition_properties` ;
DROP TABLE IF EXISTS `pla`.`vpla_competition_properties`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_competition_properties` AS
SELECT
    cpr_id, cpr_name, cpr_value, cpr_competition
FROM
    pla_competition_properties;

-- -----------------------------------------------------
-- View `pla`.`vpla_customer`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_customer` ;
DROP TABLE IF EXISTS `pla`.`vpla_customer`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_customer` AS
SELECT
    cst_id, cst_competition, cst_instance, cst_population, cst_name, cst_powertype, cst_multi_contracting, cst_can_negotiate
FROM
    pla_customer;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_distribution_transaction`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_distribution_transaction` ;
DROP TABLE IF EXISTS `pla`.`vpla_distribution_transaction`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_distribution_transaction` AS
SELECT
    dtr_id, dtr_instance, dtr_competition, dtr_broker, dtr_timeslot, dtr_kwh, dtr_charge
FROM
    pla_distribution_transaction;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_genco`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_genco` ;
DROP TABLE IF EXISTS `pla`.`vpla_genco`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_genco` AS
SELECT
    gnc_id, gnc_instance, gnc_competition, gnc_name, gnc_nominal_capacity, gnc_variability, gnc_reliability, gnc_cost, gnc_carbon_emission_rate, gnc_commitment_leadtime
FROM
    pla_genco;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_genco_state`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_genco_state` ;
DROP TABLE IF EXISTS `pla`.`vpla_genco_state`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_genco_state` AS
SELECT
    gcs_id, gcs_genco, gcs_timeslot, gcs_current_capacity, gcs_in_operation
FROM
    pla_genco_state;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_import`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_import` ;
DROP TABLE IF EXISTS `pla`.`vpla_import`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_import` AS
SELECT
    imp_id, imp_ts, imp_time, imp_class, imp_instance_id, imp_method, imp_arg1, imp_arg2, imp_arg3, imp_arg4, imp_arg5, imp_arg6, imp_arg7, imp_arg8, imp_arg9, imp_arga, imp_argb, imp_argc, imp_argd imp_target
FROM
    pla_import;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_market_position`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_market_position` ;
DROP TABLE IF EXISTS `pla`.`vpla_market_position`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_market_position` AS
SELECT
    mpo_id, mpo_instance, mpo_competition, mpo_broker, mpo_genco, mpo_buyer, mpo_timeslot, mpo_initial
FROM
    pla_market_position;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_market_position_update`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_market_position_update` ;
DROP TABLE IF EXISTS `pla`.`vpla_market_position_update`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_market_position_update` AS
SELECT
    mpu_id, mpu_market_position, mpu_timeslot, mpu_mwh
FROM
    pla_market_position_update;

-- -----------------------------------------------------
-- View `pla`.`vpla_market_transaction`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_market_transaction` ;
DROP TABLE IF EXISTS `pla`.`vpla_market_transaction`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_market_transaction` AS
SELECT
    mtr_id, mtr_instance, mtr_competition, mtr_broker, mtr_genco, mtr_buyer, mtr_order_timeslot, mtr_delivery_timeslot, mtr_mwh, mtr_price
FROM
    pla_market_transaction;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_order`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_order` ;
DROP TABLE IF EXISTS `pla`.`vpla_order`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_order` AS
SELECT
    ord_id, ord_instance, ord_competition, ord_broker, ord_genco, ord_buyer, ord_timeslot, ord_timeslot_placed, ord_mwh, ord_limit
FROM
    pla_order;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_orderbook`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_orderbook` ;
DROP TABLE IF EXISTS `pla`.`vpla_orderbook`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_orderbook` AS
SELECT
    obk_id, obk_instance, obk_competition, obk_timeslot, obk_clearing_price, obk_date_executed
FROM
    pla_orderbook;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_orderbook_order`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_orderbook_order` ;
DROP TABLE IF EXISTS `pla`.`vpla_orderbook_order`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_orderbook_order` AS
SELECT
    obo_id, obo_instance, obo_competition, obo_orderbook, obo_mwh, obo_limit_price, obo_type
FROM
    pla_orderbook_order;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_rate`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_rate` ;
DROP TABLE IF EXISTS `pla`.`vpla_rate`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_rate` AS
SELECT
    rte_id, rte_instance, rte_competition, rte_tariff, 
    rte_weekly_begin, rte_weekly_end, rte_daily_begin, rte_daily_end,
    rte_tier_threshold, rte_fixed, rte_min_value, rte_max_value,
    rte_notice_interval, rte_expected_mean, rte_max_curtailment
FROM
    pla_rate;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_tariff`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_tariff` ;
DROP TABLE IF EXISTS `pla`.`vpla_tariff`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_tariff` AS
SELECT
    trf_id, trf_instance, trf_broker, trf_competition
FROM
    pla_tariff;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_tariff_transaction`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_tariff_transaction` ;
DROP TABLE IF EXISTS `pla`.`vpla_tariff_transaction`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_tariff_transaction` AS
SELECT
    tfc_id, tfc_instance, tfc_customer, tfc_tariff, tfc_timeslot, tfc_customer_count, tfc_kwh, tfc_charge, tfc_type
FROM
    pla_tariff_transaction;

-- -----------------------------------------------------
-- View `pla`.`vpla_timeslot`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_timeslot` ;
DROP TABLE IF EXISTS `pla`.`vpla_timeslot`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_timeslot` AS
SELECT
    tsl_id, NULL AS tsl_instance, tsl_competition, tsl_serial, tsl_time, tsl_timestring
FROM
    pla_timeslot;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_weather_forecast`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_weather_forecast` ;
DROP TABLE IF EXISTS `pla`.`vpla_weather_forecast`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_weather_forecast` AS
SELECT
    wfc_id, wfc_instance, wfc_competition, wfc_timeslot
FROM
    pla_weather_forecast;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_weather_forecast_data`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_weather_forecast_data` ;
DROP TABLE IF EXISTS `pla`.`vpla_weather_forecast_data`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_weather_forecast_data` AS
SELECT
    wfd_id, wfd_forecast, wfd_instance, wfd_offset, wfd_temperature, wfd_wind_speed, wfd_wind_direction, wfd_cloud_cover
FROM
    pla_weather_forecast_data;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_weather_report`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_weather_report` ;
DROP TABLE IF EXISTS `pla`.`vpla_weather_report`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_weather_report` AS
SELECT
    wre_id, wre_instance, wre_competition, wre_timeslot, wre_temperature, wre_wind_speed, wre_wind_direction, wre_cloud_cover
FROM
    pla_weather_report;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_hourly_charge`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_hourly_charge` ;
DROP TABLE IF EXISTS `pla`.`vpla_hourly_charge`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_hourly_charge` AS
SELECT hcg_id, hcg_instance, hcg_rate, hcg_timeslot, hcg_value
FROM pla_hourly_charge;
;

-- -----------------------------------------------------
-- View `pla`.`vpla_balancing_order`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_balancing_order` ;
DROP TABLE IF EXISTS `pla`.`vpla_balancing_order`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_balancing_order` AS
SELECT
    bor_id, bor_instance, bor_tariff, bor_timeslot, bor_exercise_ratio, bor_price
FROM 
    pla_balancing_order;

-- -----------------------------------------------------
-- View `pla`.`vpla_balancing_control_event`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `pla`.`vpla_balancing_control_event` ;
DROP TABLE IF EXISTS `pla`.`vpla_balancing_control_event`;
USE `pla`;
CREATE  OR REPLACE VIEW `pla`.`vpla_balancing_control_event` AS
SELECT
    bce_id, bce_instance, bce_tariff, bce_timeslot, bce_kwh, bce_payment
FROM
    pla_balancing_control_event;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO pla_admin;
 DROP USER pla_admin;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'pla_admin' IDENTIFIED BY 'pla_admin';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`convert_key` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'pla_admin';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'pla_admin';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'pla_admin';
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE `pla`.`pla_cleared_trade` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_balancing_transaction` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_bank_transaction` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_broker` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_buyer` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_buyer_state` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_cash_position` TO 'pla_admin';
GRANT DELETE, UPDATE, SELECT, INSERT ON TABLE `pla`.`pla_competition` TO 'pla_admin';
GRANT DELETE, UPDATE, SELECT, INSERT ON TABLE `pla`.`pla_competition_properties` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_customer` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_distribution_transaction` TO 'pla_admin';
GRANT DELETE, INSERT, UPDATE, SELECT ON TABLE `pla`.`pla_genco` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_genco_state` TO 'pla_admin';
GRANT DELETE, INSERT, UPDATE, SELECT, DROP ON TABLE `pla`.`pla_import` TO 'pla_admin';
GRANT DELETE, INSERT, UPDATE, SELECT ON TABLE `pla`.`pla_market_position` TO 'pla_admin';
GRANT DELETE, UPDATE, SELECT, INSERT ON TABLE `pla`.`pla_market_position_update` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_market_transaction` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_order` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_orderbook` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_orderbook_order` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_rate` TO 'pla_admin';
GRANT DELETE, UPDATE, INSERT, SELECT ON TABLE `pla`.`pla_tariff` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_tariff_transaction` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_timeslot` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_weather_forecast` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_weather_forecast_data` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_weather_report` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`vpla_import` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`vpla_import_new` TO 'pla_admin';
GRANT ALL ON procedure `pla`.`delete_competition` TO 'pla_admin';
GRANT ALL ON procedure `pla`.`do_log` TO 'pla_admin';
GRANT ALL ON procedure `pla`.`import` TO 'pla_admin';
GRANT ALL ON procedure `pla`.`import_buyers` TO 'pla_admin';
GRANT ALL ON procedure `pla`.`import_customers` TO 'pla_admin';
GRANT ALL ON function `pla`.`test_import` TO 'pla_admin';
GRANT ALL ON function `pla`.`convert_key` TO 'pla_admin';
GRANT ALL ON function `pla`.`bootstrap_length` TO 'pla_admin';
GRANT ALL ON function `pla`.`aeq` TO 'pla_admin';
GRANT ALL ON function `pla`.`convert_number` TO 'pla_admin';
GRANT ALL ON function `pla`.`convert_timestamp` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_hourly_charge` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_balancing_control_event` TO 'pla_admin';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `pla`.`pla_balancing_order` TO 'pla_admin';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO pla_analysis;
 DROP USER pla_analysis;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'pla_analysis' IDENTIFIED BY 'pla_analysis';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'pla_analysis';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'pla_analysis';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'pla_analysis';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'pla_analysis';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'pla_analysis';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO wketter;
 DROP USER wketter;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'wketter' IDENTIFIED BY 'e23UchmZGLMW';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'wketter';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'wketter';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'wketter';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'wketter';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'wketter';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO jcollins;
 DROP USER jcollins;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'jcollins' IDENTIFIED BY 'NgKB7o9XZRwj';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'jcollins';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'jcollins';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'jcollins';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'jcollins';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'jcollins';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mradulescu;
 DROP USER mradulescu;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mradulescu' IDENTIFIED BY 'CACEc78RHCJZ';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'mradulescu';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'mradulescu';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'mradulescu';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'mradulescu';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'mradulescu';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO dproesch;
 DROP USER dproesch;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'dproesch' IDENTIFIED BY '3Ai8PptfLBHC';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'dproesch';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'dproesch';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'dproesch';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'dproesch';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'dproesch';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO rnanoha;
 DROP USER rnanoha;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'rnanoha' IDENTIFIED BY 'pR7agt3TbGmY';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'rnanoha';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'rnanoha';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'rnanoha';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'rnanoha';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'rnanoha';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO jbabic;
 DROP USER jbabic;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'jbabic' IDENTIFIED BY '7ohVfWiu8KYy';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'jbabic';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'jbabic';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'jbabic';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'jbabic';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'jbabic';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO duriely;
 DROP USER duriely;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'duriely' IDENTIFIED BY 'Akuo47CuheKn';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'duriely';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'duriely';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'duriely';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'duriely';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'duriely';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO durieli;
 DROP USER durieli;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'durieli' IDENTIFIED BY 'Akuo47CuheKn';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'durieli';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'durieli';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'durieli';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'durieli';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'durieli';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO nvilarosado;
 DROP USER nvilarosado;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'nvilarosado' IDENTIFIED BY 'FC3qmxc6JigM';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'nvilarosado';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'nvilarosado';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'nvilarosado';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'nvilarosado';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'nvilarosado';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO rtallakuate;
 DROP USER rtallakuate;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'rtallakuate' IDENTIFIED BY 'bR8cxbZiPD8k';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'rtallakuate';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'rtallakuate';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'rtallakuate';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'rtallakuate';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'rtallakuate';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO jserrano;
 DROP USER jserrano;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'jserrano' IDENTIFIED BY 'xmZ3akmbKj3Z';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'jserrano';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'jserrano';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'jserrano';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'jserrano';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'jserrano';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mpeters;
 DROP USER mpeters;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mpeters' IDENTIFIED BY 'keKa2fXNk3KF';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'mpeters';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'mpeters';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'mpeters';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'mpeters';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'mpeters';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mkahlen;
 DROP USER mkahlen;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mkahlen' IDENTIFIED BY 'bHX6RsedHsVLj';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'mkahlen';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'mkahlen';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'mkahlen';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'mkahlen';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'mkahlen';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO mbobbert;
 DROP USER mbobbert;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'mbobbert' IDENTIFIED BY 'QjDXLNvPUMb3V';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'mbobbert';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'mbobbert';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'mbobbert';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'mbobbert';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'mbobbert';
SET SQL_MODE = '';
GRANT USAGE ON *.* TO vkrslak;
 DROP USER vkrslak;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'vkrslak' IDENTIFIED BY 'vHLCUXx7zqsxD';

GRANT EXECUTE ON function `pla`.`convert_key` TO 'vkrslak';
GRANT EXECUTE ON function `pla`.`convert_number` TO 'vkrslak';
GRANT EXECUTE ON function `pla`.`convert_timestamp` TO 'vkrslak';
GRANT EXECUTE ON function `pla`.`bootstrap_length` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_transaction` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_bank_transaction` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_broker` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_buyer` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_buyer_state` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_cash_position` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_cleared_trade` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_competition` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_competition_properties` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_customer` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_distribution_transaction` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_genco` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_genco_state` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_market_position` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_market_position_update` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_market_transaction` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_order` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_orderbook_order` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_rate` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_tariff` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_tariff_transaction` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_timeslot` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_weather_forecast_data` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_weather_report` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_hourly_charge` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_control_event` TO 'vkrslak';
GRANT SELECT ON TABLE `pla`.`vpla_balancing_order` TO 'vkrslak';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
