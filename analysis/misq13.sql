-- Create a few preliminaries. In particular we need a filtered list
-- of all valid competitions that are supposed to appear as part of
-- our analyses, and we need a mapping between games played with
-- competitive brokers, and equivalent games played with only the
-- default broker.

DROP TABLE IF EXISTS tmp.validcmp;
CREATE TABLE IF NOT EXISTS tmp.validcmp AS
	(SELECT * FROM
      (SELECT cmp_id competition,
              cmp_name name,
			  cpr_value comment,
              count(DISTINCT bkr_id) participants,
              group_concat(DISTINCT bkr_name ORDER BY bkr_name ASC) participant_names
      FROM 
        vpla_competition,
		vpla_competition_properties,
        vpla_broker
      WHERE 
        bkr_competition = cmp_id AND
		cpr_competition = cmp_id AND
		cpr_name = 'Comment'
      GROUP BY cmp_id) a,
	  (SELECT tsl_competition, MAX(tsl_serial) length
		FROM
			vpla_timeslot
		GROUP BY
			tsl_competition) t
		
	WHERE
		tsl_competition = competition AND
		-- Some of the 2012 pilot games were faulty. In particular, there were
		-- early terminations (hence the length constraint), market clearing problems,
		-- and Crocodile abused a flaw in the customer models that made all the
		-- games it participated in useless. Also, there are quite a few games with
		-- Unreasonably high margins that are due to customer model flaws.
		((comment = '2012 Power TAC Nuremberg Demo Games' AND 
		 length > 1000 AND 
		 participant_names NOT LIKE '%Crocodile%' AND
		 competition NOT IN
			(SELECT DISTINCT clt_competition
			 FROM vpla_cleared_trade 
			 WHERE clt_execution_price < 0) AND
		 competition NOT IN
			(SELECT competition FROM
				(SELECT trf_competition competition,
						ABS(AVG(tfc_charge/tfc_kwh)) avg_price_mwh
				 FROM
					vpla_tariff_transaction,
					vpla_tariff
				WHERE
					(tfc_kwh != 0.0) AND
					tfc_type IN ('CONSUME') AND
					tfc_tariff = trf_id
				GROUP BY
					trf_competition) avgm
			 WHERE avg_price_mwh > 2.0)) OR
		(comment = '2013 Power TAC Finals' AND 
		 CAST(SUBSTRING(name, 6) AS UNSIGNED) >= 107) OR
		(comment = '2013 Power TAC Finals Equivalent NoComp Games' AND 
		 CAST(SUBSTRING(name, 13) AS UNSIGNED) >= 107)) AND
		participants IN (1,3,5,8));

DROP TABLE IF EXISTS tmp.validcmp_signatures;
CREATE TABLE tmp.validcmp_signatures AS
	(SELECT competition, name, comment, length, 
		   group_concat(DISTINCT bkr_code ORDER BY bkr_code ASC SEPARATOR '') signature
	FROM
		(SELECT competition, name, comment, length,
			CASE
				WHEN bkr_name = 'AstonTAC' THEN 'A'
				WHEN bkr_name = 'CrocodileAgent' THEN 'C'
				WHEN bkr_name = 'cwiBroker' THEN 'W'	
				WHEN bkr_name = 'default broker' THEN 'D'
				WHEN bkr_name = 'DemokriTAC' THEN 'O'	
				WHEN bkr_name = 'INAOEBroker02' THEN 'I'
				WHEN bkr_name = 'LARGEpower' THEN 'L'
				WHEN bkr_name = 'Mertacor' THEN 'M'
				WHEN bkr_name = 'MinerTA' THEN 'N'
				WHEN bkr_name = 'MLLBroker' THEN 'F'
				WHEN bkr_name = 'SotonPower' THEN 'S'
				WHEN bkr_name = 'UTest' THEN 'T'
				WHEN bkr_name = 'TacTex' THEN 'T'
				ELSE bkr_name
			END AS bkr_code
		FROM 	
			tmp.validcmp,
			vpla_broker
		WHERE
			competition = bkr_competition) a
	GROUP BY
		competition);

DROP TABLE IF EXISTS tmp.cmpmap;
CREATE TABLE tmp.cmpmap AS
	(SELECT  comp.gameid gameid,
		comp.competition competitive,
		nocomp.competition monopoly
	FROM
		(SELECT competition, 
			CAST(SUBSTRING(name, 6) AS UNSIGNED) gameid 
		FROM tmp.validcmp
		WHERE comment = '2013 Power TAC Finals') comp,
		(SELECT competition, 
			CAST(SUBSTRING(name, 13) AS UNSIGNED) gameid 
		FROM tmp.validcmp
		WHERE comment = '2013 Power TAC Finals Equivalent NoComp Games') nocomp
	WHERE 
		comp.gameid = nocomp.gameid
	ORDER BY
		comp.gameid);

DROP TABLE IF EXISTS tmp.mtr_stats;
CREATE TABLE IF NOT EXISTS tmp.mtr_stats AS
	(SELECT mtr_competition competition,
		mtr_broker broker,
		(mtr_mwh > 0) mtr_purchase,
		sum(mtr_mwh) mtr_sum_mwh,
		avg(mtr_mwh) mtr_avg_mwh,
		min(mtr_mwh) mtr_min_mwh,
		max(mtr_mwh) mtr_max_mwh,
		stddev(mtr_mwh) mtr_stddev_mwh,
		min(mtr_price) mtr_min_price,
		max(mtr_price) mtr_max_price,
		stddev(mtr_price) mtr_stddev_price,
		sum(mtr_mwh * mtr_price) / sum(mtr_mwh) mtr_weighted_avg_price
	FROM
		vpla_market_transaction
	WHERE
		mtr_broker IS NOT NULL AND
		(mtr_mwh != 0)
	GROUP BY
		competition, broker, (mtr_mwh > 0));

DROP TABLE IF EXISTS tmp.mtr_overall_stats;
CREATE TABLE IF NOT EXISTS tmp.mtr_overall_stats AS
	(SELECT competition,
		mtr_o_purchase,
		sum(mtr_o_sum_mwh_t) mtr_o_sum_mwh,
		avg(mtr_o_sum_mwh_t) mtr_o_avg_mwh,
		min(mtr_o_sum_mwh_t) mtr_o_min_mwh,
		max(mtr_o_sum_mwh_t) mtr_o_max_mwh,
		stddev(mtr_o_sum_mwh_t) mtr_o_stddev_mwh,
		min(mtr_o_charge_t / abs(mtr_o_sum_mwh_t)) mtr_o_min_price,
		max(mtr_o_charge_t / abs(mtr_o_sum_mwh_t)) mtr_o_max_price,
		stddev(mtr_o_charge_t / abs(mtr_o_sum_mwh_t)) mtr_o_stddev_price,
		sum(mtr_o_charge_t) / sum(abs(mtr_o_sum_mwh_t)) mtr_o_weighted_avg_price
	FROM
		(SELECT mtr_competition competition,
			(mtr_mwh > 0) mtr_o_purchase,
			mtr_delivery_timeslot,
			sum(mtr_mwh) mtr_o_sum_mwh_t,
			sum(mtr_price * abs(mtr_mwh)) mtr_o_charge_t
		FROM
			vpla_market_transaction
		WHERE
			mtr_broker IS NOT NULL AND
			(mtr_mwh != 0.0)
		GROUP BY
			mtr_competition, mtr_delivery_timeslot, (mtr_mwh > 0)) a
	GROUP BY competition, mtr_o_purchase);

DROP TABLE IF EXISTS tmp.dtr_stats;
CREATE TABLE IF NOT EXISTS tmp.dtr_stats AS
	(SELECT dtr_competition competition,
		dtr_broker broker,
		(dtr_kwh > 0) dtr_purchase,
		sum(dtr_kwh) dtr_sum_kwh,
		avg(dtr_kwh) dtr_avg_kwh,
		min(dtr_kwh) dtr_min_kwh,
		max(dtr_kwh) dtr_max_kwh,
		stddev(dtr_kwh) dtr_stddev_kwh,
		min(dtr_charge / abs(dtr_kwh)) dtr_min_price,
		max(dtr_charge / abs(dtr_kwh)) dtr_max_price,
		stddev(dtr_charge / abs(dtr_kwh)) dtr_stddev_price,
		sum(dtr_charge) / sum(abs(dtr_kwh)) dtr_weighted_avg_price
	FROM
		vpla_distribution_transaction
	WHERE
		dtr_broker IS NOT NULL AND
		(dtr_kwh != 0.0)
	GROUP BY
		competition, broker, (dtr_kwh > 0));


DROP TABLE IF EXISTS tmp.dtr_overall_stats;
CREATE TABLE IF NOT EXISTS tmp.dtr_overall_stats AS
	(SELECT competition,
		dtr_o_purchase,
		sum(dtr_o_sum_kwh_t) dtr_o_sum_kwh,
		avg(dtr_o_sum_kwh_t) dtr_o_avg_kwh,
		min(dtr_o_sum_kwh_t) dtr_o_min_kwh,
		max(dtr_o_sum_kwh_t) dtr_o_max_kwh,
		stddev(dtr_o_sum_kwh_t) dtr_o_stddev_kwh,
		min(dtr_o_charge_t / abs(dtr_o_sum_kwh_t)) dtr_o_min_price,
		max(dtr_o_charge_t / abs(dtr_o_sum_kwh_t)) dtr_o_max_price,
		stddev(dtr_o_charge_t / abs(dtr_o_sum_kwh_t)) dtr_o_stddev_price,
		sum(dtr_o_charge_t) / sum(abs(dtr_o_sum_kwh_t)) dtr_o_weighted_avg_price
	FROM
		(SELECT dtr_competition competition,
			(dtr_kwh > 0) dtr_o_purchase,
			dtr_timeslot,
			sum(dtr_kwh) dtr_o_sum_kwh_t,
			sum(dtr_charge) dtr_o_charge_t
		FROM
			vpla_distribution_transaction
		WHERE
			(dtr_kwh != 0.0)
		GROUP BY
			dtr_competition, dtr_timeslot, (dtr_kwh > 0)) a
	GROUP BY competition, dtr_o_purchase);


DROP TABLE IF EXISTS tmp.btr_stats;
CREATE TABLE IF NOT EXISTS tmp.btr_stats AS
	(SELECT btr_competition competition,
		btr_broker broker,
		(btr_kwh > 0) btr_purchase,
		sum(btr_kwh) btr_sum_kwh,
		avg(btr_kwh) btr_avg_kwh,
		min(btr_kwh) btr_min_kwh,
		max(btr_kwh) btr_max_kwh,
		stddev(btr_kwh) btr_stddev_kwh,
		min(btr_charge / abs(btr_kwh)) btr_min_price,
		max(btr_charge / abs(btr_kwh)) btr_max_price,
		stddev(btr_charge / abs(btr_kwh)) btr_stddev_price,
		sum(btr_charge) / sum(abs(btr_kwh)) btr_weighted_avg_price
	FROM
		vpla_balancing_transaction
	WHERE
		btr_broker IS NOT NULL AND
		(btr_kwh != 0.0)
	GROUP BY
		competition, broker, (btr_kwh > 0));

DROP TABLE IF EXISTS tmp.btr_overall_stats;
CREATE TABLE IF NOT EXISTS tmp.btr_overall_stats AS
	(SELECT competition,
		btr_o_purchase,
		sum(btr_o_sum_kwh_t) btr_o_sum_kwh,
		avg(btr_o_sum_kwh_t) btr_o_avg_kwh,
		min(btr_o_sum_kwh_t) btr_o_min_kwh,
		max(btr_o_sum_kwh_t) btr_o_max_kwh,
		stddev(btr_o_sum_kwh_t) btr_o_stddev_kwh,
		min(btr_o_charge_t / abs(btr_o_sum_kwh_t)) btr_o_min_price,
		max(btr_o_charge_t / abs(btr_o_sum_kwh_t)) btr_o_max_price,
		stddev(btr_o_charge_t / abs(btr_o_sum_kwh_t)) btr_o_stddev_price,
		sum(btr_o_charge_t) / sum(abs(btr_o_sum_kwh_t)) btr_o_weighted_avg_price
	FROM
		(SELECT btr_competition competition,
			(btr_kwh > 0) btr_o_purchase,
			btr_timeslot,
			sum(btr_kwh) btr_o_sum_kwh_t,
			sum(btr_charge) btr_o_charge_t
		FROM
			vpla_balancing_transaction
		WHERE
			(btr_kwh != 0.0)
		GROUP BY
			btr_competition, btr_timeslot, (btr_kwh > 0)) a
	GROUP BY competition, btr_o_purchase);


DROP TABLE IF EXISTS tmp.tfc_stats;
CREATE TABLE IF NOT EXISTS tmp.tfc_stats AS
	(SELECT trf_competition competition,
		trf_broker broker,
		(tfc_kwh > 0) tfc_purchase,
		sum(tfc_kwh) tfc_sum_kwh,
		avg(tfc_kwh) tfc_avg_kwh,
		min(tfc_kwh) tfc_min_kwh,
		max(tfc_kwh) tfc_max_kwh,
		stddev(tfc_kwh) tfc_stddev_kwh,
		min(tfc_charge / abs(tfc_kwh)) tfc_min_price,
		max(tfc_charge / abs(tfc_kwh)) tfc_max_price,
		stddev(tfc_charge / abs(tfc_kwh)) tfc_stddev_price,
		sum(tfc_charge) / sum(abs(tfc_kwh)) tfc_weighted_avg_price
	FROM
		vpla_tariff_transaction,
		vpla_tariff
	WHERE
		(tfc_kwh != 0.0) AND
		tfc_type IN ('CONSUME', 'PRODUCE') AND
		tfc_tariff = trf_id
	GROUP BY
		competition, broker, (tfc_kwh > 0));

DROP TABLE IF EXISTS tmp.tfc_overall_stats;
CREATE TABLE IF NOT EXISTS tmp.tfc_overall_stats AS
	(SELECT competition,
		tfc_o_purchase,
		sum(tfc_o_sum_kwh_t) tfc_o_sum_kwh,
		avg(tfc_o_sum_kwh_t) tfc_o_avg_kwh,
		min(tfc_o_sum_kwh_t) tfc_o_min_kwh,
		max(tfc_o_sum_kwh_t) tfc_o_max_kwh,
		stddev(tfc_o_sum_kwh_t) tfc_o_stddev_kwh,
		min(tfc_o_charge_t / abs(tfc_o_sum_kwh_t)) tfc_o_min_price,
		max(tfc_o_charge_t / abs(tfc_o_sum_kwh_t)) tfc_o_max_price,
		stddev(tfc_o_charge_t / abs(tfc_o_sum_kwh_t)) tfc_o_stddev_price,
		sum(tfc_o_charge_t) / sum(abs(tfc_o_sum_kwh_t)) tfc_o_weighted_avg_price
	FROM
		(SELECT trf_competition competition,
			(tfc_kwh > 0) tfc_o_purchase,
			tfc_timeslot,
			sum(tfc_kwh) tfc_o_sum_kwh_t,
			sum(tfc_charge) tfc_o_charge_t
		FROM
			vpla_tariff_transaction,
			vpla_tariff
		WHERE
			(tfc_kwh != 0.0) AND
			tfc_type IN ('CONSUME', 'PRODUCE') AND
			tfc_tariff = trf_id
		GROUP BY
			trf_competition, tfc_timeslot, (tfc_kwh > 0)) a
	GROUP BY competition, tfc_o_purchase);


DROP TABLE IF EXISTS tmp.trf_types;
CREATE TABLE IF NOT EXISTS tmp.trf_types AS
	(SELECT tariff,
		   competition,
		   broker,
		   ((rates = 1 AND fixed) OR 
			(rates = 1 AND num_weekly_begin + num_daily_begin + num_tier_threshold = 3)) tariff_fixed,
		   (num_weekly_begin + num_daily_begin > 2) tariff_variable,
		   (num_tier_threshold > 1) tariff_tiered,
		   (num_hourly_charges > 1) tariff_rtp,
		   ((rates = 1 AND fixed AND max_rate > 0)) tariff_consumption,
		   ((rates = 1 AND fixed AND min_rate < 0)) tariff_production,
		   NOT (max_curtailment IS NULL OR max_curtailment = 0) tariff_curtailable
	FROM
		(SELECT trf_id tariff,
		   trf_competition competition,
		   trf_broker broker,
		   count(rte_id) rates,
		   count(distinct rte_weekly_begin) num_weekly_begin,
		   count(distinct rte_weekly_end) num_weekly_end,
		   count(distinct rte_daily_begin) num_daily_begin,
		   count(distinct rte_daily_end) num_daily_end,
		   count(distinct rte_tier_threshold) num_tier_threshold,
		   min(rte_fixed) fixed,
		   min(rte_min_value) min_rate,
		   max(rte_max_value) max_rate,
		   count(distinct rte_notice_interval) num_notice_intervals,
		   avg(rte_expected_mean) avg_expected_rate,
		   max(rte_max_curtailment) max_curtailment,
		   count(hcg_id) num_hourly_charges
		FROM
			vpla_tariff
		JOIN vpla_rate ON (rte_tariff = trf_id)
		LEFT OUTER JOIN vpla_hourly_charge ON (hcg_rate = rte_id)
		GROUP BY
			trf_id, trf_competition, trf_broker) a);


DROP TABLE IF EXISTS tmp.trf_type_volumes;
CREATE TABLE IF NOT EXISTS tmp.trf_type_volumes AS
(SELECT 
			competition,
			(tfc_kwh > 0) purchase,
			tariff_fixed, 
			tariff_variable, 
			tariff_tiered, 
			tariff_rtp, 
			tariff_curtailable,
			sum(tfc_kwh) sum_kwh
		FROM
			vpla_tariff_transaction,
			tmp.trf_types
		WHERE
			(tfc_kwh != 0.0) AND
			tfc_type IN ('CONSUME', 'PRODUCE') AND
			tfc_tariff = tariff
		GROUP BY
			competition, (tfc_kwh > 0), 
			tariff_fixed, tariff_variable, 
			tariff_tiered, tariff_rtp, 
			tariff_curtailable);

DROP TABLE IF EXISTS tmp.analysis_tfc;
CREATE TABLE IF NOT EXISTS tmp.analysis_tfc AS
	(SELECT comp_tfc.competition, 
		    comp_tfc.tfc_purchase purchase, 
		    sum(POWER((tfc_sum_kwh / tfc_o_sum_kwh) * 100, 2)) hhi,
			tfc_o_weighted_avg_price retail_price_level,
			tfc_o_stddev_price retail_price_variability,
			tfc_o_sum_kwh total_retail_kwh
	FROM 
		tmp.tfc_stats comp_tfc,
		tmp.tfc_overall_stats comp_tfc_o
	WHERE
		comp_tfc.competition = comp_tfc_o.competition AND
		comp_tfc.tfc_purchase = comp_tfc_o.tfc_o_purchase
	GROUP BY
		competition, comp_tfc.tfc_purchase);

DROP TABLE IF EXISTS tmp.analysis_tfc_types;
CREATE TABLE IF NOT EXISTS tmp.analysis_tfc_types AS
	(SELECT
		competition,
		purchase,
		innovative_kwh / total_kwh innovative_share
	FROM
		(SELECT
			competition,
			purchase,
			sum((tariff_curtailable OR tariff_rtp) * sum_kwh) innovative_kwh,
			sum(sum_kwh) total_kwh
		FROM 
			tmp.trf_type_volumes
		GROUP BY
			competition,
			purchase) a
	WHERE
		a.total_kwh != 0);


DROP TABLE IF EXISTS tmp.analysis_bal_quality;
CREATE TABLE IF NOT EXISTS tmp.analysis_bal_quality AS
	(SELECT
		b.competition competition,
		btr_o_purchase purchase,
		btr_o_sum_kwh / 1000.0 btr_o_sum_mwh,
		mtr_o_sum_mwh,
		(btr_o_sum_kwh / 1000.0) / mtr_o_sum_mwh balancing_ratio,
		btr_o_weighted_avg_price * 1000.0 btr_o_weighted_avg_price,
		mtr_o_weighted_avg_price,
		(btr_o_weighted_avg_price * 1000.0) / mtr_o_weighted_avg_price balancing_price_ratio
	FROM 
		tmp.btr_overall_stats b,
		tmp.mtr_overall_stats m
	WHERE
		b.competition = m.competition AND
		b.btr_o_purchase = m.mtr_o_purchase);

DROP TABLE IF EXISTS tmp.analysis_margin_sourcing;
CREATE TABLE IF NOT EXISTS tmp.analysis_margin_sourcing AS
	(SELECT
		t_sale.competition competition,
		t_sale.tfc_o_sum_kwh / 1000.0 tfc_o_sum_mwh_sale,
		t_purchase.tfc_o_sum_kwh / 1000.0 tfc_o_sum_mwh_purchase,
		m_purchase.mtr_o_sum_mwh mtr_o_sum_mwh_purchase,
		m_sale.mtr_o_sum_mwh mtr_o_sum_mwh_sale,
		(m_purchase.mtr_o_sum_mwh + m_sale.mtr_o_sum_mwh) / abs(t_sale.tfc_o_sum_kwh / 1000.0) sourced_wholesale,
		t_purchase.tfc_o_sum_kwh / abs(t_sale.tfc_o_sum_kwh) sourced_retail,
		abs((t_sale.tfc_o_weighted_avg_price * 1000.0) / m_purchase.mtr_o_weighted_avg_price) margin_wholesale,
		abs(t_sale.tfc_o_weighted_avg_price / t_purchase.tfc_o_weighted_avg_price) margin_retail
	FROM 
		tmp.tfc_overall_stats t_sale,
		tmp.tfc_overall_stats t_purchase,
		tmp.mtr_overall_stats m_sale,
		tmp.mtr_overall_stats m_purchase
	WHERE
		t_sale.competition = m_sale.competition AND
		t_purchase.competition = m_sale.competition AND
		m_purchase.competition = m_sale.competition AND
		t_sale.tfc_o_purchase = 0 AND
		t_purchase.tfc_o_purchase = 1 AND
		m_sale.mtr_o_purchase = 0 AND
		m_purchase.mtr_o_purchase = 1);

DROP TABLE IF EXISTS tmp.ord_stats;
CREATE TABLE IF NOT EXISTS tmp.ord_stats AS (
  SELECT ord_competition competition,
    ord_broker broker,
    (ord_mwh > 0) ord_purchase,
    (ord_limit != 0) ord_limit,		
    sum(ord_mwh) ord_sum_mwh,
    avg(ord_mwh) ord_avg_mwh,
    min(ord_mwh) ord_min_mwh,
    max(ord_mwh) ord_max_mwh,
    stddev(ord_mwh) ord_stddev_mwh,
    avg(ord_limit) ord_avg_limit,
    min(ord_limit) ord_min_limit,
    max(ord_limit) ord_max_limit,
    stddev(ord_limit) ord_stddev_limit
  FROM
    vpla_order
  WHERE
    ord_broker IS NOT NULL
    AND (ord_mwh != 0.0)
  GROUP BY competition, broker, (ord_mwh > 0), (ord_limit != 0));