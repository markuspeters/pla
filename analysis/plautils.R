library(RMySQL)

plalocal<-function(username, password) {
  placon <<- dbConnect(MySQL(),
                       user=username, password=password,
                   dbname="pla", host="localhost")
#  on.exit(dbDisconnect(placon))
}

plaremote<-function(username, password) {
  placon <<- dbConnect(MySQL(),
                       user=username, password=password,
                       dbname="pla", host="pla.econaissance.com")
#  on.exit(dbDisconnect(placon))
}

pladone<-function() {
  dbDisconnect(placon)
}

plaf<-function(query){
  rs <- dbSendQuery(placon, query)
  data <- fetch(rs, n = -1)
  wait <- dbHasCompleted(rs)
  dbClearResult(rs)
  return(data);
}

plaqcomp<-function(){
  data <- plaf("SELECT cmp_id, cmp_name, cpr_value as cmp_comment
    FROM 
        vpla_competition, 
        vpla_competition_properties
    WHERE
        cpr_competition = cmp_id AND
        cpr_name = 'Comment'");

  data$cmp_id <- factor(data$cmp_id)
  data$cmp_name <- factor(data$cmp_name)
  data$cmp_comment <- factor(data$cmp_comment)
  
  return(data)
}

plaqbroker<-function(competition=NULL){
  if(is.null(competition)){
    # All brokers
    data <- plaf("SELECT bkr_id, bkr_name, bkr_competition
        FROM vpla_broker");
  } else {
    data <- plaf(paste("SELECT bkr_id, bkr_name, bkr_competition
        FROM vpla_broker
        WHERE bkr_competition IN (", paste(competition, collapse=","), ")"));
  }
  
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  data$bkr_competition <- factor(data$bkr_competition)
  
  return(data)
}
  
plaqcashfinal<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND cpo_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT vpla_cash_position.cpo_competition competition, cpo_balance balance, bkr_id, bkr_name
                FROM
                    vpla_cash_position, vpla_timeslot, vpla_broker,
                (SELECT cpo_competition, MAX(tsl_serial) maxserial
                 FROM
                   (SELECT cpo_competition, tsl_id, tsl_serial
                    FROM vpla_cash_position, vpla_timeslot
                    WHERE
                      tsl_id = cpo_timeslot AND
                      cpo_broker IS NOT NULL", qualifier,
                    "GROUP BY
                      cpo_competition, tsl_serial) cposerials
                 GROUP BY
                    cpo_competition) maxcposerials
                WHERE
                  vpla_cash_position.cpo_timeslot = tsl_id AND
                  tsl_competition = maxcposerials.cpo_competition AND
                  tsl_serial = maxcposerials.maxserial AND
                  cpo_broker = bkr_id");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name, ordered=T)
  
  return(data)
}

plaqcashhist<-function(competition){
  data <- plaf(gsub("__CMP", competition, 
    "SELECT bkr_id, bkr_name, serial, deposit, cumposition
    FROM
    (SELECT bkr_id, bkr_name, (tsl_serial - pla.bootstrap_length(__CMP)) serial, 
     init, deposit,
     @cs := IF(@prevbkr = bkr_id, @cs+deposit, deposit) AS cumdeposit,
     init + @cs AS cumposition,
     @prevbkr := bkr_id AS prevbkr
     FROM
     (SELECT bkr_id, bkr_name, tsl_serial, MAX(cpo_initial) init, SUM(cpd_amount) deposit
      FROM
        vpla_broker,
        vpla_cash_position,
        vpla_cash_position_deposit,
        vpla_timeslot
      WHERE
        cpd_cash_position = cpo_id AND
        cpd_timeslot = tsl_id AND
        cpo_broker = bkr_id AND
        bkr_competition = __CMP
      GROUP BY 
        bkr_id,tsl_id
      ORDER BY
        bkr_id,tsl_serial) tmp,
     (SELECT @prevbkr:=0, @cs:=0) vars) query", fixed=T))
  
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  
  return(data)
}

plaqbal<-function(competition){
  data <- plaf(gsub("__CMP", competition, 
    "SELECT bkr_id, bkr_name, (tsl_serial - pla.bootstrap_length(__CMP)) serial, 
      sum(btr_kwh) sum_kwh, sum(btr_charge) sum_charge
    FROM
      vpla_balancing_transaction, vpla_broker, vpla_timeslot
    WHERE
      btr_broker IS NOT NULL AND
      btr_broker = bkr_id AND            
      btr_timeslot = tsl_id AND 
      bkr_competition = __CMP
    GROUP BY
      bkr_id,tsl_serial
    ORDER BY
      bkr_id,tsl_serial ASC", fixed=T))
  
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  
  return(data)
}

plaqbalratio<-function(competition){
  data <- plaf(gsub("__CMP", competition, 
    "SELECT  tsl_serial - pla.bootstrap_length(__CMP) serial,
      sum(btr_kwh) balancing_vol, 
      sum(dtr_kwh) distribution_vol, 
      sum(btr_kwh) / sum(dtr_kwh) balancing_ratio 
    FROM    
      vpla_balancing_transaction, 
      vpla_distribution_transaction, 
      vpla_timeslot,
      vpla_broker
    WHERE   
      btr_competition = __CMP AND 
      dtr_competition = __CMP AND
      btr_timeslot = tsl_id AND 
      dtr_timeslot = tsl_id AND
      btr_broker = bkr_id AND 
      dtr_broker = bkr_id
    GROUP BY 
      tsl_serial
    ORDER BY 
      tsl_serial ASC", fixed=T))
  
  return(data)
}

plaqwholesaletransactions<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND bkr_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT purchase.bkr_competition competition, purchase.bkr_id bkr_id, purchase.bkr_name bkr_name,
                       purchase_mwh, weighted_avg_purchase_price, sale_mwh, weighted_avg_sale_price
                FROM
                  (SELECT bkr_competition, bkr_id, bkr_name, sum(mtr_mwh) purchase_mwh, sum(mtr_mwh * mtr_price) / sum(mtr_mwh) weighted_avg_purchase_price
                  FROM
                    vpla_market_transaction, 
                    vpla_broker
                  WHERE
                    mtr_broker = bkr_id AND
                    mtr_mwh > 0 ", qualifier,
                " GROUP BY
                    bkr_competition, bkr_id) purchase
               LEFT OUTER JOIN
                (SELECT bkr_competition, bkr_id, bkr_name, sum(mtr_mwh) sale_mwh, sum(mtr_mwh * mtr_price) / sum(mtr_mwh) weighted_avg_sale_price
                  FROM
                    vpla_market_transaction, 
                    vpla_broker
                  WHERE
                    mtr_broker = bkr_id AND
                    mtr_mwh < 0 ", qualifier,
              " GROUP BY
                  bkr_competition, bkr_id) sale
              ON purchase.bkr_id = sale.bkr_id");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  
  return(data)
}

plaqtarifftransactions<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND bkr_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT consumption.bkr_competition competition, consumption.bkr_id bkr_id, 
                  consumption.bkr_name bkr_name, consumption.sum_kwh consumption_kwh, 
                  consumption.avg_charge consumption_avg_charge,
                  production.sum_kwh production_kwh, 
                  production.avg_charge production_avg_charge
               FROM
                  (SELECT bkr_competition, bkr_id, bkr_name, sum(tfc_kwh) sum_kwh, sum(tfc_charge) / sum(tfc_kwh) avg_charge
                  FROM
                    vpla_tariff_transaction, 
                    vpla_tariff,
                    vpla_broker
                  WHERE
                    tfc_customer IS NOT NULL AND
                    tfc_tariff = trf_id AND
                    trf_broker = bkr_id AND
                    tfc_type = 'CONSUME'",
                    qualifier,              
                  "GROUP BY
                    bkr_competition,bkr_id) consumption
                LEFT OUTER JOIN
                  (SELECT bkr_competition, bkr_id, bkr_name, sum(tfc_kwh) sum_kwh, sum(tfc_charge) / sum(tfc_kwh) avg_charge
                  FROM
                    vpla_tariff_transaction, 
                    vpla_tariff,
                    vpla_broker
                  WHERE
                    tfc_customer IS NOT NULL AND
                    tfc_tariff = trf_id AND
                    trf_broker = bkr_id AND
                    tfc_type = 'PRODUCE'",
                    qualifier,        
                  "GROUP BY
                    bkr_competition,bkr_id) production
               ON consumption.bkr_competition = production.bkr_competition AND consumption.bkr_id = production.bkr_id");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  
  return(data)
}

plaqbalancingtransactions<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND bkr_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT  bkr_competition competition, bkr_id, bkr_name,
                        sum(btr_kwh) total_btr_kwh, sum(abs(btr_kwh)) total_abs_btr_kwh,
                        sum(btr_charge) total_btr_charge,
                        sum(dtr_kwh) total_dtr_kwh,
                        sum(dtr_charge) total_dtr_charge,
                        sum(balancing_ratio) total_balancing_ratio,
                        sum(abs_balancing_ratio) total_abs_balancing_ratio,
                        sum(balancing_ratio) / COUNT(dtr_timeslot) total_norm_balancing_ratio,
                        sum(abs_balancing_ratio) / COUNT(dtr_timeslot) total_norm_abs_balancing_ratio
                FROM
                  (SELECT bkr_competition, btr_timeslot, bkr_id, bkr_name,
                          btr_kwh, btr_charge,            
                          dtr_kwh, dtr_charge,
                          (btr_kwh / dtr_kwh) balancing_ratio,
                          (abs(btr_kwh) / abs(dtr_kwh)) abs_balancing_ratio,
                          dtr_timeslot
                  FROM
                    vpla_balancing_transaction,     
                    vpla_distribution_transaction, 
                    vpla_broker
                  WHERE   
                    btr_broker = bkr_id AND 
                    dtr_broker = bkr_id AND
                    btr_timeslot = dtr_timeslot ", qualifier, 
              " GROUP BY 
                    bkr_competition,bkr_id,btr_timeslot) tmp
              GROUP BY
                bkr_competition, bkr_id");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  
  return(data)
}

plaqretailwholesale<-function(competition=NULL){
  if(is.null(competition)){
    trf_qualifier<-""
    mtr_qualifier<-""
  } else {
    trf_qualifier<-paste("AND trf_competition IN (", paste(competition, collapse=",") ,")")
    mtr_qualifier<-paste("AND mtr_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT trf_competition competition, tfc_timeslot timeslot, tsl_serial, 
                       total_consumption, 
                       total_purchase, weighted_avg_purchase_price, 
                       total_sale,weighted_avg_sale_price
               FROM
                 (SELECT trf_competition, tfc_timeslot, tsl_serial, sum(tfc_kwh) total_consumption
                 FROM
                   vpla_tariff_transaction, 
                   vpla_tariff,
                   vpla_timeslot
                 WHERE
                   tfc_customer IS NOT NULL AND
                   tfc_tariff = trf_id AND
                   tfc_timeslot = tsl_id AND
                   tfc_type IN ('PRODUCE', 'CONSUME') ", trf_qualifier, "
                 GROUP BY tfc_timeslot) tariff
               LEFT OUTER JOIN
                 (SELECT mtr_delivery_timeslot, sum(mtr_mwh) total_purchase, 
                         sum(mtr_mwh * mtr_price) / sum(mtr_mwh) weighted_avg_purchase_price
                 FROM
                  vpla_market_transaction
                 WHERE
                  mtr_broker IS NOT NULL AND mtr_mwh > 0 ", mtr_qualifier, "
                 GROUP BY mtr_delivery_timeslot) purchase
               ON tfc_timeslot = purchase.mtr_delivery_timeslot
               LEFT OUTER JOIN
                 (SELECT mtr_delivery_timeslot, sum(mtr_mwh) total_sale, 
                         sum(mtr_mwh * mtr_price) / sum(mtr_mwh) weighted_avg_sale_price
                 FROM
                  vpla_market_transaction
                 WHERE
                  mtr_broker IS NOT NULL AND mtr_mwh < 0 ", mtr_qualifier, "
                 GROUP BY mtr_delivery_timeslot) sale
               ON tfc_timeslot = sale.mtr_delivery_timeslot");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  
  return(data)
}

plaqleadtime<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND mtr_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT purchase.bkr_competition competition, purchase.bkr_id bkr_id,
                       purchase.bkr_name, purchase.leadtime, total_purchase_mwh,
                       weighted_avg_purchaseprice, total_sale_mwh, weighted_avg_saleprice
               FROM
                (SELECT bkr_competition, bkr_id, bkr_name,
                  HOUR(TIMEDIFF(tsl_delivery.tsl_time, tsl_order.tsl_time)) leadtime,
                  SUM(mtr_mwh) total_purchase_mwh,
                  SUM(mtr_mwh * mtr_price) / SUM(mtr_mwh) weighted_avg_purchaseprice
                 FROM 
                  vpla_market_transaction,
                  vpla_broker,
                  vpla_timeslot tsl_order,
                  vpla_timeslot tsl_delivery
                WHERE
                  bkr_id = mtr_broker AND
                  tsl_delivery.tsl_id = mtr_delivery_timeslot AND
                  tsl_order.tsl_id = mtr_order_timeslot AND
                  mtr_mwh > 0 ", qualifier, "
                GROUP BY
                  bkr_competition, bkr_id, leadtime) purchase
                LEFT OUTER JOIN
                  (SELECT bkr_competition, bkr_id, bkr_name,
                  HOUR(TIMEDIFF(tsl_delivery.tsl_time, tsl_order.tsl_time)) leadtime,
                  SUM(mtr_mwh) total_sale_mwh,
                  SUM(mtr_mwh * mtr_price) / SUM(mtr_mwh) weighted_avg_saleprice
                FROM 
                  vpla_market_transaction,
                  vpla_broker,
                  vpla_timeslot tsl_order,
                  vpla_timeslot tsl_delivery
                WHERE
                  bkr_id = mtr_broker AND
                  tsl_delivery.tsl_id = mtr_delivery_timeslot AND
                  tsl_order.tsl_id = mtr_order_timeslot AND
                  mtr_mwh < 0 ", qualifier, "
                GROUP BY
                  bkr_competition, bkr_id, leadtime) sale
                ON
                  purchase.bkr_id = sale.bkr_id AND
                  purchase.leadtime = sale.leadtime");
  
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  data$bkr_id <- factor(data$bkr_id)
  data$bkr_name <- factor(data$bkr_name)
  
  return(data)
}

plaqcustomerbehavior<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND cst_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT  consumption.cst_competition competition, 
                        consumption.cst_id cst_id, consumption.cst_name cst_name,
                        consumption.tsl_serial tsl_serial, 
                        consumption_kwh, consumption_charge, 
                        production_kwh, production_charge 
               FROM
                  (SELECT cst_competition, cst_id, cst_name, tsl_serial, sum(tfc_kwh) consumption_kwh, sum(tfc_charge) consumption_charge
                  FROM
                    vpla_tariff_transaction,
                    vpla_customer,
                    vpla_timeslot
                  WHERE
                    tfc_customer = cst_id AND
                    tfc_timeslot = tsl_id AND
                    tfc_type = 'CONSUME' ", qualifier, "
                  GROUP BY
                    cst_competition, cst_id, tsl_id) consumption
               LEFT OUTER JOIN
                  (SELECT cst_competition, cst_id, cst_name, tsl_serial, sum(tfc_kwh) production_kwh, sum(tfc_charge) production_charge
                  FROM
                    vpla_tariff_transaction,
                    vpla_customer,
                    vpla_timeslot
                  WHERE
                    tfc_customer = cst_id AND
                    tfc_timeslot = tsl_id AND
                    tfc_type = 'PRODUCE' ", qualifier, "
                  GROUP BY
                    cst_competition, cst_id, tsl_id) production
              ON
                consumption.cst_id = production.cst_id AND
                consumption.tsl_serial = production.tsl_serial");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  data$cst_id <- factor(data$cst_id)
  data$cst_name <- factor(data$cst_name)
  
  return(data)
}

plaqretailprices<-function(competition=NULL){
  if(is.null(competition)){
    qualifier<-""
  } else {
    qualifier<-paste("AND trf_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT  consumption.competition competition, 
                        consumption_kwh, weighted_consumption_price,
                        production_kwh, weighted_production_price
               FROM
                 (SELECT trf_competition competition,
                         sum(tfc_kwh) consumption_kwh,
                         sum(tfc_charge) / sum(tfc_kwh) weighted_consumption_price
                 FROM 
                   vpla_tariff_transaction,
                   vpla_tariff
                 WHERE 
                   tfc_tariff = trf_id AND
                   tfc_type = 'CONSUME' ", qualifier, "
                 GROUP BY
                 trf_competition) consumption
               LEFT OUTER JOIN
                (SELECT  trf_competition competition,
                         sum(tfc_kwh) production_kwh,
                         sum(tfc_charge) / sum(tfc_kwh) weighted_production_price
                FROM 
                 vpla_tariff_transaction,
                 vpla_tariff
                WHERE 
                 tfc_tariff = trf_id AND
                 tfc_type = 'PRODUCE' ", qualifier, "
               GROUP BY
                trf_competition) production
               ON consumption.competition = production.competition");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  
  return(data)
}

plaqretailmargins<-function(competition=NULL){
  if(is.null(competition)){
    mtr_qualifier<-""
    trf_qualifier<-""
  } else {
    mtr_qualifier<-paste("AND mtr_competition IN (", paste(competition, collapse=",") ,")")
    trf_qualifier<-paste("AND trf_competition IN (", paste(competition, collapse=",") ,")")
  }
  
  query<-paste("SELECT  wholesale.competition competition,
                        wholesale.bkr_id bkr_id,
                        wholesale.bkr_name bkr_name,
                        total_purchase_kwh,
                        weighted_avg_purchase_price_kwh,
                        consumption_kwh,
                        weighted_avg_consumption_price_kwh,
                        (weighted_avg_consumption_price_kwh / weighted_avg_purchase_price_kwh) margin
               FROM
                 (SELECT  mtr_competition competition, bkr_id, bkr_name, 
                          sum(mtr_mwh) * 1000 total_purchase_kwh, 
                          sum(mtr_mwh * mtr_price) / sum(mtr_mwh) / 1000 weighted_avg_purchase_price_kwh
                 FROM
                  vpla_market_transaction,
                  vpla_broker
                 WHERE
                  mtr_broker = bkr_id AND 
                  mtr_mwh > 0 ", mtr_qualifier, "
                  GROUP BY 
                 mtr_competition, mtr_broker) wholesale
               LEFT OUTER JOIN
                (SELECT trf_competition competition, bkr_id, bkr_name,
                        sum(tfc_kwh) consumption_kwh,
                        sum(tfc_charge) / sum(tfc_kwh) weighted_avg_consumption_price_kwh
                FROM 
                 vpla_tariff_transaction,
                 vpla_tariff,
                 vpla_broker
                WHERE 
                 tfc_tariff = trf_id AND
                 tfc_type = 'CONSUME' AND
                 trf_broker = bkr_id ", trf_qualifier, "
               GROUP BY
                trf_competition, trf_broker) retail
               ON retail.bkr_id = wholesale.bkr_id");
  
  data<-plaf(query)
  
  data$competition <- factor(data$competition)
  
  return(data)
}





