#!/bin/bash

########################################################
#
# Modify these settings to match your machine
#
# You might also have to adapt the command for the XSLT
# processor (xsltproc) below to match your environment.
#
########################################################

PLA_USERNAME=pla_admin
PLA_PASSWORD=pla_admin
PLA_HOST=localhost

########################################################

cd pladoc
java -jar schemaSpy_5.0.0.jar -nohtml -I "pla_import|vpla_import|vpla_import_new" -t mysql -dp mysql-connector-java-5.1.22-bin.jar -o . -host $PLA_HOST -db pla -u $PLA_USERNAME -p $PLA_PASSWORD 
xsltproc -o ../pla_gendoc.tex pla_gendoc.xslt pla.xml
cd ..


