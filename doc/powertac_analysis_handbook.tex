\documentclass[11pt,a4paper,twoside]{article}

\usepackage{rsm-titlepage}
\usepackage[margin=2.5cm]{geometry}

\usepackage{lscape}

\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{tabularx}
\usepackage{amssymb,amsmath,amsthm}

\usepackage{nameref}
\usepackage{url,hyperref}

\usepackage[table]{xcolor}
\usepackage{xspace}
\usepackage[framemethod=tikz]{mdframed}

\usepackage{fancybox}

\usepackage{listings}

\newenvironment{wideverbatim}%
{\vskip\baselineskip\VerbatimEnvironment
\begin{Sbox}\begin{BVerbatim}}
{\end{BVerbatim}%
\end{Sbox}\noindent\centerline{\TheSbox}\vskip\baselineskip}

\setcounter{tocdepth}{3}
\renewcommand{\topfraction}{0.9}
\renewcommand{\textfraction}{0.1}

\newcommand{\IGNORE}[1]{}
\newcommand{\pla}{\texttt{PLA}\xspace}

\newtheorem{example}{Example}

% Uncomment this to remove passages that explicitly give away our identities
% \newcommand{\isAnonymous}{true}

\begin{document}

\title{Analyzing the Power Trading Agent Competition}

\ifx\isAnonymous\undefined

	\author{Markus Peters}
	\date{January 2014}
	\trnumber{ERS-2014-xxx-LIS}

	\keywords{Autonomous Agents, Business Intelligence, Electronic Commerce, Energy,
Power, Policy Guidance, Reporting, Trading Agent Competition}

	\abstract{
		The Power Trading Agent Competition is a rich, open-source simulation platform that challenges researchers to build autonomous, self-interested broker agents that compete directly with each other in a virtual future retail power market. Power TAC competitions generate a wealth of micro-level data for researchers to use in studies of power markets and their emergent behaviors. We have created a public infrastructure called {\bfseries \pla} that provides researchers with high-level query abilities on these micro-level data. We give an introduction to \pla, explain how to interpret its content, and give examples of how to create analyses based on the \pla infrastructure.
	}

	\maketitle

	\pagenumbering{roman} \tableofcontents
	
\fi

\newpage

\pagenumbering{arabic}
\setcounter{page}{1}

\section{Purpose of this document}
\label{sec:purpose}

The Power Trading Agent Competition (Power TAC, see \cite{Ketter12ERIM-PowerTAC-Spec} and \url{www.powertac.org} for more information) is a rich, open-source simulation platform that challenges researchers to build autonomous, self-interested broker agents that compete directly with each other in a virtual future retail power market. One of the express purposes for which Power TAC was created is the study of phenomena arising from the complex interaction between brokers, customers, retail and wholesale markets, and the grid operator in this environment. To that end, the Power TAC platform provides detailed and comprehensive logging facilities that protocol low-level interactions between the aforementioned actors and the Power TAC platform itself at every instance of each game. While the {\bfseries state log} text files that Power TAC produces in this way are detailed and comprehensive, they lack facilities for convenient higher-level queries, especially queries that compare and contrast multiple games.


We have created an infrastructure that provides these abilities based on standard relational database technology, and we provision a public, hosted repository of past games for researchers to export from and analyze on. We call this repository \pla, which is short for {\bfseries P}ower TAC {\bfseries L}ogfile {\bfseries A}nalysis. The goals behind this public infrastructure are:

\begin{description}
\item[Easy Access] We wish to provide quick and easy access to all official competition data recorded in the past. We hope to lower the barriers to entry for new researchers working on this data. And we aim to enable selective access using a high-level query language to relevant slices of the data without the need to sift through the comparatively large state logs.

\item[Quality Data] By providing game data in highly structured, relational form we hope to establish an authoritative repository of data that is constantly scrutinized by the community, reducing the potential for technical faults in analyses based on this data.

\item[Standard Metrics] Metrics for future energy markets are a nascent research area. In the future, we hope to provide authoritative versions of standardized high-level metrics computed on our data for researchers to compare and contrast.

\item[Reproducible Analyses] Having a standard data repository is a prerequisite for reproducible analyses. We hope that our repository encourages scholars to release queries that they have used along with their articles to other researchers' benefit.

\item[Visual Data Analysis] Having game data centralized in a standard relational database makes it accessible from standard analysis tools like Excel, IBM Cognos, R, SAS, etc.
\end{description}
%

\ifx\isAnonymous\undefined
If you are finding \pla helpful for your own work, please consider citing the paper for which we originally built \pla~\cite{Ketter13AAAI}. 
\fi
In this document, we describe how to connect to \pla, we explain how to read and interpret its content, and we give examples of how to perform analyses of the data contained in it. In contrast, it is not the purpose of this document to serve as an introduction to relational database systems or the Structured Query Language (SQL). We assume a solid working knowledge of these concepts and refer the reader to any of the numerous good textbooks on the subject for a refresher.

\ifx\isAnonymous\undefined
	\paragraph{Acknowledgements} Power TAC is the work of John Collins (Minnesota), Wolf Ketter (Erasmus Rotterdam), and a large committed community, chiefly at Aristotle University Thessaloniki, Carnegie Mellon University, Erasmus University Rotterdam, University of Minnesota, and Zagreb University. Much of the domain object documentation collected in this document are edited fragments based on the source code documentation they provided along with the Power TAC platform.
\fi

\section{Getting Started With \pla}
\label{sec:getting_started}

\begin{figure}[hbt]
\centering
\includegraphics[width=0.8\textwidth]{figures/architecture}
\caption{Top-level struture of \pla. \pla maintains a layer of views that form the interface to the user. At this point, most views are one-to-one mappings of the underlying domain entities. In future, views may also offer derived data (such as standard metrics), or legacy representations of domain entities.}
\label{fig:architecture}
\end{figure}

\pla is built on relational database technology and, as such, interfaces with its users through views and stored procedures (see Figure~\ref{fig:architecture}). The underlying data is held in tables that are not directly accessible by the user. Instead, the user interfaces with a layer of views which, in turn, connect to the underlying data. This additional level of indirection separates the physical storage of the data from the logical contract that \pla entertains with its users. For example, we plan to retain backwards compatibility as far as possible when changing the underlying physical storage of the data by keeping the view layer constant. In the following, we will refer to tables and their associated views interchangeably where there is a one-to-one relationship between the two. 

\subsection{Obtaining Access}
\label{sec:obtaining_access}

\pla requires you to provide credentials (that is, username and password) before accessing it. To receive these credentials, email 
\ifx\isAnonymous\undefined
	Markus Peters (\url{plaaccounts@econaissance.com}) stating your name and affiliation.
\else
	us (\url{plaaccounts@econaissance.com}) stating the name of the paper you are reviewing.
\fi

\subsection{Establishing a Database Connection}

\pla runs on a MySQL DBMS and you can use any client capable of connecting to MySQL, such as
\begin{itemize}
	\item MySQL command line client, \url{http://dev.mysql.com/doc/refman/5.6/en/mysql.html}
	\item MySQL Workbench, \url{http://dev.mysql.com/downloads/workbench/}
	\item JDBC access, \url{http://dev.mysql.com/downloads/connector/j/}
\end{itemize}
as well as a number of free and commercial reporting and analysis tools built on top of these. Table~\ref{tab:coordinates} summarizes the data required to establish a database connection to \pla. Please refer to the handbook of your client for more information on how to establish a connection using these data.

\begin{table}
\centering
\begin{tabular}{l|l}
{\bfseries Parameter} & {\bfseries Value} \\
\hline
Host & \url{pla.econaissance.com} \\
Port & \texttt{3306} \\
Username & assigned by \pla administrator \\
Password & assigned by \pla administrator \\
Database & \texttt{pla} \\
\end{tabular}
\caption{Access data for the \pla system. See Section~\ref{sec:obtaining_access} for more information on how to obtain a username and password.}
\label{tab:coordinates}
\end{table}

\subsection{The First Query}

After obtaining \pla credentials, you can verify access by executing a first, simple query against the database: 
\begin{wideverbatim}
mysql -h pla.econaissance.com -P 3306 -u'<UNAME>' -p'<PWD>' pla 
      -e 'SELECT * FROM vpla_competition'
\end{wideverbatim} 
where you need to replace \texttt{<UNAME>} and \texttt{<PWD>} with the username and password you received. This should give you a list of competitions available in \pla like the following:

\begin{wideverbatim}
+--------+----------+
| cmp_id | cmp_name |
+--------+----------+
|      1 | game-416 |
    ...       ...
\end{wideverbatim}

Our example uses the MySQL command line client. Please refer to the handbook of your client for specifics on entering connection data and executing queries.

\section{Conventions}
\label{sec:conventions}

Both, \pla and the underlying Power TAC simulation platform, make use of a number of conventions that make it easier to interpret and work with the data they generate.

\subsection{Naming Conventions}

\subsubsection{Table, View and Column Names}

Tables and views are prefixed with \texttt{pla} and \texttt{vpla}, respectively. In most instances there is a direct relationship between a physical table (not accessible for reporting purposes) and a view provided on top of it (used for reporting). For example, \texttt{pla\_competition} is a database table used to store a list of all competitions that \pla holds information about. Reporting users must use the corresponding view \texttt{vpla\_competition} to query this data.

All column names are prefixed with a three-letter short form of the table name. The prefix makes it easier to determine the source of certain attributes in complicated queries, and it reduces the need for disambiguation. For example, \texttt{cmp\_id} and \texttt{cmp\_name} are both columns of the \texttt{pla\_competition} table can be accessed through \texttt{vpla\_competition} by the same name.

\subsubsection{Primary Keys}

All tables in \pla have artificial integer primary keys. The associated columns have the form \texttt{<TABLE>\_id}, where \texttt{<TABLE>} is the three-letter short form of the table name referred to above. For example, \texttt{tsl\_id} is the primary key of the \texttt{pla\_timeslot} table, whereas \texttt{bkr\_id} is the primary key of the \texttt{pla\_broker} table.

\subsubsection{Foreign Keys}

Foreign key are of the form \texttt{<SRCTABLE>\_<TGTTABLE>} where \texttt{<SRCTABLE>} is the three-letter short form of the table that contains the foreign key, and \texttt{<TGTTABLE>} is the long form of the parent table, without the \texttt{pla} prefix. For example, \texttt{bkr\_competition} is the reference, from each broker, to the competition it participated in (i.e., a reference to a \texttt{cmp\_id} in \texttt{pla\_competition}). Similarly, the column \texttt{obk\_timeslot} in \texttt{pla\_orderbook} links a given orderbook to the timeslot it was valid in (i.e., a reference to a \texttt{tsl\_id} in \texttt{pla\_timeslot}).

\subsection{Identifiers and Instances}

All \pla primary keys are identifiers that are globally unique for a certain object type in \pla. For example, there is only one timeslot record in all of \pla with a given \texttt{tsl\_id}. These identifiers are what is used in establishing relationships between different tables. Identifiers are generated by the database underlying \pla while importing Power TAC log files, and there is no direct correspondence to them in the Power TAC competition they stem from.

To maintain a connection to the underlying Power TAC log file, most \pla  objects also carry an {\bfseries instance} number that stems directly from the Power TAC log file and that is unique only within the context of its parent competition. For example, while a \texttt{tsl\_id} identifies a particular timeslot object in a globally unique fashion within \pla, the corresponding \texttt{tsl\_instance} is likely not unique. Only in combination with the associated competition (e.g., using the foreign key \texttt{tsl\_competition}) would an instance uniquely identify a timeslot within \pla.

\begin{mdframed}[backgroundcolor=gray!20,skipabove=10pt,skipbelow=10pt]
{\bfseries Note:} Avoid using \pla's primary keys to identify objects over the long run, as they are subject to change. \pla guarantees that primary keys are unique, and that the internal web of references based on primary and foreign keys is consistent. The values of these keys may change, however, as a result of re-loading data or internal reorganization.

Instead, use instance numbers and other Power TAC assigned identifiers. For example, instead of referencing \texttt{bkr\_id = 4711} or \texttt{cmp\_id = 1337}, you should reference either
\begin{itemize}
\item broker \texttt{bkr\_name = 'LargePower'} within competition \texttt{cmp\_name = 'nuremberg-1234'}, or
\item broker \texttt{bkr\_instance = 0815} within competition \texttt{cmp\_name = 'nuremberg-1234'}
\end{itemize}
based on the fact that these identifier combinations uniquely identify a broker within a competition if the competition name is unique.
\end{mdframed}

\subsection{Power TAC Conventions}

There are a number of conventions in Power TAC that we have adopted in \pla:
\begin{description}
	\item[Broker Prespective] All quantities (money and physical energy, in particular) are measured from the broker's perspective. For example, in \texttt{pla\_market\_transaction}, a positive value \texttt{mtr\_mwh} = 1.0 denotes that 1.0 MWh worth of electricity has been transferred {\em from} the wholesale market {\em to} the broker. Whereas the corresponding negative value \texttt{mtr\_mwh} = -50.0 denotes that a price of 50.0 currency units flowed {\em from} the broker {\em to} the wholesale market in exchange.
	
	\item[Wholesale and Retail Units] As is customary in electricity markets, the unit of physical energy in retail electricity markets is kWh, whereas wholesale markets  use MWh units instead. When comparing between the two, the resulting factor of 1000 must be taken into account.
\end{description}

\section{Object Reference}
\label{sec:objects}

The following sections provide a reference-style overview over the \pla database. Starting with a graphical overview of the most important entities in Section~\ref{sec:graphical}, Section~\ref{sec:ref_domain_entities} provides a tabular listing of all domain entities, Section~\ref{sec:ref_view_access} describes the view access layer, and Section~\ref{sec:stored_procedures} describes the stored procedure interface. Detailed documentation is mostly concentrated on the domain entities. While the domain entities themselves are not directly accessible for reporting purposes, the view access layer gives an almost one-to-one view on them. The short view access layer reference is mainly given for the sake of completeness. The reference only explicitly comments on views where they deviate from the one-to-one principle.

\begin{table}
\centering
\begin{tabular}{l|l}
{\bfseries Short Form} & {\bfseries Data Type} \\
\hline
\texttt{BOOL} & Boolean \\
\texttt{C<n>} & String, maximum length \texttt{<n>}, e.g. \texttt{C256} \\
\texttt{DT} & Date/Time \\
\texttt{D<n,p>} & Decimal, size \texttt{<n>}, precision \texttt{<p>}, e.g. \texttt{D10,5} \\
\texttt{I<n>} & Signed Integer, size \texttt{<n>}, e.g. \texttt{I10} \\
\texttt{UI<n>} & Unsigned Integer, size \texttt{<n>}, e.g. \texttt{UI10} \\
\end{tabular}
\caption{List of data types used in \pla}
\label{tab:datatypes}
\end{table}

The following conventions and notations are used throughout the documentation:
\begin{description}
	\item[Primary Keys and Index Columns] Primary Key columns are marked with an asterisk (\texttt{*}) and columns that are part of an index are marked with a plus sign (\texttt{+}).
	\item[Data Types] Table~\ref{tab:datatypes} gives a list of all short-hands used in the data type descriptions of this section.
\end{description}

\subsection{Graphical Overview}
\label{sec:graphical}

The model views in this section provide a map of the most important entities in the \pla database. The connection between these views is as follows:
\begin{itemize}
	\item Figure~\ref{fig:model_competition} shows the skeleton of the \pla database, including entities that model competitions, the most important actors, and the time dimension.
	
	\item Figure~\ref{fig:model_tariffs} focusses on the retail market. It shows the static structure of tariffs on the one hand, and the dynamics of tariffs and the resulting distribution transactions on the other hand.
	
	\item Figure~\ref{fig:model_wholesale} depicts the most important entities on the wholesale side of the distribution system. 
	
	\item Figure~\ref{fig:model_balancing}, finally, shows the balancing side of the system.
\end{itemize}

\begin{figure}[hbt]
\centering
\includegraphics[trim=0 280 550 0,clip,width=1.0\textwidth]{figures/model_competition}
\caption{The competition is the top-level entity in \pla to which all other entities relate either directly or indirectly. The figure shows the most important entities within a competition, including the most important actors (brokers, customers, and gencos), and the time dimension.}
\label{fig:model_competition}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[trim=0 200 600 0,clip,width=0.95\textwidth]{figures/model_tariffs}
\caption{Brokers issue tariffs that are composed of rates. Customers subscribe to tariffs, consume or produce electricity under those tariffs, and eventually unsubscribe from tariffs. All of these events are special kinds of tariff transactions. Many of these tariff transactions give rise to electricity flows on the physical distribution infrastructure, which the distribution utility records as a distribution transaction.}
\label{fig:model_tariffs}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[trim=50 350 20 0,clip,width=1.0\textwidth]{figures/model_wholesale}
\caption{Wholesale markets are periodic double auctions to which brokers, gencos, and buyers submit orders. These orders (tracked in one orderbook per timeslot) give rise to market positions and, ultimately, cleared trades.}
\label{fig:model_wholesale}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[trim=150 340 580 0,clip,width=1.0\textwidth]{figures/model_balancing}
\caption{The balancing market can be seen as the real-time facet of the wholesale market, organized by the distribution utility in Power TAC. At the most fundamental level, balancing activities are captured as balancing transactions. Additionally, the distribution utility may issue balancing orders against certain types of tariffs, which in turn give rise to balancing control events. These are a way of reducing customer loads (or increasing customer production) in response to imbalances rather than invoking traditional reserve capacitites.}
\label{fig:model_balancing}
\end{figure}

\input{pla_gendoc.tex}

\subsection{Stored Procedures}
\label{sec:stored_procedures}

\subsubsection{bootstrap\_length}

Takes a Competition ID, that is, one of the values returned by a 
\begin{lstlisting}[frame=single]
SELECT cmp_id FROM vpla_competition;
\end{lstlisting}
and returns the number of bootstrap timeslots for that competition. Bootstrap timeslots are prepended to each Power TAC competition to reach a steady-state environment before Brokers start competing. In many analyses it is useful to subtract the result of \texttt{bootstrap\_length} from timeslot indices to get to an indexing where zero denotes the first timeslot in which Brokers could actually act in the environment (as opposed to the first bootstrap timeslot).

\begin{description}
	\item[Arguments] Competition ID for which the number of bootstrap timeslots is to be determined.
	\item[Return Value] A single row with a single cell containing the number of bootstrap timeslots for the given competition; or a single \texttt{NULL} cell if the give competition ID does not exist.
	\item[Example] 
\end{description}
\begin{lstlisting}[frame=single]
SELECT pla.bootstrap_length(1);
\end{lstlisting}

\section{Example Analyses}
\label{sec:examples}
\lstset{language=SQL}

This section presents a number of example queries that illustrate the way in which \pla can be used to answer questions about Power TAC competitions. Along the way, we will highlight common pitfalls and share best practices for writing \pla queries.

TODO

%\subsection{Cash Accounts and Balances}
%
%\begin{figure}
%\begin{lstlisting}[frame=single]
%SELECT cpo_competition, bkr_id, bkr_name, SUM(cpd_amount) balance
%FROM vpla_cash_position,
%     vpla_cash_position_deposit,
%     vpla_broker
%WHERE
%     cpo_id = cpd_cash_position AND
%     bkr_id = cpo_broker
%GROUP BY cpo_competition, cpo_broker 
%\end{lstlisting}
%\caption{Finding the final Cash Positions of all Brokers.}
%\label{fig:query_cashpositions}
%\end{figure}
%
%Figure~\ref{fig:query_cashpositions} shows a simple query that seeks to find out the final cash balance of each broker in each game in the database. The result of executing this query will look something like the following list:
%
%\begin{wideverbatim}
%...
%12, 102, AstonTAC, 1374327.13256
%12, 103, LARGEpower, -32067.07432
%12, 104, SotonPower, 419096.28241
%12, 105, Mertacor, 1241217.48564
%12, 106, MinerTA, 27252.73953
%12, 107, default broker, -75717.63381
%...
%\end{wideverbatim}
%
%A few items are worth mentioning with respect to this query:
%\begin{itemize}
%\item Notice, how we're using the view access layer (e.g., \texttt{vpla\_cash\_position}) to access the data we're interested in. Future versions of \pla will attempt to ensure that the structure of these views remains unchanged even if the internal representation of certain data changes.
%\item Power TAC distinguishes between the Broker, its Cash Position, and changes to that Cash Position (so-called Deposits). To obtain the final Cash Position, and ignoring a potential initial balance on the Cash Position, we simply sum over all Deposits per Competition and Broker. For this query to be fully correct, it would have to add the initial balance \texttt{vpla\_cash\_position.cpo\_initial} to this result. (The initial value is always zero at the time of this writing in early 2013, so the result is currently unaffected by this omission.)
%\item The query affects the entire \pla repository, i.e. it calculates final Cash Balances for all combinations of Competition and Broker. If possible, try to avoid such global queries and restrict, for example, the Competition to those that interest you.\footnote{\pla does not currently use a quota system for execution times, but we do intend to automatically abort long-running queries during peak hours should that become necessary.}
%\end{itemize}

\section{Frequently Asked Questions}
\label{sec:faq}

\subsection{Accessing \pla}

If you are getting the following error message 
%
\begin{lstlisting}[frame=single]
Access denied for user '<USER>'@'<IPADDRESS>' (using password: YES)
\end{lstlisting}
%
(where the {\em USER} and {\em IPADDRESS} are specific to your system) while accessing \pla or entities within the database, try the following steps:

\begin{itemize}
	\item Make sure you entered the user/password combination correctly.
	
	\item Use the MySQL command line client (as opposed to, e.g., your Integrated Development Environment) to access the database.
	
	\item Make sure you are accessing the view layer instead of the underlying tables (see Section~\ref{sec:getting_started}). That is, always query database objects starting with
	\texttt{vpla\_} instead of \texttt{pla\_}. For example, try a query like \texttt{SELECT cmp\_id FROM vpla\_competition;} before moving on to more complicated queries.
\end{itemize}

The publicly hosted instance of \pla is accessible from everywhere on the public Internet. Moreover, if you are seeing the error message above, you are already talking to the \pla database and network issues can not be the root cause of your problem.

\newpage

\appendix
\section{Operating \pla}
\lstset{language=SQL}

The writing above starts from the idea that you are making use of our publicly available \pla infrastructure. In certain scenarios you may wish to operate your own \pla installation, however. In Section~\ref{sec:creatingpla} we describe the initial setup of a \pla database, and in Section~\ref{sec:loadingpla} we explain how a Power TAC state log file is loaded into the database.

\subsection{Creating a \pla Database}
\label{sec:creatingpla}

\begin{description}
	\item[Create a database] Create an empty MySQL database called \texttt{pla}.
	
	\item[Create users] Create a user called \texttt{pla\_admin} with full schema privileges for \texttt{pla}. Create a user called \texttt{pla\_analysis} without any privileges. These will be added in the next step. As the names suggest, \texttt{pla\_admin} will be used for administrative purposes (such as loading new competitions), whereas \texttt{pla\_analysis} is an example of an analysis user. We recommend creating personalized accounts for each analysis user. Use the role \texttt{pla\_readall} in the \pla database model as a blueprint for the  privileges of \pla analysis users.
	
	\item[Create schema objects] Create the \pla tables, views, and routines by forward engineering from the \pla database model using MySQL Workbench. Alternatively, execute the \texttt{placreate.sql} script in \texttt{<PLAROOT>/scripts} directory using a MySQL client of your choice. Be aware that the model always contains the definitive version of \pla whereas the SQL script may be slightly out of date.
\end{description}

\subsection{Loading Competitions}
\label{sec:loadingpla}

Given the state log file of a past Power TAC competition, loading it into \pla is a three-step process. As user \texttt{pla\_admin}:
\begin{enumerate}
	\item {\bfseries Pre-process} using the shell-script \texttt{processlogs.sh} to transform the state log file into the format required by \pla. This step removes various unused entry types from the file and it fixes several issues that make state logs difficult to interpret programmatically.
	\begin{verbatim}
		./processlogs.sh powertac-game.state [format] > powertac-game.processed
	\end{verbatim}
	The optional \texttt{[format]} specifier can currently take the values \texttt{2012} or \texttt{2013} for the 2012 pilot log format or the 2013 tournament log format.
	
	\item {\bfseries Load} a raw version of the pre-processed state log file into the database. A dedicated staging table called \texttt{pla\_import} is used for that purpose.
	
	\item {\bfseries Process} the data in \texttt{pla\_import}, create domain entities (e.g., records in \texttt{pla\_order}) from it, and create foreign key relationships among these entities. 	
\end{enumerate}
%
The following code fragment performs the second and third step used above. It assumes that \texttt{<filename>} points to pre-processed state log file you obtained in step 1. \texttt{<compname>} is a free-form, descriptive name for the imported competition that is inserted as a comment property into \texttt{pla\_competition\_property}. It also follows the recommended procedure to remove any remains from previous loads using the initial \texttt{TRUNCATE TABLE} command.

\begin{lstlisting}[frame=single]
TRUNCATE TABLE pla_import;

LOAD DATA LOCAL
  INFILE "<filename>"
  REPLACE
  INTO TABLE pla.pla_import 
  FIELDS TERMINATED BY "::"
  LINES TERMINATED BY "\n"
  (imp_ts,imp_time,imp_class,imp_instance_id,imp_method,
   imp_arg1,imp_arg2,imp_arg3,imp_arg4,
   imp_arg5,imp_arg6,imp_arg7,imp_arg8,
   imp_arg9, imp_arga, imp_argb, imp_argc,
   imp_argd);

% '2013' is the format specifier, see processlogs.sh
% for further details.
CALL pla.import('<compname>', TRUE, '2013'); 
\end{lstlisting}

Note, that the \texttt{LOAD DATA LOCAL} command is expected to issue warnings of the form \texttt{'1261 Row 1 doesn't contain data for all columns'}. This is because the rows of Power TAC's state log files have different lengths, and not every row fills all columns of the staging table \texttt{pla\_import}. You can safely ignore these warnings.

\newpage

\bibliographystyle{plain}
\bibliography{references}

\end{document}
