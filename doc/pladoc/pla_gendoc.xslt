<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet PUBLIC "Unofficial XSLT 1.0 DTD" "http://www.w3.org/1999/11/xslt10.dtd">
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="text" indent="no"/>
	
	<xsl:template match="/">	
		<xsl:text>\subsection{Domain Entities}&#xa;</xsl:text>
		<xsl:text>\label{sec:ref_domain_entities}&#xa;</xsl:text>
	
		<xsl:text>\begin{landscape}&#xa;</xsl:text>
		<xsl:apply-templates select="/database/tables/table[@type='TABLE']"/>
		<xsl:text>\end{landscape}&#xa;</xsl:text>
			
		<xsl:text>\subsection{View Access}&#xa;</xsl:text>
		<xsl:text>\label{sec:ref_view_access}&#xa;</xsl:text>
		
		<xsl:text>\begin{landscape}&#xa;</xsl:text>		
		<xsl:apply-templates select="/database/tables/table[@type='VIEW']"/>
		<xsl:text>\end{landscape}&#xa;</xsl:text>
	</xsl:template>
	
	<xsl:template match="/database/tables/table[@type='TABLE']">		
		<xsl:variable name='escapedname'>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@name"/>
			</xsl:call-template>
		</xsl:variable>

		<!-- xsl:text>\begin{landscape}&#xa;</xsl:text -->
		<xsl:text>\subsubsection{</xsl:text><xsl:value-of select="$escapedname"/>
		<xsl:text>}&#xa;</xsl:text>

		<xsl:text>\label{sec:ref:</xsl:text><xsl:value-of select="@name"/>		
		<xsl:text>}&#xa;</xsl:text>
		
		<xsl:if test="@remarks!=''">
			<xsl:text>\begin{mdframed}[backgroundcolor=gray!10,skipabove=10pt,skipbelow=10pt]&#xa;</xsl:text>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@remarks"/>
			</xsl:call-template>
			<xsl:text>\end{mdframed}&#xa;\vspace{3mm}</xsl:text>
		</xsl:if>
				
		<xsl:text>		
			\tabcolsep=0.11cm
			\small
			\rowcolors{2}{gray!10}{white}
			\begin{tabular}{p{4.0cm}|p{1.3cm}|p{8.2cm}|p{5.0cm}|p{5.0cm}}
				\rowcolor{gray!25}
				{\bfseries Column} &amp; {\bfseries Type} &amp; 
				{\bfseries Description} &amp; {\bfseries Children} &amp; 
				{\bfseries Parents} \\
				\hline
		</xsl:text>		
		
		<xsl:apply-templates select="column"/>	
		
		<xsl:text>\end{tabular}&#xa;\vspace{3mm}&#xa;</xsl:text>
	</xsl:template>	
	
	<xsl:template match="/database/tables/table[@type='TABLE']/column">	
		<xsl:variable name='name'>
			<xsl:value-of select="@name"/>
		</xsl:variable>
		
		<xsl:variable name='escapedname'>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@name"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name='escapedremarks'>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@remarks"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name='formattedtype'>
			<xsl:call-template name="format-type">
  				<xsl:with-param name="name" select="@type"/>
				<xsl:with-param name="size" select="@size" />	
				<xsl:with-param name="digits" select="@digits"/>
				<xsl:with-param name="nullable" select="@nullable"/>
				<xsl:with-param name="auto" select="@auto"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:if test="../index[@name='PRIMARY']/column[@name=$name]">
			<xsl:text>\texttt{*}</xsl:text>
		</xsl:if>

		<xsl:if test="../index[not(@name='PRIMARY')]/column[@name=$name]">
			<xsl:text>\texttt{+}</xsl:text>
		</xsl:if>
		
		<xsl:text>\texttt{</xsl:text>
		<xsl:value-of select="$escapedname"/>
		<xsl:text>} &amp; \texttt{</xsl:text>
		<xsl:value-of select="$formattedtype"/>
		<xsl:text>} &amp; </xsl:text>
		<xsl:value-of select="$escapedremarks"/>
		<xsl:text> &amp; </xsl:text>
		<xsl:apply-templates select="child"/>	
		<xsl:text> &amp; </xsl:text>
		<xsl:apply-templates select="parent"/>	
		<xsl:text> \\&#xa;</xsl:text>
	</xsl:template>
	
	<xsl:template match="/database/tables/table[@type='TABLE']/column/child">
		<xsl:text>\texttt{\nameref{sec:ref:</xsl:text>
			<xsl:value-of select="@table"/>		
		<xsl:text>}} </xsl:text>
	</xsl:template>

	<xsl:template match="/database/tables/table[@type='TABLE']/column/parent">
		<xsl:text>\texttt{\nameref{sec:ref:</xsl:text>
			<xsl:value-of select="@table"/>		
		<xsl:text>}} </xsl:text>
	</xsl:template>
	
	<xsl:template match="/database/tables/table[@type='VIEW']">
		<xsl:variable name='escapedname'>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@name"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:text>\subsubsection{</xsl:text><xsl:value-of select="$escapedname"/>
		<xsl:text>}&#xa;</xsl:text>

		<xsl:text>\label{sec:ref:</xsl:text><xsl:value-of select="@name"/>		
		<xsl:text>}&#xa;</xsl:text>
		
		<xsl:if test="@remarks!=''">
			<xsl:text>\begin{mdframed}[backgroundcolor=gray!10,skipabove=10pt,skipbelow=10pt]&#xa;</xsl:text>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@remarks"/>
			</xsl:call-template>
			<xsl:text>\end{mdframed}&#xa;\vspace{3mm}</xsl:text>
		</xsl:if>
				
		<xsl:text>		
			\tabcolsep=0.11cm
			\small
			\rowcolors{2}{gray!10}{white}
			\begin{tabular}{p{4.0cm}|p{1.3cm}|p{18.2cm}}
				\rowcolor{gray!25}
				{\bfseries Column} &amp; {\bfseries Type} &amp; 
				{\bfseries Description} \\
				\hline
		</xsl:text>		
		
		<xsl:apply-templates select="column"/>	
		
		<xsl:text>\end{tabular}&#xa;\vspace{3mm}&#xa;</xsl:text>
	</xsl:template>
	
	<xsl:template match="/database/tables/table[@type='VIEW']/column">	
		<xsl:variable name='name'>
			<xsl:value-of select="@name"/>
		</xsl:variable>
		
		<xsl:variable name='escapedname'>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@name"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name='escapedremarks'>
			<xsl:call-template name="escape">
  				<xsl:with-param name="text" select="@remarks"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name='formattedtype'>
			<xsl:call-template name="format-type">
  				<xsl:with-param name="name" select="@type"/>
				<xsl:with-param name="size" select="@size" />	
				<xsl:with-param name="digits" select="@digits"/>
				<xsl:with-param name="nullable" select="@nullable"/>
				<xsl:with-param name="auto" select="@auto"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:text>\texttt{</xsl:text>
		<xsl:value-of select="$escapedname"/>
		<xsl:text>} &amp; \texttt{</xsl:text>
		<xsl:value-of select="$formattedtype"/>
		<xsl:text>} &amp; </xsl:text>
		<xsl:value-of select="$escapedremarks"/>
		<xsl:text> \\&#xa;</xsl:text>
	</xsl:template>
	
	<xsl:template name="escape">
		<xsl:param name="text"/>
		
		<xsl:variable name='escaped1'>		
			<xsl:call-template name="replace-string">
  				<xsl:with-param name="text" select="$text"/>
				<xsl:with-param name="replace" select="'_'" />	
				<xsl:with-param name="with" select="'\_'"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name='escaped2'>		
			<xsl:call-template name="replace-string">
  				<xsl:with-param name="text" select="$escaped1"/>
				<xsl:with-param name="replace" select="'{'" />	
				<xsl:with-param name="with" select="'\{'"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:call-template name="replace-string">
  			<xsl:with-param name="text" select="$escaped2"/>
			<xsl:with-param name="replace" select="'}'" />	
			<xsl:with-param name="with" select="'\}'"/>
		</xsl:call-template>			
	</xsl:template>
		
	<xsl:template name="replace-string">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="with"/>
		
		<xsl:choose>
			<xsl:when test="contains($text,$replace)">
				<xsl:value-of select="substring-before($text,$replace)"/>
        		<xsl:value-of select="$with"/>
        		
        		<xsl:call-template name="replace-string">
					<xsl:with-param name="text" select="substring-after($text,$replace)"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="with" select="$with"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="format-type">
		<xsl:param name="name"/>
		<xsl:param name="size"/>
		<xsl:param name="digits"/>
		<xsl:param name="nullable"/>
		<xsl:param name="auto"/>
						
		<xsl:choose>
			<xsl:when test="$name='INT UNSIGNED'">
				<xsl:text>UI</xsl:text>
	      		<xsl:value-of select="$size"/>
	      		<xsl:text></xsl:text>
			</xsl:when>
			<xsl:when test="$name='INT'">
				<xsl:text>I</xsl:text>
	      		<xsl:value-of select="$size"/>
	      		<xsl:text></xsl:text>
			</xsl:when>
			<xsl:when test="$name='DECIMAL'">
				<xsl:text>D</xsl:text>
				<xsl:value-of select="$size"/>
	     		<xsl:text>,</xsl:text>
				<xsl:value-of select="$digits"/>
			</xsl:when>
			<xsl:when test="$name='DATETIME'">
				<xsl:text>DT</xsl:text>
			</xsl:when>
			<xsl:when test="$name='BIT'">
				<xsl:text>BOOL</xsl:text>
			</xsl:when>
			<xsl:when test="$name='VARCHAR'">
				<xsl:text>C</xsl:text>
				<xsl:value-of select="$size"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>
