*
* PLA - Power TAC Logfile Analysis
*
* 05/11/2013, Markus Peters, peters@rsm.nl
*

This document describes the setup of a PLA (Power TAC Logfile Analysis) environment
including its documentation. Generally speaking, PLA consists of three elements which
we will discuss, in turn:

(1) The PLA database model, together with DDL scripts generated from it, and shell scripts
    to pre-process Power TAC state log files for loading into PLA.
    
(2) A PLA handbook, written in LaTeX, together with scripts to extract structural 
	information from the PLA database and convert it into LaTeX.
	
(3) Various R scripts for analyzing and visualizing PLA data.

The PLA handbook contains more information on setting up and operating a PLA database.

******************************************************************************************

Part (0) Getting PLA

PLA is publicly accessible from bitbucket.org, a git hoster. To clone PLA onto your 
machine you need a git client. Go to the directory where the PLA sources should be
installed and execute:

	git clone git@bitbucket.org:markuspeters/pla.git
	
This creates a directory called pla and various subdirectories. We will refer to the
absolute path of the pla directory as <PLAROOT> below.

******************************************************************************************

Part (1) The PLA database model

The pla database model is contained in <PLAROOT>/pla.mwb which can be opened with
MySQL workbench, see http://www.mysql.com/products/workbench/. Derived from the
model is <PLAROOT>/script/placreate.sql, a file containing DDL statements to set up an
empty PLA database.

******************************************************************************************

Part (2) The PLA handbook

The handbook is a self-contained LaTeX project residing in <PLAROOT>/doc. The directory
contains:

- powertac_analysis_handbook.tex: 	Sources for the PLA technical report

- pla_gendoc.tex: 					Schema metadata for the PLA database, extracted from 
									the PLA database itself, see below. This file is
									included by powertac_analysis_handbook.tex and 
									contains the table and view reference.
									
- generate.sh: 						Shell script to generate pla_gendoc.tex from an 
									existing PLA database

- pladoc:							Directory with various files needed to extract and 
									transform schema metadata to create pla_gendoc.txt

Generally speaking, the handbook is generated from two sources:

- A fixed chunk of LaTeX containing the general setup of the handbook as well as the 
  introductory text (<PLAROOT>/doc/powertac_analysis_handbook.tex)
  
- A dynamic portion that is generated directly out of the PLA database, more specifically
  the table metadata and the comment fields of tables and columns. The result of this
  generation process is <PLAROOT>/doc/pla_gendoc.tex which, in turn, is included from the 
  main document.
    
Given those two files, the usual sequence:

	pdflatex powertac_analysis_handbook
	bibtex powertac_analysis_handbook
	pdflatex powertac_analysis_handbook
	pdflatex powertac_analysis_handbook
	
executed from <PLAROOT>/doc will generate the complete handbook in 
<PLAROOT>/doc/powertac_analysis_handbook.pdf.

The dynamic portion of the pla_gendoc.tex can be re-generated using the files in the 
<PLAROOT>/doc/** directory. To do so, a few prerequisites have to be met by your system:

- You need a working Java installation on your PATH (type "java -version" in the doc 
  directory and see if a version number is displayed).
	  
- You need an XSLT processor on your path. We'll assume that you have xsltproc installed, 
  which is based on libxml and libxslt. If you're using a different XSLT processor you
  will have to edit <PLAROOT>/doc/generate.sh accordingly.
	  
The general process of generation is as follows:

- Based on a running PLA database instance, use SchemaSpy to extract the relevant metadata 
  into an XML file. SchemaSpy and the required MySQL Connector libraries are part of this 
  distribution (see the <PLAROOT>/doc/pladoc directory). This creates the file 
  <PLAROOT>/doc/pladoc/pla.xml (and a few other files in <PLAROOT>/pladoc) that contains 
  an XML representation of the PLA schema metadata.
	  
- Transform <PLAROOT>/pladoc/pla.xml into <PLAROOT>/pla_gendoc.tex using the 
  <PLAROOT>/pladoc/pla_gendoc.xslt XSLT transformation. That generates a LaTeX fragments 
  containing tabular representations of the PLA schema metadata.
	  
- Follow the pdflatex/bibtex steps above to create the overall handbook.
    
******************************************************************************************

Part (3) Analysis scripts

There are a few example scripts and figures used in our AAAI 2013 paper

Wolfgang Ketter, Markus Peters, and John Collins. 
Autonomous agents in future energy markets: The 2012 Power Trading Agent Competition. 
In Proceedings of the Twenty-Seventh AAAI Conference on Artificial Intelligence. AAAI, Forthcoming.

in the <PLAROOT>/analysis directory. Use at your own risk; this part is currently under
construction.
